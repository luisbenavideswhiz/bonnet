<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('web.partials.header', 'App\Http\ViewComposers\CategoryComposer');
        View::composer('web.partials.header', 'App\Http\ViewComposers\TypeComposer');
        View::composer('web.partials.header', 'App\Http\ViewComposers\HeaderComposer');
        View::composer('web.partials.footer', 'App\Http\ViewComposers\FooterComposer');
        View::composer('landing.fire_protection.partials.footer', 'App\Http\ViewComposers\FooterComposer');
        View::composer('landing.pool.partials.footer', 'App\Http\ViewComposers\FooterComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
