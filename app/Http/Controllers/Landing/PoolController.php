<?php

namespace App\Http\Controllers\Landing;

use App\Http\Requests\Landing\FireProtectionStoreRequest;
use App\Services\Eloquent\ContactService;
use App\Services\Mailing\ApiWhiz;
use Illuminate\Http\Request;

class PoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landing.pool.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FireProtectionStoreRequest $request
     * @param ContactService $contactService
     * @param ApiWhiz $apiWhiz
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(FireProtectionStoreRequest $request, ContactService $contactService, ApiWhiz $apiWhiz)
    {
        $data = $request->only('name', 'last_name', 'phone', 'email', 'business_name', 'product_interest', 'message');
        $data['landing_id'] = 3;
        $c = $contactService->create($data);

        $apiWhiz->sendMail(
            ['email' => getenv('MAIL_FROM_ADDRESS'), 'name' => getenv('MAIL_FROM_NAME')],
            ['email' => $c->email, 'name' => $c->name],
            'Bonnett Confirmaci&oacute;n',
            view('mailings.landing.pool.confirmation', ['contact' => $c])->render());

        $apiWhiz->sendMail(
            ['email' => getenv('MAIL_FROM_ADDRESS'), 'name' => getenv('MAIL_FROM_NAME')],
            ['email' => getenv('MAIL_ADMIN'), 'name' => 'Administrador de la plataforma'],
            'Bonnett Consulta Landing',
            view('mailings.landing.pool.confirmation-alert', ['contact' => $c])->render());

        return $this->responseSuccess('Realizado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
