<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\SliderResource;
use App\Services\Eloquent\SliderService;

class HomeController extends Controller
{
    public function slider(SliderService $sliderService)
    {
        $data = $sliderService->getAll();
        return $this->responseSuccess('', SliderResource::collection($data));
    }
}
