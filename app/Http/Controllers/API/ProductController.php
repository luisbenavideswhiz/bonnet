<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProductListResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductSimpleResource;
use App\Http\Resources\ProductsSimpleResource;
use App\Http\Resources\ProductTypeResource;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubTable;
use App\Services\Eloquent\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ProductService $productService
     * @return Response
     */
    public function index(ProductService $productService)
    {
        $data = $productService->getAll();
        return $this->responseSuccess(trans('api.product.success.list', ['total' => $data->count()]),
            ProductSimpleResource::collection($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @param ProductService $productService
     * @return Response
     */
    public function show($id, ProductService $productService)
    {
        return $this->responseSuccess('', new ProductResource($productService->find($id)));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get Product for Category
     *
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function productCategory($slug)
    {
        $products = Product::with('category')->whereHas('category', function ($query) use ($slug) {
            $query->where('slug', $slug);
        });
        $data = $products->limit(10)->get();
        return $this->responseSuccess(trans('api.product.success.list', ['total' => $data->count()]),
            ProductSimpleResource::collection($data));
    }

    /**
     * Search Products
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request)
    {
        $type = $request->get('type');
        $brand = $request->get('brand');
        $search = $request->get('q');
        $category = $request->get('category');

        $products = Product::with('category', 'brand', 'type');
        if (!is_null($search)) {
            $products = $products->where(function ($query) use ($search) {
                $query->orWhereHas('category', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%');
                });
                $query->orWhereHas('brand', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%'); 
                });
                $query->orWhereHas('type', function ($query) use ($search) {
                    $query->where('meta_value', 'like', '%' . $search . '%');
                });
                $query->orWhere('name', 'like', '%' . $search . '%')
                    ->orWhere('tag', 'like', '%' . $search . '%');
            });
        }
        if (!is_null($category)) {
            $products = $products->whereHas('category', function ($query) use ($category) {
                $query->where('slug', $category);
            });
        }
        if (!is_null($brand)) {
            $products = $products->whereHas('brand', function ($query) use ($brand) {
                $query->whereIn('slug', $brand);
            });
        }
        if (!is_null($type)) {
            $products = $products->whereHas('type', function ($query) use ($type) {
                $query->whereIn('meta_option', $type);
            });
        }

        $data = $products->paginate(12);
        return $this->responseSuccess(trans('api.product.success.list', ['total' => $data->count()]),
            ProductSimpleResource::collection($data), [
                'current_page' => (integer)$data->currentPage(),
                'last_page' => (integer)$data->lastPage(),
                'items' => (integer)$data->items(),
                'total_items' => (integer)$data->total(),
                'has_more_pages' => (integer)$data->hasMorePages(),
                'from' => (integer)$data->firstItem(),
                'to' => (integer)$data->lastItem(),
            ]);
    }

    /**
     * Get all Product Types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function productTypes(Request $request)
    {
        $category = Category::where('slug', $request->get('category'))->first()->id;
        
        $data = Product::where('category_id', $category)
                ->join('sub_tables as s', 's.id', '=', 'products.type_id')
                ->distinct()
                ->select('s.id', 's.meta_option', 's.meta_value')
                ->get();
        return $this->responseSuccess(trans('api.type.success.list', ['total' => $data->count()]),
            ProductTypeResource::collection($data));
    }

    /**
     * @param ProductService $productService
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function home(ProductService $productService, Request $request)
    {
        /*$data = [];
        foreach ($request->get('categories',[]) as $category){
            $data[$category] = new ProductsSimpleResource($productService->getAll($category, 8));
        }*/
        return $this->responseCollectionSuccess('', new ProductsSimpleResource($productService->getAll($request->category, 8)));
    }
}