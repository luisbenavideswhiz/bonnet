<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\BrandResource;
use App\Services\Eloquent\BrandService;
use App\Services\Eloquent\CategoryService;
use App\Models\Category;
use App\Models\Product;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param BrandService $brandService
     * @return Response
     */
    public function index(BrandService $brandService)
    {
        $data = $brandService->getAll();
        return $this->responseSuccess(trans('api.brand.success.list', ['total' => $data->count()]),
            BrandResource::collection($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get all Product Types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function productBrands(Request $request)
    {
        $category = Category::where('slug', $request->get('category'))->first()->id;
        $data = Product::where('category_id', $category)
                ->join('brands as b', 'b.id', '=', 'products.brand_id')
                ->distinct()
                ->select('b.*')
                ->get();
        return $this->responseSuccess(trans('api.brand.success.list', ['total' => $data->count()]),
                BrandResource::collection($data));
    }
}
