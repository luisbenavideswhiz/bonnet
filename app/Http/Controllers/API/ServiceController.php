<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\BannerResource;
use App\Http\Resources\ServiceResource;
use App\Models\SubTable;
use App\Services\Eloquent\Eloquent\ServiceService;

class ServiceController extends Controller
{
    /**
     * Banner
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function banner()
    {
        $data = SubTable::getServiceBanner();
        return $this->responseSuccess('', BannerResource::collection($data));
    }

    /**
     * Display a listing of the resource.
     *
     * @param ServiceService $serviceService
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ServiceService $serviceService)
    {
        $data = $serviceService->getAll();
        return $this->responseSuccess('', ServiceResource::collection($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
