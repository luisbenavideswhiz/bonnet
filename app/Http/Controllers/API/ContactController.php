<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\Api\ContactStoreRequest;
use App\Services\Eloquent\ContactService;
use App\Services\Mailing\ApiWhiz;

class ContactController extends Controller
{

    /**
     * @param ContactStoreRequest $request
     * @param ContactService $contactService
     * @param ApiWhiz $apiWhiz
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(ContactStoreRequest $request, ContactService $contactService, ApiWhiz $apiWhiz)
    {
        $data = $request->only(['name', 'last_name', 'document', 'business_document', 'business_name',
            'specialization_id', 'email', 'phone', 'product_interest', 'how_contact', 'message']);
        $contact = $contactService->create($data);

        $apiWhiz->prepareMail('Bonnett Confirmación',
            ['email' => getenv('MAIL_FROM_ADDRESS'), 'name' => getenv('MAIL_FROM_NAME')],
            ['email' => $contact->email, 'name' => $contact->name])
            ->setView('mailings.confirmation', ['contact' => $contact])->sendMail();

        $apiWhiz->prepareMail('Bonnett Consulta Web',
            ['email' => getenv('MAIL_FROM_ADDRESS'), 'name' => getenv('MAIL_FROM_NAME')],
            ['email' => getenv('MAIL_ADMIN'), 'name' => 'Administrador de la plataforma'])
            ->setView('mailings.confirmation-alert', ['contact' => $contact])->sendMail();
        return $this->responseSuccess('Contacto creado');
    }
}
