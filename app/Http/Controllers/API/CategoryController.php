<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\CategoriesResource;
use App\Http\Resources\CategoryResource;
use App\Services\Eloquent\CategoryService;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoryService $categoryService
     * @return Response
     */
    public function index(CategoryService $categoryService)
    {
        $data = $categoryService->getAll();
        return $this->responseSuccess(trans('api.category.success.list', ['total' => $data->count()]),
            CategoryResource::collection($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param CategoryService $categoryService
     * @return \Illuminate\Http\JsonResponse
     */
    public function home(CategoryService $categoryService)
    {
        $data = $categoryService->getAll(3);
        return $this->responseCollectionSuccess('', new CategoriesResource($data));
    }
}
