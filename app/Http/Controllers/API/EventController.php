<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\BannerResource;
use App\Http\Resources\EventResource;
use App\Models\SubTable;
use App\Services\Eloquent\EventService;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Banner
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function banner()
    {
        $data = SubTable::getEventBanner();
        return $this->responseSuccess('', BannerResource::collection($data));
    }

    /**
     * Display a listing of the resource.
     *
     * @param EventService $eventService
     * @param Request $request
     * @return void
     */
    public function index(EventService $eventService, Request $request)
    {
        $data = $eventService->getAll(6, $request->page);
        return $this->responseSuccess('', EventResource::collection($data), [
            'current_page' => (integer)$data->currentPage(),
            'last_page' => (integer)$data->lastPage(),
            'items' => (integer)$data->items(),
            'total_items' => (integer)$data->total(),
            'has_more_pages' => (integer)$data->hasMorePages(),
            'from' => (integer)$data->firstItem(),
            'to' => (integer)$data->lastItem(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
