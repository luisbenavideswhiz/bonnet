<?php

namespace App\Http\Controllers\Front;

class FrontController extends Controller
{
    public function index()
    {
        return view('front.home');
    }

    public function about()
    {
        return view('front.about');
    }

    public function products()
    {
        return view('front.products');
    }

    public function product_detail()
    {
        return view('front.product_detail');
    }

    public function services()
    {
        return view('front.services');
    }

    public function contact()
    {
        return view('front.contact');
    }

    public function events()
    {
        return view('front.events');
    }

    public function category()
    {
        return view('front.category');
    }

    public function event_detail()
    {
        return view('front.event_detail');
    }

    public function error_404()
    {
        return view('front.error_404');
    }

    public function politics()
    {
        return view('front.politics');
    }

    public function certifications()
    {
        return view('front.certifications');
    }

    public function search()
    {
        return view('front.search');
    }

    public function no_product()
    {
        return view('front.no_product');
    }

    public function thanks()
    {
        return view('front.thanks');
    }

    public function product_filter()
    {
        return view('front.product_filter');
    }

}
