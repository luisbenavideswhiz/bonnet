<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Web\CommentStoreRequest;
use App\Services\Eloquent\CommentService;
use App\Services\Eloquent\PostService;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * @param $id
     * @param CommentStoreRequest $request
     * @param CommentService $commentService
     * @param PostService $postService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store($id, CommentStoreRequest $request, CommentService $commentService, PostService $postService)
    {
        $data = $request->only('name', 'email', 'comment');
        $data['post_id'] = $id;
        $commentService->create($data);
        $items = view('web.partials.comment_item', ['comments' => $postService->comments($id)])->render();
        return $this->responseSuccess('Realziado', $items);
    }

    /**
     * @param Request $request
     * @param PostService $postService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function more(Request $request, PostService $postService)
    {
        $items = view('web.partials.comment_item',
            ['comments' => $postService->comments($postService->findSlug($request->slug)->id, $request->page)])->render();
        return $this->responseSuccess('Realziado', $items);
    }
}
