<?php

namespace App\Http\Controllers\Web;

use App\Models\Corporation;
use App\Models\Seo;
use App\Services\Eloquent\PostService;
use App\Services\Eloquent\CertificateService;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Event;
use App\Models\Gallery;
use App\Models\Product;
use App\Models\Service;
use App\Models\SubTable;
use App\Models\Slider;
use App\Models\Us;
use Illuminate\Support\Str;

class WebController extends Controller
{
    public function index()
    {
        $data['alliances'] = Gallery::getAlliances();
        $data['sliders'] = Slider::orderBy('reading_order')->get();
        $data['categories'] = Category::orderBy('reading_order')->get();
        $data['descriptive'] = SubTable::getDescriptive()->meta_option;
        $data['link'] = SubTable::getLink()->meta_option;
        $data['name_button'] = SubTable::getNameButton()->meta_option;
        $data['show_link'] = SubTable::getShowLink()->meta_option;
        $data['second_banner'] = [
            'center' => SubTable::getSecondBannerCenter()->meta_value,
            'right_text' => SubTable::getSecondBannerRight()->meta_value,
            'right_link' => SubTable::getSecondBannerRight()->meta_option,
            'left_text' => SubTable::getSecondBannerLeft()->meta_value,
            'left_link' => SubTable::getSecondBannerLeft()->meta_option,
        ];
        $data['general_text'] = SubTable::where('meta_key', '=', 'general-slider-text')->first();
        $data['seo'] = Seo::where('name', '=', 'home')->first();
        return view('web.home', $data);
    }

    public function about()
    {
        $data['vision'] = Us::getVision();
        $data['mission'] = Us::getMission();
        $data['holding'] = Us::getHolding();
        $data['enterprise'] = Us::getEnterprise();
        $data['corporations'] = Corporation::orderBy('reading_order')->get();
        $data['seo'] = Seo::where('name', '=', 'us')->first();
        return view('web.about', $data);
    }

    public function product_detail($slug)
    {
        $data['product'] = Product::where('slug', '=', $slug)->first();
        $data['seo'] = Seo::where('name', '=', $slug)->first();
        return view('web.product_detail', $data);
    }

    public function services()
    {
        $data['banner'] = SubTable::getServiceBanner();
        $data['services'] = Service::take(2)->get();
        $data['seo'] = Seo::where('name', '=', 'service')->first();
        return view('web.services', $data);
    }

    public function events()
    {
        $data['banner'] = SubTable::getEventBanner();
        $data['seo'] = Seo::where('name', '=', 'event')->first();
        $data['events'] = Event::limit(6)->orderBy('id', 'desc')->get();
        return view('web.events', $data);
    }

    public function event_ajax(Request $request)
    {
        $id = $request->id;

        $events = Event::where('id', '<', $id)->paginate(6);

        if (!$events->isEmpty()) {
            $view = view('web.events_ajax', ['events' => $events])->render();
            return $this->responseSuccess('', $view);
        }
        return $this->responseError('');
    }

    public function eventDetail($slug)
    {
        $data['seo'] = Seo::where('name', '=', $slug)->first();
        $data['event'] = Event::where('slug', '=', $slug)->first();
        $data['recent_events'] = Event::where('slug', '!=', $slug)->orderBy('reading_order')->limit(3)->get();
        return view('web.event_detail', $data);
    }

    public function category()
    {
        return view('web.category');
    }

    public function politics()
    {
        $data['policies'] = Us::getPolicy();
        $data['seo'] = Seo::where('name', '=', 'politic')->first();
        return view('web.politics', $data);
    }

    public function history()
    {
        $data['history'] = Us::getHistory();
        $data['seo'] = Seo::where('name', '=', 'history')->first();
        return view('web.history', $data);
    }

    public function certifications(CertificateService $certificateService)
    {
        $data['seo'] = Seo::where('name', '=', 'certification')->first();
        $data['certificates'] = $certificateService->getAll();
        return view('web.certifications', $data);
    }

    public function search()
    {
        $data['seo'] = Seo::where('name', '=', 'search')->first();
        return view('web.search', $data);
    }

    public function coming()
    {
        $data['seo'] = Seo::where('name', '=', 'coming')->first();
        return view('web.coming', $data);
    }

    public function no_product()
    {
        $data['seo'] = Seo::where('name', '=', 'no_product')->first();
        return view('web.no_product', $data);
    }

    public function thanks()
    {
        $data['seo'] = Seo::where('name', '=', 'thanks')->first();
        return view('web.thanks', $data);
    }

    public function product_filter(Request $request)
    {
        $brand = Brand::where('slug', Str::slug(strtolower($request->q), '-'))->count();
        $type  = SubTable::where('meta_option', Str::slug(strtolower($request->q), '-'))->count();

        if($brand == 1) {
            return redirect()->to('producto/buscar?category='.$request->category.'&brand[]='.Str::slug(strtolower($request->q), '-'));
        }
        if($type == 1) {
            return redirect()->to('producto/buscar?category='.$request->category.'&type[]='.Str::slug(strtolower($request->q), '-'));
        }
        if ($request->category == 'equipos-para-sector-agricola') {
            return redirect()->to('producto/buscar?category=equipos-para-el-sector-industrial');
        }
        $data['seo'] = Seo::where('name', '=', $request->category)->first();
        return view('web.product_filter', $data);
    }

    public function posts(PostService $postService)
    {
        $data['seo'] = Seo::where('name', '=', 'service')->first();
        $data['posts'] = $postService->getAll([], [], 6, 1, '*');
        return view('web.posts', $data);
    }

    public function post($slug, PostService $postService)
    {
        $data['seo'] = Seo::where('name', '=', 'service')->first();
        $data['post'] = $postService->findSlug($slug);
        $data['posts'] = $postService->getAll(['post' => $data['post']->id], [], 3, 1, '*');
        $data['url'] = 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        return view('web.post', $data);
    }
}