<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\GeneralTextRequest;
use App\Http\Requests\Admin\SliderStoreRequest;
use App\Http\Requests\Admin\SliderUpdateRequest;
use App\Models\Gallery;
use App\Models\Slider;
use App\Models\SubTable;
use App\Services\Eloquent\SliderService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['general_text'] = SubTable::where('meta_key', '=', 'general-slider-text')->first();
        return view('admin.views.slider.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.views.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SliderStoreRequest $request
     * @param SliderService $sliderService
     * @return \Illuminate\Http\Response
     */
    public function store(SliderStoreRequest $request, SliderService $sliderService)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('title', 'button', 'description', 'url');
            $path = '/storage/' . $request->image->store('slider', 'public');
            $gallery = Gallery::create(['name' => $request->image->getClientOriginalName(), 'type' => 'SLIDER',
                'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,]);
            $data['gallery_id'] = $gallery->id;
            $data['reading_order'] = Slider::max('reading_order') + 1;
            $sliderService->create($data);
            DB::commit();
            $response = $this->responseSuccess('Banner creado', ['redirect' => route('slider.index')]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param SliderService $sliderService
     * @return \Illuminate\Http\Response
     */
    public function show($id, SliderService $sliderService)
    {
        $slider = $sliderService->find($id);
        return $this->responseSuccess('', $slider);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param SliderService $sliderService
     * @return \Illuminate\Http\Response
     */
    public function edit($id, SliderService $sliderService)
    {
        $data['slider'] = $sliderService->find($id);
        return view('admin.views/slider/update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SliderUpdateRequest $request
     * @param SliderService $sliderService
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderUpdateRequest $request, SliderService $sliderService, $id)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('title', 'button', 'description', 'url');
            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('slider', 'public');
                $gallery = Gallery::create(['name' => $request->image->getClientOriginalName(), 'type' => 'SLIDER',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,]);
                $data['gallery_id'] = $gallery->id;
            }
            $sliderService->find($id)->update($data);
            DB::commit();
            $response = $this->responseSuccess('Banner actualizado', ['redirect' => route('slider.index')]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param SliderService $sliderService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, SliderService $sliderService)
    {
        $row = $sliderService->find($id);
        Slider::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
        $row->delete();
        return $this->responseSuccess('Banner eliminado', ['redirect' => route('slider.index')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Slider::count();

        $sliders = Slider::orderBy('reading_order');
        if (isset($search['value'])) {
            $sliders = $sliders->where(function ($query) use ($search) {
                $query->orWhere('name', 'like', '%' . $search['value'] . '%');
                $query->orWhere('address', 'like', '%' . $search['value'] . '%');
            });
        }
        $sliders = $length > 0 ? $sliders->paginate($length, ['*'], 'draw', $page) : $sliders->paginate($totalPosts);
        $data = [
            'status' => true,
            'sliders' => $sliders->items(),
            'draw' => $request->draw,
            'perPage' => $sliders->perPage(),
            'recordsTotal' => isset($search['value']) ? $sliders->total() : $totalPosts,
            'recordsFiltered' => $sliders->total(),
            'lastPage' => $sliders->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Slider::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])->decrement('reading_order');
                } else {
                    Slider::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])->increment('reading_order');
                }
                Slider::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }

    public function textGeneralUpdate(GeneralTextRequest $request)
    {
        try {
            $data = $request->only('meta_value');
            $data['meta_key'] = 'general-slider-text';
            SubTable::updateOrCreate(['meta_key' => 'general-slider-text'], $data);
            $response = $this->responseSuccess('Realizado', ['redirect' => route('slider.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }
}
