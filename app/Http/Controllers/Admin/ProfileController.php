<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ProfileChangePassRequest;
use App\Http\Requests\Admin\ProfileUpdateRequest;
use App\Models\Gallery;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['profile'] = User::find(Auth::user()->id);
        return view('admin.views.profile.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProfileUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileUpdateRequest $request)
    {
        try {
            $data = $request->only('name', 'last_name');
            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('product', 'public');
                $gallery = Gallery::create(['name' => $request->image->getClientOriginalName(), 'type' => 'PROFILE',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path]);
                $data['gallery_id'] = $gallery->id;
            }
            User::find($request->id)->update($data);
            $response = $this->responseSuccess('Actualizado', ['redirect' => route('profile.index')]);
        } catch (\Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileChangePassRequest $request, $id)
    {
        try {
            if (Hash::check($request->old_password, Auth::user()->password)) {
                User::find($id)->update(['password' => bcrypt($request->new_password)]);
                $response = $this->responseSuccess('Contraseña Actualizada', ['redirect' => route('profile.index')]);
            } else {
                $response = $this->responseError('', ['message' => 'Contraseña Actual incorrecta']);
            }
        } catch (\Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
