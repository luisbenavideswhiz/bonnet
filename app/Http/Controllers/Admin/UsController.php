<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UsEnterpriseStoreRequest;
use App\Http\Requests\Admin\UsStoreHoldingRequest;
use App\Http\Requests\Admin\UsStoreRequest;
use App\Models\Gallery;
use App\Models\Us;
use Exception;
use Illuminate\Support\Facades\DB;

class UsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function enterprise()
    {
        $data['enterprise'] = Us::getEnterprise();
        return view('admin.views.us.enterprise.index', $data);
    }

    public function mission()
    {
        $data['mission'] = Us::getMission();
        return view('admin.views.us.mission.index', $data);
    }

    public function vision()
    {
        $data['vision'] = Us::getVision();
        return view('admin.views.us.vision.index', $data);
    }

    public function holding()
    {
        $data['holding'] = Us::getHolding();
        return view('admin.views.us.holding.index', $data);
    }

    public function policy()
    {
        $data['policies'] = Us::getPolicy();
        return view('admin.views.us.policy.index', $data);
    }

    public function history()
    {
        $data['history'] = Us::getHistory();
        return view('admin.views.us.history.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param UsEnterpriseStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeEnterprise(UsEnterpriseStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('description');
            $data['type'] = 'ENTERPRISE';
            Us::updateOrCreate(['type' => 'ENTERPRISE'], $data);
            DB::commit();
            $response = $this->responseSuccess('Actualizado', ['redirect' => '/admin/us/enterprise']);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param UsStoreHoldingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeHolding(UsStoreHoldingRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('description');
            $data['type'] = strtoupper('holding');

            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('us', 'public');
                $gallery = Gallery::create([
                    'name' => $request->image->getClientOriginalName(), 'type' => strtoupper('holding'),
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
                ]);
                $data['gallery_id'] = $gallery->id;
            }

            Us::updateOrCreate(['type' => 'HOLDING'], $data);
            $response = $this->responseSuccess('Actualizado', ['redirect' => '/admin/us/holding']);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UsStoreRequest $request
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UsStoreRequest $request, $type)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('description');
            $data['type'] = strtoupper($type);

            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('us', 'public');
                $gallery = Gallery::create([
                    'name' => $request->image->getClientOriginalName(), 'type' => strtoupper($type),
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
                ]);
                $data['gallery_id'] = $gallery->id;
            }

            Us::updateOrCreate(['type' => $type], $data);
            $response = $this->responseSuccess('Actualizado', ['redirect' => '/admin/us/' . $type]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
