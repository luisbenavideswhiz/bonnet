<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\HeadquarterStoreRequest;
use App\Models\Headquarter;
use App\Services\Eloquent\HeadquarterService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HeadquarterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.views.us.headquarter.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.views.us.headquarter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param HeadquarterStoreRequest $request
     * @param HeadquarterService $headquarterService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(HeadquarterStoreRequest $request, HeadquarterService $headquarterService)
    {
        try {
            $data = $request->only('name', 'phone', 'email', 'address');
            $location = explode(',', $request->location);
            $data['latitude'] = $location[0];
            $data['longitude'] = $location[1];
            $data['reading_order'] = Headquarter::max('reading_order') + 1;
            $headquarterService->create($data);
            $response = $this->responseSuccess('Sede creado', ['redirect' => route('headquarter.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $headquarter = Headquarter::find($id);
        return $this->responseSuccess('', $headquarter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['headquarter'] = Headquarter::find($id);
        return view('admin.views.us.headquarter.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param HeadquarterStoreRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(HeadquarterStoreRequest $request, $id)
    {
        try {
            $data = $request->only('name', 'phone', 'email', 'address');
            $location = explode(',', $request->location);
            $data['latitude'] = $location[0];
            $data['longitude'] = $location[1];
            Headquarter::find($id)->update($data);
            $response = $this->responseSuccess('Sede creado', ['redirect' => route('headquarter.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @param HeadquarterService $headquarterService
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, HeadquarterService $headquarterService)
    {
        $row = $headquarterService->find($id);
        Headquarter::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
        $row->delete();
        return $this->responseSuccess('Sede eliminada', ['redirect' => route('headquarter.index')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Headquarter::count();

        $headquarters = Headquarter::orderBy('reading_order');
        if (isset($search['value'])) {
            $headquarters = $headquarters->where(function ($query) use ($search) {
                $query->orWhere('name', 'like', '%' . $search['value'] . '%');
                $query->orWhere('address', 'like', '%' . $search['value'] . '%');
            });
        }
        $headquarters = $length > 0 ? $headquarters->paginate($length, ['*'], 'draw', $page) : $headquarters->paginate($totalPosts);
        $data = [
            'status' => true,
            'headquarters' => $headquarters->items(),
            'draw' => $request->draw,
            'perPage' => $headquarters->perPage(),
            'recordsTotal' => isset($search['value']) ? $headquarters->total() : $totalPosts,
            'recordsFiltered' => $headquarters->total(),
            'lastPage' => $headquarters->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Headquarter::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])
                        ->decrement('reading_order');
                } else {
                    Headquarter::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])
                        ->increment('reading_order');
                }
                Headquarter::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }

}