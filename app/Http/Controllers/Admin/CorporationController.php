<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CorporationStoreRequest;
use App\Http\Requests\Admin\CorporationUpdateRequest;
use App\Models\Corporation;
use App\Models\Gallery;
use App\Services\Eloquent\CorporationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CorporationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.views.us.corporation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.views.us.corporation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CorporationStoreRequest $request
     * @param CorporationService $corporationService
     * @return \Illuminate\Http\Response
     */
    public function store(CorporationStoreRequest $request, CorporationService $corporationService)
    {
        try {
            $data = $request->only('title', 'description');
            $path = '/storage/' . $request->image->store('corporation', 'public');
            $gallery = Gallery::create([
                'name' => $request->image->getClientOriginalName(), 'type' => 'CORPORATION',
                'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
            ]);
            $data['gallery_id'] = $gallery->id;
            $data['reading_order'] = Corporation::max('reading_order') + 1;
            $corporationService->create($data);
            $response = $this->responseSuccess('Corporación creado', ['redirect' => route('corporation.index')]);
        } catch (\Exception $e) {
            $response = $this->responseSuccess($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @param CorporationService $corporationService
     * @return \Illuminate\Http\Response
     */
    public function show($id, CorporationService $corporationService)
    {
        $corporation = $corporationService->find($id);
        return $this->responseSuccess('', $corporation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @param CorporationService $corporationService
     * @return \Illuminate\Http\Response
     */
    public function edit($id, CorporationService $corporationService)
    {
        $data['corporation'] = $corporationService->find($id);
        return view('admin.views.us.corporation.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @param CorporationUpdateRequest $request
     * @param CorporationService $corporationService
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, CorporationUpdateRequest $request, CorporationService $corporationService)
    {
        try {
            $data = $request->only('title', 'description');
            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('corporation', 'public');
                $gallery = Gallery::create([
                    'name' => $request->image->getClientOriginalName(), 'type' => 'CORPORATION',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
                ]);
                $data['gallery_id'] = $gallery->id;
            }
            $corporationService->find($id)->update($data);
            $response = $this->responseSuccess('Corporación creado', ['redirect' => route('corporation.index')]);
        } catch (\Exception $e) {
            $response = $this->responseSuccess($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @param CorporationService $corporationService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, CorporationService $corporationService)
    {
        try {
            $row = $corporationService->find($id);
            Corporation::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
            $row->delete();
            $response = $this->responseSuccess('Corporación eliminado', ['redirect' => route('corporation.index')]);
        } catch (\Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Corporation::count();

        $corporations = Corporation::orderBy('reading_order');
        if (isset($search['value'])) {
            $corporations = $corporations->where(function ($query) use ($search) {
                $query->orWhere('title', 'like', '%' . $search['value'] . '%');
            });
        }
        $corporations = $length > 0 ? $corporations->paginate($length, ['*'], 'draw', $page) : $corporations->paginate($totalPosts);
        $data = [
            'status' => true,
            'corporations' => $corporations->items(),
            'draw' => $request->draw,
            'perPage' => $corporations->perPage(),
            'recordsTotal' => isset($search['value']) ? $corporations->total() : $totalPosts,
            'recordsFiltered' => $corporations->total(),
            'lastPage' => $corporations->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Corporation::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])
                        ->decrement('reading_order');
                } else {
                    Corporation::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])
                        ->increment('reading_order');
                }
                Corporation::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}
