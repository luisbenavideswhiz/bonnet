<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ChatsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function download()
    {
        return Excel::download(new ChatsExport, 'chats.xlsx');
    }
}
