<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function postUploader(Request $request)
    {
        $path = $request->hasFile('upload') ? '/storage/' . $request->upload->store('editor', 'public') : '';
        $gallery = Gallery::create(['name' => File::name($path), 'type' => 'image', 'path' => $path, 'mime' => 'PNG']);

        return response()->json([
            'uploaded' => true,
            'fileName' => $gallery->name,
            'url' => url($gallery->path)
        ]);
    }
}
