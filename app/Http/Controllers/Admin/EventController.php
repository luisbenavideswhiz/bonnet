<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\EventStoreRequest;
use App\Http\Requests\Admin\EventUpdateRequest;
use App\Models\Event;
use App\Models\Gallery;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * @var string
     */
    private $redirect = '/admin/event';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.event.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.views.event.create ');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EventStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('title', 'text', 'description');
            $data['slug'] = str_slug($request->title);
            $data['reading_order'] = Event::max('reading_order') + 1;
            $path = '/storage/' . $request->image->store('event', 'public');
            $gallery = Gallery::create([
                'name' => $request->image->getClientOriginalName(), 'type' => 'EVENT',
                'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
            ]);
            $data['gallery_id'] = $gallery->id;

            $event = Event::create($data);
            DB::commit();
            $response = $this->responseSuccess('Evento creado',
                ['redirect' => route('event.edit', ['id' => $event->id])]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::with('gallery')->find($id);
        return $this->responseSuccess('', $event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['event'] = Event::find($id);
        return view('admin.views.event.update ', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EventUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventUpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('title', 'text', 'description');
            $data['slug'] = str_slug($request->title);

            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('event/', 'public');
                $gallery = Gallery::create([
                    'name' => $request->image->getClientOriginalName(), 'type' => 'EVENT',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
                ]);
                $data['gallery_id'] = $gallery->id;
            }

            Event::find($id)->update($data);
            DB::commit();
            $response = $this->responseSuccess('Evento Actualizado', ['redirect' => route('event.edit', ['id' => $id])]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $row = Event::find($id);
            Event::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
            $row->delete();
            $response = $this->responseSuccess('Evento eliminado', ['redirect' => $this->redirect]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Event::orderBy('id', 'DESC')->count();

        $events = Event::orderBy('reading_order');
        if (isset($search['value'])) {
            $events = $events->where(function ($query) use ($search) {
                $query->orWhere('title', 'like', '%' . $search['value'] . '%');
            });
        }
        $events = $length > 0 ? $events->paginate($length, ['*'], 'draw', $page) : $events->paginate($totalPosts);
        $data = [
            'status' => true,
            'events' => $events->items(),
            'draw' => $request->draw,
            'perPage' => $events->perPage(),
            'recordsTotal' => isset($search['value']) ? $events->total() : $totalPosts,
            'recordsFiltered' => $events->total(),
            'lastPage' => $events->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Event::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])
                        ->decrement('reading_order');
                } else {
                    Event::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])
                        ->increment('reading_order');
                }
                Event::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}
