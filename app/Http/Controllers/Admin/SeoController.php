<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SeoStoreRequest;
use App\Http\Resources\CategorySelectResource;
use App\Http\Resources\EventSelectResource;
use App\Http\Resources\PostSelectResource;
use App\Http\Resources\ProductSelectResource;
use App\Http\Resources\SeosResource;
use App\Services\Eloquent\CategoryService;
use App\Services\Eloquent\EventService;
use App\Services\Eloquent\PostService;
use App\Services\Eloquent\ProductService;
use App\Services\Eloquent\SeoService;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.seo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.views.seo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SeoStoreRequest $request
     * @param SeoService $seoService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SeoStoreRequest $request, SeoService $seoService)
    {
        $data = $request->only('category', 'name', 'title', 'url', 'alt', 'description', 'keywords');
        !$request->hasFile('image') ?: $data['image'] = '/storage/' . $request->image->store('seo', 'public');
        if ($seoService->existName($data['name'])) {
            return $this->responseError('Error', ['error' => 'Sección de la web ya existe']);
        } else {
            $seoService->create($data);
            return $this->responseSuccess('Realizado', ['redirect' => route('seo.index')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param SeoService $seoService
     * @return \Illuminate\Http\Response
     */
    public function edit($id, SeoService $seoService)
    {
        $data['seo'] = $seoService->find($id);
        return view('admin.views.seo.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     * @param SeoService $seoService
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, SeoService $seoService)
    {
        $data = $request->only('category', 'name', 'title', 'url', 'alt', 'description', 'keywords');
        !$request->hasFile('image') ?: $data['image'] = '/storage/' . $request->image->store('seo', 'public');
        $seoService->find($id)->update($data);
        return $this->responseSuccess('Realizado', ['redirect' => route('seo.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param SeoService $seoService
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, SeoService $seoService)
    {
        $seoService->find($id)->delete();
        return $this->responseSuccess('Realizado', ['redirect' => route('seo.index')]);
    }

    /**
     * @param Request $request
     * @param SeoService $eventService
     * @return \Illuminate\Http\JsonResponse
     */
    public function listData(Request $request, SeoService $eventService)
    {
        $search = $request->get('search', []);
        empty($search['value']) ?: $search['value'] = htmlentities($search['value']);
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $data = $eventService->getAll($search, [], $length, $page);
        return $this->responseCollectionSuccess('', new SeosResource($data));
    }

    /**
     * Data for selects
     *
     * @param Request $request
     * @param CategoryService $categoryService
     * @param ProductService $productService
     * @param EventService $eventService
     * @param PostService $postService
     * @return \Illuminate\Http\JsonResponse
     */
    public function sectionData(Request $request, CategoryService $categoryService, ProductService $productService,
                                EventService $eventService, PostService $postService)
    {
        $data = null;
        if ($request->filter === 'web') {
            $data = collect([
                ['id' => 'home', 'text' => 'Inicio'],
                ['id' => 'us', 'text' => 'Nosotros'],
                ['id' => 'event', 'text' => 'Eventos'],
                ['id' => 'history', 'text' => 'Historia'],
                ['id' => 'product', 'text' => 'Productos'],
                ['id' => 'service', 'text' => 'Servicios'],
                ['id' => 'contact', 'text' => 'Contactos'],
                ['id' => 'politic', 'text' => 'Politicas de Calidad'],
                ['id' => 'certification', 'text' => 'Certificaciones']
            ]);
            if (isset($request->search)) {
                $pos = $data->search(function ($i, $k) use ($request) {
                    if (stripos($i['text'], $request->search) !== false) {
                        return $i;
                    }
                });
                $data = [$data->get($pos)];
            }
        }
        if ($request->filter === 'main') {
            $data = CategorySelectResource::collection($categoryService->getFilter(['value' => $request->search]));
        }
        if ($request->filter === 'event') {
            $data = EventSelectResource::collection($eventService->getFilter(['value' => $request->search]));
        }
        if ($request->filter === 'product') {
            $data = ProductSelectResource::collection($productService->getFilter(['value' => $request->search]));
        }
        if ($request->filter === 'post') {
            $data = PostSelectResource::collection($postService->getAll(['value' => $request->search]));
        }
        return $this->responseSuccess('', $data);
    }
}
