<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PostStoreRequest;
use App\Http\Requests\Admin\PostUpdateRequest;
use App\Http\Resources\PostsResource;
use App\Models\Post;
use App\Services\Eloquent\CommentService;
use App\Services\Eloquent\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.views.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostStoreRequest $request
     * @param PostService $postService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PostStoreRequest $request, PostService $postService)
    {
        $data = $request->only('title', 'short_description', 'description', 'tags');
        !isset($request->image) ?: $data['image'] = '/storage/' . $request->image->store('post', 'public');
        $data['slug'] = str_slug($request->slug);
        $data['reading_order'] = $postService->maxOrder() + 1;
        $post = $postService->create($data);
        return $this->responseSuccess('Realizado',
            ['redirect' => route('post.edit', ['id' => $post->id])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param PostService $postService
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, PostService $postService)
    {
        return $this->responseSuccess('', $postService->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param PostService $postService
     * @return \Illuminate\Http\Response
     */
    public function edit($id, PostService $postService)
    {
        return view('admin.views.post.update', ['post' => $postService->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostUpdateRequest $request
     * @param  int $id
     * @param PostService $postService
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id, PostService $postService)
    {
        $data = $request->only('title', 'short_description', 'description', 'tags');
        !isset($request->image) ?: $data['image'] = '/storage/' . $request->image->store('post', 'public');
        $data['slug'] = str_slug($request->slug);
        $post = $postService->find($id);
        $post->update($data);
        return $this->responseSuccess('Realizado',
            ['redirect' => route('post.edit', ['id' => $post->id])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param PostService $postService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, PostService $postService)
    {
        $postService->find($id)->delete();
        return $this->responseSuccess('Realizado', ['redirect' => route('post.index')]);
    }

    /**
     * @param Request $request
     * @param PostService $postService
     * @return \Illuminate\Http\JsonResponse
     */
    public function listData(Request $request, PostService $postService)
    {
        $search = $request->get('search', []);
        empty($search['value']) ?: $search['value'] = htmlentities($search['value']);
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $data = $postService->getAll($search, [], $length, $page);
        return $this->responseCollectionSuccess('', new PostsResource($data));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Post::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])->decrement('reading_order');
                } else {
                    Post::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])->increment('reading_order');
                }
                Post::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }

    /**
     * @param $id
     * @param PostService $postService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function comments($id, PostService $postService)
    {
        return view('admin.views.comment.index', ['post' => $postService->find($id)]);
    }

    /**
     * @param $id
     * @param CommentService $commentService
     * @return \Illuminate\Http\JsonResponse
     */
    public function comment($id, CommentService $commentService)
    {
        return $this->responseSuccess('Realizado', $commentService->find($id));
    }

    /**
     * @param $id
     * @param CommentService $commentService
     * @param PostService $postService
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentDelete($id, CommentService $commentService, PostService $postService)
    {
        $c = $commentService->find($id);
        $p = $postService->find($c->post_id);
        $c->delete();
        return $this->responseSuccess('Realizado',
            ['redirect' => route('post.comments', ['id' => $p->id])]);
    }
}
