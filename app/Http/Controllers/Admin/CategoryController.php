<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Models\Category;
use App\Models\Gallery;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    /**
     * @var string
     */
    private $redirect = '/admin/category';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.views.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryStoreRequest $request)
    {
        try {
            $data = $request->only('name', 'description');
            $data['slug'] = str_slug($request->name);
            $data['parent_id'] = 0;
            $data['level'] = 1;
            $data['reading_order'] = Category::max('reading_order') + 1;

            $path = '/storage/' . $request->image->store('category', 'public');
            $gallery = Gallery::create(['name' => $request->image->getClientOriginalName(), 'type' => 'CATEGORY',
                'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path]);
            $data['gallery_id'] = $gallery->id;

            Category::create($data);
            $response = $this->responseSuccess('Categoría Registrada', ['redirect' => route('category.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $data = Category::with('image')->find($id);
        return $this->responseSuccess('', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @param CategoryUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, CategoryUpdateRequest $request)
    {
        try {
            $data = $request->only('name', 'description');
            $data['slug'] = str_slug($request->name);
            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('category', 'public');
                $gallery = Gallery::create(['name' => $request->image->getClientOriginalName(), 'type' => 'CATEGORY',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path]);
                $data['gallery_id'] = $gallery->id;
            }
            Category::find($id)->update($data);
            $response = $this->responseSuccess('Categoría Actualizada', ['redirect' => route('category.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $row = Category::find($id);
            if (count($row->product) === 0) {
                Category::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
                $row->delete();
                $response = $this->responseSuccess('Categoría Eliminada', ['redirect' => $this->redirect]);
            } else {
                $response = $this->responseError('', ['message' => 'No se puede eliminar por que cuenta con productos']);
            }
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Category::orderBy('id', 'DESC')->count();

        $categories = Category::orderBy('reading_order');
        if (isset($search['value'])) {
            $categories = $categories->where(function ($query) use ($search) {
                $query->orWhere('name', 'like', '%' . $search['value'] . '%');
            });
        }
        $categories = $length > 0 ? $categories->paginate($length, ['*'], 'draw', $page) : $categories->paginate($totalPosts);
        $data = [
            'status' => true,
            'categories' => $categories->items(),
            'draw' => $request->draw,
            'perPage' => $categories->perPage(),
            'recordsTotal' => isset($search['value']) ? $categories->total() : $totalPosts,
            'recordsFiltered' => $categories->total(),
            'lastPage' => $categories->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Category::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])
                        ->decrement('reading_order');
                } else {
                    Category::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])
                        ->increment('reading_order');
                }
                Category::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }

}