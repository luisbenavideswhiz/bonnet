<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ProductDetailStoreRequest;
use App\Models\Gallery;
use App\Models\Product;
use App\Models\ProductDetail;
use Exception;
use Illuminate\Http\Request;

class ProductDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductDetailStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductDetailStoreRequest $request)
    {
        try {
            $data = $request->only('title', 'description');
            $data['product_id'] = $request->id;

            if ($request->has('pdf')) {
                $path = '/storage/' . $request->pdf->store('product' . $request->id, 'public');
                $gallery = Gallery::create([
                    'name' => $request->pdf->getClientOriginalName(), 'type' => 'PRODUCT_DETAIL_PDF',
                    'mime' => $request->pdf->getClientMimeType(), 'description' => '', 'path' => $path,
                ]);
                $data['pdf_id'] = $gallery->id;
            };

            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('product' . $request->id, 'public');
                $gallery = Gallery::create([
                    'name' => $request->image->getClientOriginalName(), 'type' => 'PRODUCT_DETAIL_IMG',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
                ]);
                $data['gallery_id'] = $gallery->id;
            }

            if (!is_null($request->product_detail_id)) {
                ProductDetail::find($request->product_detail_id)->update($data);
            } else {
                ProductDetail::create($data);
            }
            $response = $this->responseSuccess('Caracteristica creada',
                ['redirect' => '/admin/product/detail/' . $request->id . '/edit']);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $feature = ProductDetail::with('pdf', 'image')->find($id);
        return $this->responseSuccess('', $feature);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['product'] = Product::find($id);
        $data['features'] = ProductDetail::where('product_id', '=', $id)->get();
        return view('admin.views.product.detail.index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $feature = ProductDetail::find($id);
        $feature->delete();
        return $this->responseSuccess('Característica eliminada',
            ['redirect' => '/admin/product/detail/' . $feature->product_id . '/edit']);
    }

}