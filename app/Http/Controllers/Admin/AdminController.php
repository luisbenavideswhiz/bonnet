<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FooterStoreRequest;
use App\Http\Requests\Admin\HeaderStoreRequest;
use App\Http\Requests\Admin\UsEnterpriseStoreRequest;
use App\Http\Requests\Admin\OurLinesUpdateRequest;
use App\Models\Gallery;
use App\Models\SubTable;
use Exception;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.index');
    }

    /**
     * get view alliance and galleries
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alliance()
    {
        $data['gallery'] = Gallery::getAlliances();
        return view('admin.views.home.alliance.index', $data);
    }

    /**
     * get view footer
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function footer()
    {
        $data['email'] = SubTable::getFooterEmail();
        $data['phone'] = SubTable::getFooterPhone();
        $data['address'] = SubTable::getFooterAddress();
        $data['description'] = SubTable::getFooterDescription();
        $data['socialNetwork'] = SubTable::getFooterSocialNetwork();
        $data['quickLink'] = SubTable::getFooterQuickLink();
        $data['networks'] = SubTable::getSocialNetwork();
        return view('admin.views.home.footer.index', $data);
    }

    /**
     * store footer
     *
     * @param FooterStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postFooter(FooterStoreRequest $request)
    {
        try {
            SubTable::updateOrCreate(['meta_key' => 'footer_email'], ['meta_key' => 'footer_email', 'meta_value' => $request->email]);
            SubTable::updateOrCreate(['meta_key' => 'footer_phone'], ['meta_key' => 'footer_phone', 'meta_value' => $request->phone]);
            SubTable::updateOrCreate(['meta_key' => 'footer_address'], ['meta_key' => 'footer_address', 'meta_value' => $request->address]);
            SubTable::updateOrCreate(['meta_key' => 'footer_description'], ['meta_key' => 'footer_description', 'meta_value' => '', 'meta_option' => $request->description]);

            //Social Network
            $rowsS = SubTable::where('meta_key', '=', 'footer_social_network');
            $rowsS->count() <= 0 ?: $rowsS->delete();
            foreach ($request->social_network_id as $key => $value) {
                $sn = ['meta_key' => 'footer_social_network', 'meta_value' => $value, 'meta_option' => $request->social_network[$key]];
                SubTable::create($sn);
            }
            //Quick Link
            $rowsQ = SubTable::where('meta_key', '=', 'footer_quick_link');
            $rowsQ->count() == 0 ?: $rowsQ->delete();
            foreach ($request->url_text as $key => $value) {
                $ql = ['meta_key' => 'footer_quick_link', 'meta_value' => $value, 'meta_option' => $request->url_link[$key]];
                SubTable::create($ql);
            }
            $response = $this->responseSuccess('Realizado', ['redirect' => '/admin/home/footer']);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * get event banner
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function event()
    {
        $data['event'] = SubTable::where('meta_key', '=', 'event_banner')->first();
        return view('admin.views.event.banner.index', $data);
    }

    /**
     * store event banner
     *
     * @param UsEnterpriseStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEvent(UsEnterpriseStoreRequest $request)
    {
        try {
            $data['meta_key'] = 'event_banner';
            $data['meta_value'] = '';
            $data['meta_option'] = $request->description;
            SubTable::updateOrCreate(['meta_key' => 'event_banner'], $data);
            $response = $this->responseSuccess('Realizado', ['redirect' => '/admin/home/event']);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * get service banner
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function service()
    {
        $data['service'] = SubTable::where('meta_key', '=', 'service_banner')->first();
        return view('admin.views.service.banner.index', $data);
    }

    /**
     * store service banner
     *
     * @param UsEnterpriseStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postService(UsEnterpriseStoreRequest $request)
    {
        try {
            $data['meta_key'] = 'service_banner';
            $data['meta_value'] = '';
            $data['meta_option'] = $request->description;
            SubTable::updateOrCreate(['meta_key' => 'service_banner'], $data);
            $response = $this->responseSuccess('Realizado', ['redirect' => '/admin/home/service']);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    public function header()
    {
        $data['banner_top'] = SubTable::getHeaderBannerTop();
        $data['menu_us'] = SubTable::getBannerTopMenuUs();
        $data['menu_product'] = SubTable::getBannerTopMenuProduct();
        $data['menu_service'] = SubTable::getBannerTopMenuService();
        $data['menu_contact'] = SubTable::getBannerTopMenuContact();
        $data['menu_event'] = SubTable::getBannerTopMenuEvent();
        $data['menu_blog'] = SubTable::getBannerTopMenuBlog();
        return view('admin.views.home.header.index', $data);
    }

    public function postHeader(HeaderStoreRequest $request)
    {
        try {
            $data = $request->only('meta_option', 'meta_link');
            $data['meta_key'] = 'header_banner_top';
            $data['meta_value'] = $request->has('meta_value') ? $request->meta_value : 0;
            SubTable::updateOrCreate(['meta_key' => 'header_banner_top'], $data);
            if (isset($request->us)) {
                $dataUs = ['meta_key' => 'header_banner_top_us', 'meta_value' => $request->us];
                SubTable::updateOrCreate(['meta_key' => 'header_banner_top_us'], $dataUs);
            }
            if (isset($request->product)) {
                $dataProduct = ['meta_key' => 'header_banner_top_product', 'meta_value' => $request->product];
                SubTable::updateOrCreate(['meta_key' => 'header_banner_top_product'], $dataProduct);
            }
            if (isset($request->service)) {
                $dataService = ['meta_key' => 'header_banner_top_service', 'meta_value' => $request->service];
                SubTable::updateOrCreate(['meta_key' => 'header_banner_top_service'], $dataService);
            }
            if (isset($request->contact)) {
                $dataContact = ['meta_key' => 'header_banner_top_contact', 'meta_value' => $request->contact];
                SubTable::updateOrCreate(['meta_key' => 'header_banner_top_contact'], $dataContact);
            }
            if (isset($request->event)) {
                $dataEvent = ['meta_key' => 'header_banner_top_event', 'meta_value' => $request->event];
                SubTable::updateOrCreate(['meta_key' => 'header_banner_top_event'], $dataEvent);
            }
            if (isset($request->blog)) {
                $dataBlog = ['meta_key' => 'header_banner_top_blog', 'meta_value' => $request->blog];
                SubTable::updateOrCreate(['meta_key' => 'header_banner_top_blog'], $dataBlog);
            }
            $response = $this->responseSuccess('Realizado', ['redirect' => route('admin.header')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Descriptive index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function descriptive()
    {
        $data['descriptive'] = SubTable::getDescriptive();
        $data['link'] = SubTable::getLink();
        $data['name_button'] = SubTable::getNameButton();
        $data['show_link'] = SubTable::getShowLink();
        return view('admin.views.home.descriptive.index', $data);
    }

    /**
     * Descriptive store
     *
     * @param UsEnterpriseStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDescriptive(OurLinesUpdateRequest $request)
    {
        try {
            SubTable::where('meta_key', '=', 'descriptive')->update(['meta_option' => $request->description]);
            SubTable::where('meta_key', '=', 'name_button')->update(['meta_option' => $request->name_button]);
            SubTable::where('meta_key', '=', 'link')->update(['meta_option' => $request->link]);
            SubTable::where('meta_key', '=', 'show_link')->update(['meta_option' => $request->show_link]);
            $response = $this->responseSuccess('Realizado', ['redirect' => route('admin.descriptive')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * get view login
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('admin.login.index');
    }
}
