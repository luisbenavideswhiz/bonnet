<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SecondSliderRequest;
use App\Models\SubTable;
use Illuminate\Http\Request;

class SecondSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'text_center' => SubTable::getSecondBannerCenter(),
            'text_right' => SubTable::getSecondBannerRight(),
            'text_left' => SubTable::getSecondBannerLeft(),
        ];
        return view('admin.views.second-slider.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SecondSliderRequest $request)
    {
        try {
            SubTable::updateOrCreate(['meta_key' => 'second_slider_center'], ['meta_key' => 'second_slider_center', 'meta_value' => $request->text_center]);
            SubTable::updateOrCreate(['meta_key' => 'second_slider_right'], ['meta_key' => 'second_slider_right', 'meta_value' => $request->text_right, 'meta_option' => $request->link_right]);
            SubTable::updateOrCreate(['meta_key' => 'second_slider_left'], ['meta_key' => 'second_slider_left', 'meta_value' => $request->text_left, 'meta_option' => $request->link_left]);
            $response = $this->responseSuccess('Banner Actualizado', ['redirect' => route('second-slider.index')]);
        } catch (\Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
