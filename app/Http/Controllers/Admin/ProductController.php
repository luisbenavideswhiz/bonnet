<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ProductUpdateRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Product;
use App\Http\Requests\Admin\ProductStoreRequest;
use App\Models\SubTable;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.views.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['brands'] = Brand::all();
        $data['categories'] = Category::all();
        $data['productTypes'] = SubTable::getProductType();
        return view('admin.views.product.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only(['name', 'type_id', 'sku', 'category_id', 'status', 'brand_id', 'stock',
                'description', 'tag', 'model']);
            $data['slug'] = str_slug($request->name);

            $path = '/storage/' . $request->image->store('product', 'public');
            $gallery = Gallery::create(['name' => $request->image->getClientOriginalName(), 'type' => 'PRODUCT',
                'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path]);
            $data['gallery_id'] = $gallery->id;

            if ($request->has('pdf')) {
                $path = '/storage/' . $request->pdf->store('product', 'public');
                $pdf = Gallery::create(['name' => $request->pdf->getClientOriginalName(), 'type' => 'PRODUCT',
                    'mime' => $request->pdf->getClientMimeType(), 'description' => '', 'path' => $path]);
                $data['pdf_id'] = $pdf->id;
            }
            $data['reading_order'] = Product::max('reading_order') + 1;
            $product = Product::create($data);
            DB::commit();
            $response = $this->responseSuccess('Producto creado',
                ['redirect' => route('product.index')]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $product = Product::find($id);
        return $this->responseSuccess('', $product);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['brands'] = Brand::all();
        $data['categories'] = Category::all();
        $data['product'] = Product::find($id);
        $data['productTypes'] = SubTable::getProductType();
        return view('admin.views/product/update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param ProductUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, ProductUpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only(['name', 'type_id', 'sku', 'category_id', 'status', 'brand_id', 'stock',
                'description', 'tag', 'model']);
            $data['slug'] = str_slug($request->name);

            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('product', 'public');
                $gallery = Gallery::create(['name' => $request->image->getClientOriginalName(), 'type' => 'PRODUCT',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path]);
                $data['gallery_id'] = $gallery->id;
            }

            if ($request->has('pdf')) {
                $path = '/storage/' . $request->pdf->store('product', 'public');
                $pdf = Gallery::create(['name' => $request->pdf->getClientOriginalName(), 'type' => 'PRODUCT',
                    'mime' => $request->pdf->getClientMimeType(), 'description' => '', 'path' => $path]);
                $data['pdf_id'] = $pdf->id;
            }

            Product::find($id)->update($data);
            DB::commit();
            $response = $this->responseSuccess('Producto actualizado',
                ['redirect' => route('product.index')]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $row = Product::find($id);
            Product::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
            $row->delete();
            $response = $this->responseSuccess('Producto eliminado', ['redirect' => route('product.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Product::count();

        $products = Product::with('brand', 'category')->orderBy('reading_order');
        if (isset($search['value'])) {
            $products = $products->where(function ($query) use ($search) {
                $query->orWhereHas('brand', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search['value'] . '%');
                });
                $query->orWhereHas('category', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search['value'] . '%');
                });
                $query->orWhere('name', 'like', '%' . $search['value'] . '%');
            });
        }
        $products = $length > 0 ? $products->paginate($length, ['*'], 'draw', $page) : $products->paginate($totalPosts);
        $data = [
            'status' => true,
            'products' => $products->items(),
            'draw' => $request->draw,
            'perPage' => $products->perPage(),
            'recordsTotal' => isset($search['value']) ? $products->total() : $totalPosts,
            'recordsFiltered' => $products->total(),
            'lastPage' => $products->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Product::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])->decrement('reading_order');
                } else {
                    Product::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])->increment('reading_order');
                }
                Product::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}