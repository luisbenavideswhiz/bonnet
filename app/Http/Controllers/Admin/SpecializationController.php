<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SpecializationStoreRequest;
use App\Models\SubTable;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SpecializationController extends Controller
{
    /**
     * @var string
     */
    private $redirect = '/admin/specialization';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['specializations'] = SubTable::getSpecialization();
        return view('admin.views.specialization.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SpecializationStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SpecializationStoreRequest $request)
    {
        try {
            $data['meta_key'] = 'specialization';
            $data['meta_value'] = $request->name;
            $data['reading_order'] = SubTable::where('meta_key', '=', 'specialization')->max('reading_order') + 1;
            SubTable::create($data);
            $response = $this->responseSuccess('Especialización Registrada', ['redirect' => $this->redirect]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $data = SubTable::find($id);
        return $this->responseSuccess('', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $data = SubTable::find($id);
        return $this->responseSuccess('', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @param SpecializationStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, SpecializationStoreRequest $request)
    {
        try {
            $data['meta_value'] = $request->name;
            SubTable::find($id)->update($data);
            $response = $this->responseSuccess('Especialización Actualizada', ['redirect' => $this->redirect]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $row = SubTable::find($id);
            if (count($row->contacts) === 0) {
                SubTable::where('meta_key', '=', 'specialization')->where('reading_order', '>', $row->reading_order)->decrement('reading_order');
                $row->delete();
                $response = $this->responseSuccess('Especialización Eliminada', ['redirect' => $this->redirect]);
            } else {
                $response = $this->responseError('', ['message' => 'No se puede eliminar por que cuenta con registros en mensajes']);
            }
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = SubTable::where('meta_key', '=', 'specialization')->count();

        $specializations = SubTable::where('meta_key', '=', 'specialization')->orderBy('reading_order');
        if (isset($search['value'])) {
            $specializations = $specializations->where(function ($query) use ($search) {
                $query->orWhere('meta_value', 'like', '%' . $search['value'] . '%');
            });
        }
        $specializations = $length > 0 ? $specializations->paginate($length, ['*'], 'draw', $page) : $specializations->paginate($totalPosts);
        $data = [
            'status' => true,
            'specializations' => $specializations->items(),
            'draw' => $request->draw,
            'perPage' => $specializations->perPage(),
            'recordsTotal' => isset($search['value']) ? $specializations->total() : $totalPosts,
            'recordsFiltered' => $specializations->total(),
            'lastPage' => $specializations->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    SubTable::where('meta_key', '=', 'specialization')->where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])->decrement('reading_order');
                } else {
                    SubTable::where('meta_key', '=', 'specialization')->where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])->increment('reading_order');
                }
                SubTable::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}
