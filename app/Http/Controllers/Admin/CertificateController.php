<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CertificateStoreRequest;
use App\Http\Requests\Admin\CertificateUpdateRequest;
use App\Models\Certificate;
use App\Services\Eloquent\CertificateService;
use App\Services\Eloquent\GalleryService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.certificate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CertificateStoreRequest $request
     * @param CertificateService $certificateService
     * @param GalleryService $galleryService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CertificateStoreRequest $request, CertificateService $certificateService,
                          GalleryService $galleryService)
    {
        $data = $request->only('name');
        $data['reading_order'] = $certificateService->maxReadingOrder() + 1;
        $path = '/storage/' . $request->image->store('category', 'public');
        $gallery = $galleryService->create(['name' => $request->image->getClientOriginalName(),
            'type' => 'CERTIFICATE', 'mime' => $request->image->getClientMimeType(), 'description' => '',
            'path' => $path]);
        $data['gallery_id'] = $gallery->id;
        $certificateService->create($data);
        return $this->responseSuccess('Certificado registrado',
            ['redirect' => route('certificate.index')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param CertificateService $certificateService
     * @return \Illuminate\Http\Response
     */
    public function edit($id, CertificateService $certificateService)
    {
        return $this->responseSuccess('', $certificateService->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CertificateUpdateRequest $request
     * @param  int $id
     * @param CertificateService $certificateService
     * @param GalleryService $galleryService
     * @return \Illuminate\Http\Response
     */
    public function update(CertificateUpdateRequest $request, $id, CertificateService $certificateService,
                           GalleryService $galleryService)
    {
        $data = $request->only('name');
        if ($request->has('image')) {
            $path = '/storage/' . $request->image->store('category', 'public');
            $gallery = $galleryService->create(['name' => $request->image->getClientOriginalName(),
                'type' => 'CERTIFICATE', 'mime' => $request->image->getClientMimeType(), 'description' => '',
                'path' => $path]);
            $data['gallery_id'] = $gallery->id;
        }
        $certificateService->find($id)->update($data);
        return $this->responseSuccess('Certificado actualizado',
            ['redirect' => route('certificate.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param CertificateService $certificateService
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, CertificateService $certificateService)
    {
        $row = $certificateService->find($id);
        $certificateService->decrementReadingOrder($row->reading_order);
        $row->delete();
        return $this->responseSuccess('Realizado', ['redirect' => route('certificate.index')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        empty($search['value']) ?: $search['value'] = htmlentities($search['value']);
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Certificate::orderBy('id', 'DESC')->count();

        $certificate = Certificate::orderBy('reading_order');
        if (isset($search['value'])) {
            $certificate = $certificate->where(function ($query) use ($search) {
                $query->orWhere('name', 'like', '%' . $search['value'] . '%');
            });
        }
        $certificate = $length > 0 ? $certificate->paginate($length, ['*'], 'draw', $page) : $certificate->paginate($totalPosts);
        $data = [
            'status' => true,
            'certificates' => $certificate->items(),
            'draw' => $request->draw,
            'perPage' => $certificate->perPage(),
            'recordsTotal' => isset($search['value']) ? $certificate->total() : $totalPosts,
            'recordsFiltered' => $certificate->total(),
            'lastPage' => $certificate->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Certificate::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])
                        ->decrement('reading_order');
                } else {
                    Certificate::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])
                        ->increment('reading_order');
                }
                Certificate::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}
