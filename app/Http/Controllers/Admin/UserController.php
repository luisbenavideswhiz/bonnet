<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UserStoreRequest;
use App\Http\Requests\Admin\UserUpdateRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\Database\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::where('id', '!=', Auth::user()->id)->where('email', '!=', 'support@bonnett.pe')->get();
        return view('admin.views.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['roles'] = Role::where('name', '!=', 'superadmin')->get();
        return view('admin.views.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        try {
            $data = $request->only('name', 'last_name', 'email');
            $data['password'] = bcrypt($request->password);
            $user = User::create($data);
            $user->assign($request->role);
            $response = $this->responseSuccess('Usuario Creado', ['redirect' => route('user.index')]);
        } catch (\Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = User::find($id);
        return $this->responseSuccess('', $row);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);
        $data['roles'] = Role::where('name', '!=', 'superadmin')->get();
        return view('admin.views.users.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $data = $request->only('name', 'last_name', 'email');
            $request->password === null ?: $data['password'] = bcrypt($request->password);
            $user = User::find($id);
            count($user->getRoles()) <= 0 ?: $user->retract($user->getRoles()[0]);
            $user->assign($request->role);
            $user->update($data);
            $response = $this->responseSuccess('Usuario Modificado', ['redirect' => route('user.index')]);
        } catch (\Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return $this->responseSuccess('Usuario Eliminado', ['redirect' => route('user.index')]);
    }
}
