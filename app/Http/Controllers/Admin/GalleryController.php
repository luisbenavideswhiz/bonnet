<?php

namespace App\Http\Controllers\Admin;

use App\Models\EventGallery;
use App\Models\Gallery;
use App\Models\GalleryPost;
use App\Models\GalleryProduct;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    /**
     * @param $type
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImagesByType($type, Request $request)
    {
        $files = [];
        $id = $request->has('id') ? '/' . $request->id : '';

        foreach ($request->gallery as $file) {
            $path = $file->store('gallery/' . $type . $id, 'public');
            $description = !is_null($request->description[0]) ? $request->description[0] : '';
            $gallery = Gallery::create(['name' => $file->getClientOriginalName(), 'type' => strtoupper($type),
                'mime' => $file->getClientMimeType(), 'description' => $description, 'path' => 'storage/' . $path,
            ]);

            if ($type == 'product') {
                GalleryProduct::create(['gallery_id' => $gallery->id, 'product_id' => $request->id,]);
            }

            if ($type == 'event') {
                EventGallery::create(['gallery_id' => $gallery->id, 'event_id' => $request->id,]);
            }

            if ($type == 'post') {
                GalleryPost::create(['gallery_id' => $gallery->id, 'post_id' => $request->id,]);
            }

            Image::make($gallery->path)->resize(80, 80)->save($gallery->thumbnail_src, 80);

            array_push($files, [
                'delete_type' => 'DELETE', 'delete_url' => url('admin/gallery/' . $type . '/' . $gallery->id),
                'name' => $gallery->name, 'size' => $gallery->size, 'type' => $gallery->mime, 'url' => $gallery->url,
                'thumbnail_url' => $gallery->thumbnail, 'description' => $gallery->description,
            ]);
        }
        return response()->json(['files' => $files]);
    }

    /**
     * @param $type
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteByType($type, $id)
    {
        $gallery = Gallery::where('type', '=', strtoupper($type))->where('id', '=', $id)->first();
        File::delete($gallery->path);
        File::delete($gallery->thumbnail_src);
        $gallery->delete();
        return $this->responseSuccess('Archivo Eliminado');
    }

    public function update($id, Request $request)
    {
        try {
            $data = $request->only('description');
            Gallery::find($id)->update($data);
            $response = $this->responseSuccess('Actualizado');
        } catch (\Exception $e) {
            $response = $this->responseSuccess($e->getMessage());
        }
        return $response;
    }
}
