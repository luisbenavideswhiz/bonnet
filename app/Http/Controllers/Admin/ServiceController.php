<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ServiceStoreRequest;
use App\Http\Requests\Admin\ServiceUpdateRequest;
use App\Models\Gallery;
use App\Models\Service;
use App\Models\ServiceDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.service.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.views.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ServiceStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ServiceStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('title', 'description', 'detail');
            $path = '/storage/' . $request->image->store('service', 'public');
            $gallery = Gallery::create([
                'name' => $request->image->getClientOriginalName(), 'type' => 'SERVICE',
                'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
            ]);
            $data['gallery_id'] = $gallery->id;
            $data['reading_order'] = Service::max('reading_order') + 1;
            Service::create($data);
            DB::commit();
            $response = $this->responseSuccess('Servicio creado', ['redirect' => route('service.index')]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        return $this->responseSuccess('', $service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['service'] = Service::find($id);
        return view('admin.views.service.update ', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServiceUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(ServiceUpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('title', 'description', 'detail');

            if ($request->has('image')) {
                $path = '/storage/' . $request->image->store('service', 'public');
                $gallery = Gallery::create([
                    'name' => $request->image->getClientOriginalName(), 'type' => 'SERVICE',
                    'mime' => $request->image->getClientMimeType(), 'description' => '', 'path' => $path,
                ]);
                $data['gallery_id'] = $gallery->id;
            }

            Service::find($id)->update($data);
            DB::commit();
            $response = $this->responseSuccess('Servicio Actualizado', ['redirect' => route('service.index')]);
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        try {
            $row = Service::find($id);
            Service::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
            $row->delete();
            ServiceDetail::where('service_id', '=', $id)->delete();
            $response = $this->responseSuccess('Servicio eliminado', ['redirect' => route('service.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Service::count();

        $services = Service::orderBy('reading_order');
        if (isset($search['value'])) {
            $services = $services->where(function ($query) use ($search) {
                $query->orWhere('name', 'like', '%' . $search['value'] . '%');
            });
        }
        $services = $length > 0 ? $services->paginate($length, ['*'], 'draw', $page) : $services->paginate($totalPosts);
        $data = [
            'status' => true,
            'services' => $services->items(),
            'draw' => $request->draw,
            'perPage' => $services->perPage(),
            'recordsTotal' => isset($search['value']) ? $services->total() : $totalPosts,
            'recordsFiltered' => $services->total(),
            'lastPage' => $services->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Service::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])->decrement('reading_order');
                } else {
                    Service::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])->increment('reading_order');
                }
                Service::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}