<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ProductTypeStoreRequest;
use App\Models\SubTable;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductTypeController extends Controller
{
    /**
     * @var string
     */
    public $redirect = '/admin/product/type';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.product.type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductTypeStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductTypeStoreRequest $request)
    {
        try {
            $data = $request->only('meta_value');
            $data['meta_key'] = 'product_type';
            $data['meta_option'] = str_slug($request->meta_value);
            $data['reading_order'] = SubTable::where('meta_key', '=', 'product_type')->max('reading_order') + 1;
            SubTable::create($data);
            $response = $this->responseSuccess('Tipo de prodcuto creado', ['redirect' => $this->redirect]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SubTable::find($id);
        return $this->responseSuccess('', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param ProductTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, ProductTypeStoreRequest $request)
    {
        try {
            $data = $request->only('meta_value');
            $data['meta_option'] = str_slug($request->meta_value);
            SubTable::find($id)->update($data);
            $response = $this->responseSuccess('Tipo de producto actualizado', ['redirect' => $this->redirect]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $row = SubTable::find($id);
            if (count($row->product) === 0) {
                SubTable::where('meta_key', '=', 'product_type')->where('reading_order', '>', $row->reading_order)->decrement('reading_order');
                $row->delete();
                $response = $this->responseSuccess('Tipo de producto eliminada', ['redirect' => $this->redirect]);
            } else {
                $response = $this->responseError('', ['message' => 'No se puede eliminar por que cuenta con productos']);
            }
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = SubTable::where('meta_key', '=', 'product_type')->count();

        $types = SubTable::where('meta_key', '=', 'product_type')->orderBy('reading_order');
        if (isset($search['value'])) {
            $types = $types->where(function ($query) use ($search) {
                $query->orWhere('meta_value', 'like', '%' . $search['value'] . '%');
            });
        }
        $types = $length > 0 ? $types->paginate($length, ['*'], 'draw', $page) : $types->paginate($totalPosts);
        $data = [
            'status' => true,
            'types' => $types->items(),
            'draw' => $request->draw,
            'perPage' => $types->perPage(),
            'recordsTotal' => isset($search['value']) ? $types->total() : $totalPosts,
            'recordsFiltered' => $types->total(),
            'lastPage' => $types->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    SubTable::where('meta_key', '=', 'product_type')->where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])->decrement('reading_order');
                } else {
                    SubTable::where('meta_key', '=', 'product_type')->where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])->increment('reading_order');
                }
                SubTable::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (\Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}
