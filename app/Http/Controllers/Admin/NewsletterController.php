<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\NewsletterStoreRequest;
use App\Http\Requests\Admin\NewsletterUpdateRequest;
use App\Http\Resources\NewslettersResource;
use App\Services\Eloquent\NewsletterPostService;
use App\Services\Eloquent\NewsletterService;
use App\Services\Eloquent\PostService;
use App\Services\Mailing\ApiWhiz;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.views.newsletter.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PostService $postService
     * @return \Illuminate\Http\Response
     */
    public function create(PostService $postService)
    {
        $data['posts'] = $postService->getAll();
        return view('admin.views.newsletter.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewsletterStoreRequest $request
     * @param NewsletterService $newsletterService
     * @param NewsletterPostService $newsletterPostService
     * @return \Illuminate\Http\Response
     */
    public function store(NewsletterStoreRequest $request, NewsletterService $newsletterService,
                          NewsletterPostService $newsletterPostService)
    {
        $data = $request->only('title', 'description', 'link', 'button_name', 'affair', 'preview_text');
        !isset($request->image) ?: $data['image'] = '/storage/' . $request->image->store('newsletter', 'public');
        $n = $newsletterService->create($data);
        foreach ($request->posts as $post) {
            $newsletterPostService->create(['newsletter_id' => $n->id, 'post_id' => $post]);
        }
        return $this->responseSuccess('Realizado', ['redirect' => route('newsletter.index')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param NewsletterService $newsletterService
     * @return \Illuminate\Http\Response
     */
    public function show($id, NewsletterService $newsletterService)
    {
        return $this->responseSuccess('', $newsletterService->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @param NewsletterService $newsletterService
     * @param PostService $postService
     * @return \Illuminate\Http\Response
     */
    public function edit($id, NewsletterService $newsletterService, PostService $postService)
    {
        $data['posts'] = $postService->getAll();
        $data['newsletter'] = $newsletterService->find($id);
        return view('admin.views.newsletter.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsletterUpdateRequest $request
     * @param  int $id
     * @param NewsletterService $newsletterService
     * @param NewsletterPostService $newsletterPostService
     * @return \Illuminate\Http\Response
     */
    public function update(NewsletterUpdateRequest $request, $id, NewsletterService $newsletterService,
                           NewsletterPostService $newsletterPostService)
    {
        $data = $request->only('title', 'description', 'link', 'button_name', 'affair', 'preview_text');
        !isset($request->image) ?: $data['image'] = '/storage/' . $request->image->store('newsletter', 'public');
        $n = $newsletterService->find($id);
        $n->update($data);
        $newsletterPostService->deleteAll($n->id);
        foreach ($request->posts as $post) {
            $newsletterPostService->create(['newsletter_id' => $n->id, 'post_id' => $post]);
        }
        return $this->responseSuccess('Realizado', ['redirect' => route('newsletter.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param NewsletterService $newsletterService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, NewsletterService $newsletterService)
    {
        $newsletterService->find($id)->delete();
        return $this->responseSuccess('Realizado', ['modal' => 'delete']);
    }

    /**
     * @param Request $request
     * @param NewsletterService $newsletterService
     * @return \Illuminate\Http\JsonResponse
     */
    public function listData(Request $request, NewsletterService $newsletterService)
    {
        $search = $request->get('search', []);
        empty($search['value']) ?: $search['value'] = htmlentities($search['value']);
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $data = $newsletterService->getAll($search, [], $length, $page);
        return $this->responseCollectionSuccess('', new NewslettersResource($data));
    }

    /**
     * @param $id
     * @param NewsletterService $newsletterService
     * @param ApiWhiz $apiWhiz
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function send($id, NewsletterService $newsletterService, ApiWhiz $apiWhiz)
    {
        $newsletter = $newsletterService->find($id);
        $apiWhiz->sendMailByList('default', [
            'email' => getenv('MAIL_FROM_ADDRESS'),
            'name' => getenv('MAIL_FROM_NAME')
        ],
            $newsletter->title,
            view('mailings.newsletter', ['newsletter' => $newsletter])->render(),
            [getenv('MAIL_ADMIN')]);
        return $this->responseSuccess('Se ha encolado el envio de newsletter');
    }

    /**
     * @param $id
     * @param NewsletterService $newsletterService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview($id, NewsletterService $newsletterService)
    {
        return view('mailings.newsletter', ['newsletter' => $newsletterService->find($id)]);
    }
}
