<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\RoleStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Silber\Bouncer\Database\Ability;
use Silber\Bouncer\Database\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['roles'] = Role::where('name', '!=', 'superadmin')->get();
        return view('admin.views.roles.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->getRoles()[0] === 'superadmin') {
            $data['abilities'] = Ability::where('name', '!=', '*')->get();
        } else {
            $data['abilities'] = Ability::where('name', '!=', '*')->where('name', '!=', 'manage_user')->get();
        }
        return view('admin.views.roles.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoleStoreRequest $request)
    {
        if ($request->name != 'superadmin') {
            $data = $request->only('name', 'title');
            $role = Bouncer::role()->create($data);
            if ($request->has('all')) {
                Bouncer::allow($role)->everything();
            } else {
                foreach ($request->ability as $key => $value) {
                    Bouncer::allow($role)->to($value);
                }
            }
            $response = $this->responseSuccess('Rol Creado', ['redirect' => route('role.index')]);
        } else {
            $response = $this->responseError('', ['message' => 'Rol Inválido']);
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        return $this->responseSuccess('', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['role'] = Role::find($id);
        $data['abilities'] = Ability::where('name', '!=', '*')->get();
        return view('admin.views.roles.update', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleStoreRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleStoreRequest $request, $id)
    {
        if ($request->name != 'superadmin') {
            $data = $request->only('name', 'title');
            $role = Role::find($id);
            foreach ($role->getAbilities() as $ability) {
                Bouncer::disallow($role)->to($ability->name);
            }
            $role->update($data);
            foreach ($request->ability as $key => $value) {
                Bouncer::allow($role)->to($value);
            }
            $response = $this->responseSuccess('Rol Actualizado', ['redirect' => route('role.index')]);
        } else {
            $response = $this->responseError('', ['message' => 'Rol Inválido']);
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if (count($role->users) === 0) {
            $role->delete();
            $response = $this->responseSuccess('Rol Eliminada', ['redirect' => route('role.index')]);
        } else {
            $response = $this->responseError('', ['message' => 'No se puede eliminar por que cuenta con usuarios']);
        }
        return $response;
    }
}
