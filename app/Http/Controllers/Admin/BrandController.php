<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BrandStoreRequest;
use App\Models\Brand;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    /**
     * @var string
     */
    private $redirect = '/admin/brand';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.views.brand.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BrandStoreRequest $request)
    {
        try {
            $data = $request->only('name');
            $data['slug'] = str_slug($data['name']);
            $data['reading_order'] = Brand::max('reading_order') + 1;
            Brand::create($data);
            $response = $this->responseSuccess('Marca Registrada', ['redirect' => route('brand.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $data = Brand::find($id);
        return $this->responseSuccess('', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @param BrandStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, BrandStoreRequest $request)
    {
        try {
            $data = $request->only('name');
            $data['slug'] = str_slug($data['name']);
            Brand::find($id)->update($data);
            $response = $this->responseSuccess('Marca Actualizada', ['redirect' => route('brand.index')]);
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $row = Brand::find($id);
            if (count($row->product) === 0) {
                Brand::where('reading_order', '>', $row->reading_order)->decrement('reading_order');
                $row->delete();
                $response = $this->responseSuccess('Marca Eliminada', route('brand.index'));
            } else {
                $response = $this->responseError('', ['message' => 'No se puede eliminar por que cuenta con productos']);
            }
        } catch (Exception $e) {
            $response = $this->responseError($e->getMessage());
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $search = $request->get('search', []);
        if (!empty($search['value'])) {
            $search['value'] = htmlentities($search['value']);
        }
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $totalPosts = Brand::count();

        $brands = Brand::orderBy('reading_order');
        if (isset($search['value'])) {
            $brands = $brands->where(function ($query) use ($search) {
                $query->orWhere('name', 'like', '%' . $search['value'] . '%');
            });
        }
        $brands = $length > 0 ? $brands->paginate($length, ['*'], 'draw', $page) : $brands->paginate($totalPosts);
        $data = [
            'status' => true,
            'brands' => $brands->items(),
            'draw' => $request->draw,
            'perPage' => $brands->perPage(),
            'recordsTotal' => isset($search['value']) ? $brands->total() : $totalPosts,
            'recordsFiltered' => $brands->total(),
            'lastPage' => $brands->lastPage()
        ];
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                if ($request->data['order_new'] > $request->data['order_old']) {
                    Brand::where('reading_order', '<=', $request->data['order_new'])
                        ->where('reading_order', '>', $request->data['order_old'])->decrement('reading_order');
                } else {
                    Brand::where('reading_order', '>=', $request->data['order_new'])
                        ->where('reading_order', '<', $request->data['order_old'])->increment('reading_order');
                }
                Brand::find($request->data['id'])->update(['reading_order' => $request->data['order_new']]);
            }
            DB::commit();
            $response = $this->responseSuccess('Modificado');
        } catch (Exception $e) {
            DB::rollBack();
            $response = $this->responseSuccess('Error');
        }
        return $response;
    }
}