<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use App\Models\SubTable;
use Illuminate\View\View;

class HeaderComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $data['banner_top'] = SubTable::getHeaderBannerTop();
        $data['menu_us'] = SubTable::getBannerTopMenuUs();
        $data['menu_product'] = SubTable::getBannerTopMenuProduct();
        $data['menu_service'] = SubTable::getBannerTopMenuService();
        $data['menu_contact'] = SubTable::getBannerTopMenuContact();
        $data['menu_event'] = SubTable::getBannerTopMenuEvent();
        $data['menu_blog'] = SubTable::getBannerTopMenuBlog();
        $view->with($data);
    }
}