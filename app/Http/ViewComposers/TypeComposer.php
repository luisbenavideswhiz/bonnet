<?php

namespace App\Http\ViewComposers;

use App\Models\Product;
use Illuminate\View\View;

class TypeComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $types = Product::join('sub_tables as s', 's.id', 'products.type_id')
                        ->distinct()
                        ->select('s.meta_value', 's.meta_option', 's.id', 'products.category_id')
                        ->orderBy('s.meta_value')
                        ->get();
        $view->with('types', $types);
    }
}