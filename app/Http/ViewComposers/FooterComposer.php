<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use App\Models\SubTable;
use Illuminate\View\View;

class FooterComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $footer = [
            'email' => SubTable::getFooterEmail(),
            'phone' => SubTable::getFooterPhoneArray(),
            'address' => SubTable::getFooterAddress(),
            'description' => SubTable::getFooterDescription(),
            'socialNetwork' => SubTable::getFooterSocialNetwork(),
            'quickLink' => SubTable::getFooterQuickLink(),
            'networks' => SubTable::getSocialNetwork(),
        ];
        $categories = Category::has('product')->get();
        $view->with(['footer' => $footer, 'categories' => $categories]);
    }
}