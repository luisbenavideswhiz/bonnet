<?php

namespace App\Http\Requests\Landing;

use Illuminate\Foundation\Http\FormRequest;

class FireProtectionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|digits:9',
            'email' => 'required|email',
            'product_interest' => 'required',
            'business_name' => 'required',
            'message' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'last_name' => 'apellido',
            'phone' => 'celular',
            'email' => 'email',
            'product_interest' => 'producto',
            'business_name' => 'empresa',
            'message' => 'mensaje',
        ];
    }

    public function messages()
    {
        return [
            'phone.digits' => 'El campo celular es incorrecto',
            'email.email' => 'El campo email es incorrecto',
        ];
    }
}
