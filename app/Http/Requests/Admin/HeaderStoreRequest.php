<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HeaderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meta_value' => '',
            'meta_option' => 'required',
            'meta_link' => 'required',
            'us' => 'required',
            'product' => 'required',
            'contact' => 'required',
            'service' => 'required',
            'event' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'meta_value' => '',
            'meta_option.required' => 'Texto es requerido',
            'meta_link.required' => 'Link es requerido',
            'us.required' => 'Nosotros es requerido',
            'product.required' => 'Productos es requerido',
            'contact.required' => 'Contactos es requerido',
            'service.required' => 'Servicios es requerido',
            'event.required' => 'Eventos es requerido',
        ];
    }
}
