<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'image' => 'dimensions:max_width=1000,max_height=700',
        ];
    }

    public function messages()
    {
        return [
            'description.required' => 'Descripción es requerido',
            'image.required' => 'Imagen es requerda',
            'image.dimensions' => 'Imagen debe ser de 1000 x 700',
        ];
    }
}
