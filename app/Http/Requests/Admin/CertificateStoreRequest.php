<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CertificateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'required|dimensions:max_width=270,max_height=380',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nombre es requerido',
            'image.required' => 'Imagen es requerido',
            'image.dimensions' => 'Imagen debe ser de 270 x 380',
        ];
    }
}
