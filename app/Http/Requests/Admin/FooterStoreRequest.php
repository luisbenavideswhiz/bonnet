<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FooterStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
            'description' => 'required',
            'url_text' => 'required',
            'url_link.*' => 'required|url',
            'social_network_id.*' => 'required',
            'social_network.*' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.email' => 'Email incorrecto',
            'email.required' => 'Email es requerido',
            'phone.required' => 'Teléfono es requerido',
            'address.required' => 'Dirección es requerido',
            'description.required' => 'Descripción es requerido',
            'url_text.required' => 'Título de enlace es requerido',
            'url_link.*.required' => 'Url de enlace es requrido',
            'url_link.*.url' => 'Url de enlace es incorrecto',
            'social_network_id.*.required' => 'Selecciona una red social',
            'social_network.*.required' => 'Texto en red social requerido',
        ];
    }
}
