<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EventUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'text' => 'required',
            'description' => 'required',
            'image' => 'dimensions:max_width=740,max_height=560',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Título es requrido',
            'text.required' => 'Descripción corta es requerido',
            'description.required' => 'Descripción completa es requerido',
            'image.dimensions' => 'Imagen debe ser de 740 x 560',
        ];
    }
}
