<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SecondSliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text_center' => 'required',
            'text_right' => 'required',
            'link_right' => 'required|url',
            'text_left' => 'required',
            'link_left' => 'required|url',
        ];
    }

    public function messages()
    {
        return [
            'text_center.required' => 'Texto central es requerido',
            'text_right.required' => 'Texto derecha es requerido',
            'link_right.required' => 'Link derecha es requerido',
            'link_right.url' => 'Link derecha incorrecto',
            'text_left.required' => 'Texto izquierda es requerido',
            'link_left.required' => 'Link izquierda es requerido',
            'link_left.url' => 'Link izquierda incorrecto',
        ];
    }
}
