<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NewsletterStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'link' => 'required',
            'button_name' => 'required',
            'affair' => 'required',
            'preview_text' => 'required',
            'image' => 'required|dimensions:max_width=700,max_height=340',
            'posts' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'título',
            'description' => 'descripción',
            'link' => 'link',
            'button_name' => 'nombre del boton',
            'affair' => 'asunto',
            'preview_text' => 'texto previo',
            'image' => 'imagen',
            'posts' => 'blogs',
        ];
    }
}
