<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|dimensions:max_width=540,max_height=640',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nombre es requerido',
            'description.required' => 'Descripción es requerido',
            'image.required' => 'Imagen es requerido',
            'image.dimensions' => 'Imagen debe ser de 540 x 640',
        ];
    }
}
