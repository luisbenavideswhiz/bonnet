<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HeadquarterStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'location' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nombre de sede es requerido',
            'phone.required' => 'Teléfono es requerido',
            'email.required' => 'Correo es requerido',
            'address.required' => 'Dirección es requerido',
            'location.required' => 'Seleccione la ubicación en el mapa',
        ];
    }
}
