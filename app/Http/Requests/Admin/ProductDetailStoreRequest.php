<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductDetailStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'image' => 'dimensions:max_width=540,max_height=560',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Nombre de detalle es requerido',
            'description.required' => 'Descripción es requerido',
            'image.dimensions' => 'La imagen debe ser de 270x270 px'
        ];
    }
}
