<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NewsletterUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'link' => 'required',
            'button_name' => 'required',
            'affair' => 'required',
            'preview_text' => 'required',
            'image' => '',
            'posts' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'título',
            'description' => 'descripción',
            'link' => 'link',
            'button_name' => 'nombre del boton',
            'affair' => 'asunto',
            'preview_text' => 'texto previo',
            'image' => 'imagen',
            'posts' => 'blogs',
        ];
    }
}
