<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class OurLinesUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => 'required',
            'name_button'   => 'required',
            'link'          => 'required'
        ];
    }

    public function messages()
    {
        return [
            'description.required'  => 'Descripción es requerido.',
            'name_button.required'  => 'Nombre del boton es requerido.',
            'link.required'         => 'Link del boton es requerido.'
        ];
    }
}
