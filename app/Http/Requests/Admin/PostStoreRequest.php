<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'image' => 'required|dimensions:max_width=1080,max_height=780',
            'tags' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'título',
            'slug' => 'ruta',
            'short_description' => 'descripción corta',
            'description' => 'detalle',
            'image' => 'imagen',
            'tags' => 'etiquetas'
        ];
    }
}
