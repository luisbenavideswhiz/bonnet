<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ServiceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'detail' => 'required',
            'image' => 'dimensions:max_width=1080,max_height=780',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Título es requrido',
            'description.required' => 'Descripción es requerido',
            'detail.required' => 'Detalle es requerido',
            'image.dimensions' => 'Imagen debe ser de 1080 x 780',
        ];
    }
}
