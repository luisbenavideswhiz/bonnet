<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SliderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => '',
            'button' => '',
            'description' => '',
            'url' => '',
            'image' => 'required|dimensions:max_width=1800,max_height=900',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Título es requerido',
            'button.required' => 'Nombre de botón es requerido',
            'description.required' => 'Descripción es requerido',
            'url.required' => 'Enlace es requerido',
            'url.url' => 'Enlace es inválido',
            'image.required' => 'Imagen es requerida',
            'image.dimensions' => 'Imagen debe ser de 1800 x 900',
        ];
    }
}
