<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProfileChangePassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'new_password' => 'required|different:old_password',
            'confirm_password' => 'required|same:new_password',
        ];
    }

    public function messages()
    {
        return [
            'old_password.required' => 'Contraseña actual es requerida',
            'new_password.required' => 'Contraseña nueva es requerida',
            'new_password.different' => 'Contraseña nueva debe ser diferente de la actual',
            'confirm_password.required' => 'Confirmacion de contraseña es requerida',
            'confirm_password.same' => 'Confirmacion de contraseña incorrecta',
        ];
    }
}
