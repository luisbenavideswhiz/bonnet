<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type_id' => 'required',
            'status' => 'required',
            'sku' => 'required',
            'brand_id' => 'required',
            'category_id' => 'required',
            'stock' => 'required|numeric',
            'description' => 'required',
            'tag' => 'required',
            'image' => 'dimensions:max_width=540,max_height=560',
            'pdf' => 'mimes:pdf|max:10000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nombre es requerido',
            'type_id.required' => 'Tipo de producto es requerido',
            'status.required' => 'Destacar producto es requerido',
            'sku.required' => 'SKU es requerido',
            'brand_id.required' => 'Marca es requerido',
            'category_id.required' => 'Categoría es requerido',
            'stock.required' => 'Stock es requerido',
            'description.required' => 'Descripción es requerido',
            'tag.required' => 'Etiquetas son requeridas',
            'image.dimensions' => 'Imagen debe ser de 540 x 560',
            'pdf' => 'PDF'
        ];
    }
}
