<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SeoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required',
            'name' => 'required',
            'title' => 'required|min:2',
            'alt' => '',
            'description' => 'required|min:2',
            'image' => '',
        ];
    }

    public function messages()
    {
        return [
            'category.required' => 'Categorías es requerido',
            'name.required' => 'Sección es requerido',
            'name.unique' => 'Esta sección de la web ya está en uso',
            'title.required' => 'Título es requerido',
            'title.min' => 'Título inválido',
            'alt.required' => 'alt es requerido',
            'description.required' => 'Descripción es requerido',
            'description.min' => 'Descripción inválida',
            'image' => '',
        ];
    }
}
