<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CorporationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|dimensions:max_width=170,max_height=56.17',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Título es requerido',
            'description.required' => 'Descrippción es requerido',
            'image.required' => 'Imagen es requerido',
            'image.dimensions' => 'Imagen debe ser de 170x56.17',
        ];
    }
}
