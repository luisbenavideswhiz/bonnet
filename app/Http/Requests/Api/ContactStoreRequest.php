<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class ContactStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'last_name' => 'required|string',
            'document' => 'required|digits:8',
            'business_document' => 'digits:11',
            'business_name' => '',
            'specialization_id' => 'required',
            'other' => 'required_if:specialization_id,==,0',
            'email' => 'required|email',
            'phone' => 'required|digits_between:7,9',
            'product_interest' => 'required|string',
            'how_contact' => 'required',
            'message' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nombre es requerido',
            'last_name.required' => 'Apellido es requerido',
            'document.required' => 'DNI es requerido',
            'document.digits' => 'DNI debe ser de 8 digitos',
            'business_document.required' => 'RUC es requerido',
            'business_document.digits' => 'RUC debe ser de 11 digitos',
            'business_name.required' => 'Nombre de la empresa es requerido',
            'specialization_id' => 'Especializacion es requerido',
            'email.required' => 'Email es requerido',
            'email.email' => 'Email es inválido',
            'phone.required' => 'Teléfono es requerido',
            'phone.digits_between' => 'Teléfono debe ser de 7 a 9 digitos',
            'product_interest.required' => 'Producto de interés es requerido',
            'how_contact.required' => 'Seleccione una opcion',
            'message.required' => 'Mensaje es requerido',
            'specialization_id.required' => 'Nivel de especialización es requerido',
            'other.required_if' => 'Otra especialización es requerido'
        ];
    }
}
