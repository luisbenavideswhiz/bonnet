<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class ComingSoon
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date1 = Carbon::now();
        $date2 = Carbon::create(2018, 10, 1, 17, 18, 00);

        if ($date1 <= $date2 && !$request->has('token')) {
            $response = redirect(route('web.coming'));
        } else {
            $response = $next($request);
        }
        return $response;
    }
}
