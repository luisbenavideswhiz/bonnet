<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductListResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'image' => isset($this->image->path) ? $this->image->path : '',
        ];
    }
}
