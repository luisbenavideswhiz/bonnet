<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\Resource;

class ProductSimpleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'model' => $this->model,
            'image' => isset($this->image->path) ? $this->image->path : '',
            'status' => $this->statusName,
            'type' => [
                'id' => $this->type_id,
                'slug' => $this->type->meta_option,
                'name' => $this->type->meta_value
            ],
            'brand' => [
                'id' => $this->brand_id,
                'slug' => $this->brand->slug,
                'name' => $this->brand->name
            ],
            'category' => [
                'id' => $this->category_id,
                'slug' => $this->category->slug,
                'name' => $this->category->name
            ],
        ];
    }
}