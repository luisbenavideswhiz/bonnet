<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SeoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category,
            'category_name' => $this->category_name,
            'name' => $this->name,
            'title' => $this->title,
            'url' => $this->url,
            'description' => $this->description,
            'alt' => $this->alt,
            'keywords' => $this->keywords,
            'image' => $this->image,
        ];
    }
}
