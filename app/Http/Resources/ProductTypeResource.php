<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductTypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->meta_option,
            'name' => $this->meta_value
        ];
    }
}
