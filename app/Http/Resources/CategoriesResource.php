<?php

namespace App\Http\Resources;

use App\Serializers\ResourceCollectionTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoriesResource extends ResourceCollection
{
    use ResourceCollectionTrait;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->responseResourceCollection('data', CategoryResource::collection($this->collection));
    }
}
