<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PostResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'date_publish' => $this->date_publish,
            'short_description' => $this->short_description,
            'description' => $this->description,
            'image' => $this->image,
            'tags' => $this->tags,
            'author' => $this->author,
            'reading_order' => $this->reading_order,
        ];
    }
}
