<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SliderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'button' => $this->button,
            'url' => $this->url,
            'path' => $this->image->path,
        ];
    }
}
