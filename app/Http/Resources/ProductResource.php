<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'sku' => $this->sku,
            'slug' => $this->slug,
            'name' => $this->name,
            'description' => $this->description,
            'tag' => $this->tag,
            'stock' => $this->stock,
            'image' => $this->image->path,
            'status' => $this->statusName,
            'type' => [
                'id' => $this->type_id,
                'name' => $this->type->meta_value
            ],
            'brand' => [
                'id' => $this->brand_id,
                'name' => $this->brand->name,
                'slug' => $this->brand->slug
            ],
            'category' => [
                'id' => $this->category_id,
                'name' => $this->category->name,
                'slug' => $this->category->slug,
            ],
            'gallery' => GalleryResource::collection($this->gallery),
            'detail' => ProductDetailResource::collection($this->detail),
        ];
    }

    public static function toSimple($product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'category' => [
                'id' => $product->category_id,
                'name' => $product->category->name
            ],
        ];
    }
}