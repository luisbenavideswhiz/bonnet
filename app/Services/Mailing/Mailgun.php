<?php

namespace App\Services\Mailing;


use App\Http\Clients\HttpClient;

class Mailgun
{

    private $client;
    private $version;
    private $url;
    private $domain;
    private $apiKey;


    /**
     * Pushy constructor.
     */
    public function __construct()
    {
        $this->client = app()->make(HttpClient::class);
        $this->url = 'https://api.mailgun.net';
        $this->version = 'v3';
        $this->setDomain();
        $this->setApiKey();
        $this->client->setHeaders(['Authorization'=>'Basic '.base64_encode("api:" . $this->apiKey)]);
    }

    /**
     * @param null $apiKey
     * @return $this
     */
    private function setApiKey($apiKey = null)
    {
        if(is_null($this->apiKey)){
            $this->apiKey = config('services.mailgun.secret');
        }else{
            $this->apiKey = $apiKey;
        }
        return $this;
    }

    /**
     * @param null $domain
     * @return $this
     */
    private function setDomain($domain = null)
    {
        if(is_null($this->domain)){
            $this->domain = config('services.mailgun.domain');
        }else{
            $this->domain = $domain;
        }
        return $this;
    }


    /**
     * @param string $part
     * @return string
     */
    private function getBasePath($part = '')
    {
        return $this->url.'/'.$this->version.'/'.$this->domain.$part;
    }


    /**
     * @param $eventType
     * @param $recipient
     * @param null $from
     * @param array $dates
     * @param null $next
     * @return mixed
     */
    public function events($eventType, $recipient = null, $from = null, $dates = [], $next=null)
    {
        if(!is_null($next)){
            $response = $this->client->get($next);
        }else{
            $query = [
                'event' => $eventType
            ];

            if(isset($recipient)){
                $query['recipient'] = $recipient;
            }

            if(isset($dates['begin'])){
                $query['begin'] = $dates['begin'];
                $query['ascending'] = !isset($dates['ascending'])?'yes':$dates['ascending'];
            }
            if(isset($dates['end'])){
                $query['end'] = $dates['end'];
                $query['ascending'] = !isset($dates['ascending'])?'yes':$dates['ascending'];
            }

            if(!is_null($from)){
                $query['from'] = $from;
            }
            $response = $this->client->get($this->getBasePath('/events'), $query);
        }
        return $response;
    }

    /**
     * @param $eventTypes
     * @param $start
     * @param $end
     * @return mixed
     */
    public function stats($eventTypes, $start, $end)
    {
        $query = [
            'event' => $eventTypes,
            'start' => $start,
            'end' => $end
        ];
        return $this->client->get($this->getBasePath('/stats/total'), $query);
    }

}
