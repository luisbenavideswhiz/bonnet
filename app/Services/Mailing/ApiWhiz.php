<?php

namespace App\Services\Mailing;

use App\Http\Clients\HttpClient;
use Exception;
use Illuminate\Support\Facades\Cache;

class ApiWhiz
{

    private $client;
    private $url;
    private $clientId;
    private $clientToken;
    private $accessToken;

    private $sender;
    private $addressee;
    private $subject;
    private $template;
    private $cc;

    /**
     * ApiWhiz constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->client = app()->make(HttpClient::class);
        $this->url = 'https://api.whiz.pe';
        $this->clientId = env('CLIENT_ID');
        $this->clientToken = env('CLIENT_SECRET');
        $this->accessToken = $this->getAccessToken();
        $this->cc = [];
    }

    /**
     * @throws Exception
     */
    private function getAccessToken()
    {
        if(!Cache::has('access_token')){
            $response =  $this->client->post($this->url.'/oauth/token', [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientToken,
                'scope' => '*'
            ]);
            if(!$response->response->status){
                throw new Exception('Not working oauth');
            }
            cache(['access_token' => $response->response->access_token], now()->addDay());
        }
        return cache('access_token');
    }

    /**
     * @param $sender
     * @param $addressee
     * @param $subject
     * @param $template
     * @return $this
     * @throws \Throwable
     */
    public function prepareMail($subject, $sender, $addressee, $template = null)
    {
        $this->sender = $sender;
        $this->addressee = $addressee;
        $this->subject = $subject;
        if(!is_null($template)){
            $this->template = $template;
        }
        return $this;
    }

    /**
     * @param $view
     * @param $data
     * @return ApiWhiz
     * @throws \Throwable
     */
    public function setView($view, $data = [])
    {
        $this->template = view($view, $data)->render();
        return $this;
    }

    public function addCC($email, $name)
    {
        $this->cc[] = ['email' => $email, 'name' => $name];
        return $this;
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function sendMail()
    {
        $this->client->addHeader('Authorization', 'Bearer '.$this->accessToken);
        throw_if(is_null($this->template), new Exception('view is undefined'));
        $data = [
            'sender' => $this->sender,
            'addressee' => $this->addressee,
            'subject' => $this->subject,
            'template' => $this->template
        ];
        if(!is_null($this->cc) && count($this->cc) > 0){
            $data['cc'] = $this->cc;
        }
        return $this->client->post($this->url.'/v1/mail/send', $data);
    }

    public function sendMailByList($list = 'default', $sender, $subject, $template, $except = [])
    {
        $this->client->addHeader('Authorization', 'Bearer '.$this->accessToken);
        return $this->client->post($this->url.'/v1/list/'.$list.'/mail/send', [
            'sender' => $sender,
            'subject' => $subject,
            'template' => $template,
            'excepts' => $except
        ]);
    }
}