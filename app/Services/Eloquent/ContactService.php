<?php

namespace App\Services\Eloquent;

use App\Models\Contact;

class ContactService extends BaseService
{
    /**
     * CategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct(Contact::class);
    }
}