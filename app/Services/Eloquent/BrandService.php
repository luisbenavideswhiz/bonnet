<?php

namespace App\Services\Eloquent;

use App\Models\Brand;

class BrandService extends BaseService
{
    /**
     * CategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct(Brand::class);
    }
}