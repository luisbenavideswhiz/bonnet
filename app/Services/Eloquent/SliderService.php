<?php

namespace App\Services\Eloquent;

use App\Models\Slider;

class SliderService extends BaseService
{
    /**
     * ProductService constructor.
     */
    public function __construct()
    {
        parent::__construct(Slider::class);
    }

    public function find($id)
    {
        return $this->getModel()->where('id', '=', $id)->first();
    }

    /**
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        return $data->select($fields)->orderBy('reading_order')->paginate($limit);
    }
}