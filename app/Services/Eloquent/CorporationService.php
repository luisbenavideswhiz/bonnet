<?php

namespace App\Services\Eloquent;

use App\Models\Corporation;

class CorporationService extends BaseService
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct(Corporation::class);
    }

    /**
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        return $data->select($fields)->orderBy('reading_order')->paginate($limit);
    }
}