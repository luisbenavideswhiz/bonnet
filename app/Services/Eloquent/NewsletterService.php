<?php

namespace App\Services\Eloquent;

use App\Models\Newsletter;

class NewsletterService extends BaseService
{
    /**
     * ServiceService constructor.
     */
    public function __construct()
    {
        parent::__construct(Newsletter::class);
    }

    /**
     * @param array $filters
     * @param array $order
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($filters = [], $order = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('title', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->paginate($limit, ['*'], 'draw', $page);
    }

    /**
     * @param array $filters
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getFilter($filters = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('title', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->paginate($limit, ['*'], 'draw', $page);
    }
}