<?php

namespace App\Services\Eloquent;

use App\Models\Product;

class ProductService extends BaseService
{
    /**
     * ProductService constructor.
     */
    public function __construct()
    {
        parent::__construct(Product::class);
    }

    public function find($id)
    {
        return $this->getModel()->where('id','=', $id)->first();
    }

    /**
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @param null $category
     * @return mixed
     */
    public function getAll($category = null, $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel()->where('status', '!=', 0);
        if(isset($category)){
            $data = $data->where('category_id', '=', $category);
        }
        return $data->select($fields)->orderBy('reading_order')->paginate($limit);
    }

    public function getFilter($filters = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('name', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->paginate($limit, ['*'], 'draw', $page);
    }

    public function getList($category)
    {
        return $this->getModel()->where('category_id', $category)
                                ->join('categories as c', 'c.id', '=', 'products.category_id')
                                ->distinct()
                                ->select('c.id', 'c.name')
                                ->get();
    }
}