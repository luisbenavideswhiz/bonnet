<?php

namespace App\Services\Eloquent;

use App\Models\Seo;

class SeoService extends BaseService
{
    /**
     * SeoService constructor.
     */
    public function __construct()
    {
        parent::__construct(Seo::class);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function existName($name)
    {
        return count($this->getModel()->where('name', '=', $name)->first()) == 1 ? true : false;
    }

    /**
     * @return mixed
     */
    public function count()
    {
        return $this->getModel()->count();
    }

    /**
     * @param array $filters
     * @param array $order
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($filters = [], $order = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('name', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->paginate($limit, ['*'], 'draw', $page);
    }
}