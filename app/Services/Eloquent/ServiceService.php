<?php

namespace App\Services\Eloquent;

use App\Models\Service;

class ServiceService extends BaseService
{
    /**
     * ServiceService constructor.
     */
    public function __construct()
    {
        parent::__construct(Service::class);
    }

    /**
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        return $data->select($fields)->orderBy('reading_order')->paginate($limit);
    }
}