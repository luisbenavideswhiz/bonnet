<?php

namespace App\Services\Eloquent;

use App\Models\SubTable;

class SpecializationService extends BaseService
{
    /**
     * CategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct(SubTable::class);
    }
}