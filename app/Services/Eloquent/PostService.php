<?php

namespace App\Services\Eloquent;

use App\Models\Post;

class PostService extends BaseService
{
    /**
     * PostService constructor.
     */
    public function __construct()
    {
        parent::__construct(Post::class);
    }

    /**
     * @param array $filters
     * @param array $order
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($filters = [], $order = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel()->with('gallery');
        if (isset($filters['post'])) {
            $data->where('id', '!=', $filters['post']);
        }
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('title', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->select($fields)->orderBy('reading_order')->paginate($limit);
    }

    /**
     * @return mixed
     */
    public function maxOrder()
    {
        return $this->getModel()->max('reading_order');
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findSlug($slug)
    {
        return $this->getModel()->where('slug', '=', $slug)->first();
    }

    public function comments($id, $page = 1)
    {
        return $this->getModel()->find($id)->comments()->paginate(6);
    }
}