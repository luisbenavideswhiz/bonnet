<?php

namespace App\Services\Eloquent;

use App\Models\Gallery;

class GalleryService extends BaseService
{
    /**
     * CategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct(Gallery::class);
    }
}