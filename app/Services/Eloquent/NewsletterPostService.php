<?php

namespace App\Services\Eloquent;

use App\Models\NewsletterPost;

class NewsletterPostService extends BaseService
{
    /**
     * ServiceService constructor.
     */
    public function __construct()
    {
        parent::__construct(NewsletterPost::class);
    }

    /**
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        return $data->select($fields)->paginate($limit);
    }

    /**
     * @param array $filters
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getFilter($filters = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('title', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->paginate($limit, ['*'], 'draw', $page);
    }

    public function deleteAll($id)
    {
        return $this->getModel()->where('newsletter_id', '=', $id)->delete();
    }
}