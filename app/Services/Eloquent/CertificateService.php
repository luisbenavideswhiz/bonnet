<?php

namespace App\Services\Eloquent;

use App\Models\Certificate;

class CertificateService extends BaseService
{
    /**
     * CategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct(Certificate::class);
    }

    /**
     * @param array $filters
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($filters = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('name', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->select($fields)->orderBy('reading_order')->paginate($limit);
    }

    public function find($id)
    {
        return $this->getModel()->with('image')->find($id);
    }

    public function maxReadingOrder()
    {
        return $this->getModel()->max('reading_order') == null ? 0 : $this->getModel()->max('reading_order');
    }

    public function decrementReadingOrder($order)
    {
        return $this->getModel()->where('reading_order', '>', $order)->decrement('reading_order');
    }
}