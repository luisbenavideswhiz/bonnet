<?php

namespace App\Services\Eloquent;

use App\Models\Category;

class CategoryService extends BaseService
{
    /**
     * CategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct(Category::class);
    }

    /**
     * @param int $limit
     * @param int $page
     * @param array $fields
     * @return mixed
     */
    public function getAll($limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        return $data->has('product')->select($fields)->orderBy('reading_order')->paginate($limit);
    }

    public function getFilter($filters = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel();
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('name', 'like', '%' . $filters['value'] . '%');
            });
        }
        return $data->paginate($limit, ['*'], 'draw', $page);
    }
}