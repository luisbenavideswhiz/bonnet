<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $table = 'posts';
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date_publish'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['title', 'slug', 'date_publish', 'short_description', 'description', 'image', 'tags',
        'author', 'reading_order'];
    public $timestamps = true;

    public function gallery()
    {
        return $this->belongsToMany(Gallery::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }
}