<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Headquarter extends Model
{
    use SoftDeletes;
    protected $table = 'headquarters';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['name', 'address', 'phone', 'email', 'longitude', 'latitude', 'reading_order'];
    public $timestamps = true;

}