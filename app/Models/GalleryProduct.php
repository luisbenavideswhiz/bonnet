<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GalleryProduct extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'gallery_product';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['gallery_id', 'product_id'];
}
