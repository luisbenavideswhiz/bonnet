<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Corporation extends Model
{
    use SoftDeletes;
    protected $table = 'corporations';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['title', 'description', 'gallery_id', 'reading_order'];
    public $timestamps = true;

    public function image()
    {
        return $this->belongsTo(Gallery::class, 'gallery_id');
    }
}
