<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newsletter extends Model
{
    use SoftDeletes;
    protected $table = 'newsletters';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['title', 'description', 'link', 'button_name', 'affair', 'preview_text', 'image'];
    public $timestamps = true;

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
