<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['name', 'slug', 'description', 'sku', 'tag', 'type_id', 'category_id',
        'brand_id', 'gallery_id', 'pdf_id', 'status', 'stock', 'reading_order', 'model'];
    public $timestamps = true;

    /**
     * get the status in string
     * @return string
     */
    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 2:
                $response = 'Destacado';
                break;
            case 1:
                $response = 'Publicado';
                break;
            default:
                $response = 'Borrador';
                break;
        }
        return $response;
    }

    protected $appends = ['status_name'];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function detail()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function gallery()
    {
        return $this->belongsToMany(Gallery::class);
    }

    public function pdf()
    {
        return $this->belongsTo(Gallery::class, 'pdf_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(Gallery::class, 'gallery_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(SubTable::class, 'type_id', 'id');
    }
}