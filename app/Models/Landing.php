<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Landing extends Model
{
    use SoftDeletes;
    protected $table = 'landings';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['name', 'slug'];
    public $timestamps = true;

    public function contacts()
    {
        return $this->belongsToMany(Contact::class);
    }
}
