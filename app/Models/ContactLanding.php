<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactLanding extends Model
{
    protected $table = 'contact_landing';
    protected $fillable = ['contact_id', 'landing_id'];
    public $timestamps = true;
}
