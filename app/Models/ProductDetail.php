<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDetail extends Model
{

    use SoftDeletes;
    protected $table = 'product_details';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['product_id', 'description', 'title', 'pdf_id', 'gallery_id'];
    public $timestamps = true;

    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function pdf()
    {
        return $this->belongsTo(Gallery::class, 'pdf_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(Gallery::class, 'gallery_id');
    }

}