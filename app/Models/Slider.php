<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;
    protected $table = 'sliders';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['title', 'button', 'description', 'url', 'gallery_id', 'reading_order'];
    public $timestamps = true;

    public function image()
    {
        return $this->belongsTo(Gallery::class, 'gallery_id');
    }
}
