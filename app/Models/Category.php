<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['name', 'slug', 'parent_id', 'level', 'description', 'gallery_id', 'reading_order'];
    public $timestamps = true;

    public function product()
    {
        return $this->hasMany('App\Models\Product')->orderBy('reading_order');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Gallery', 'gallery_id');
    }
}