<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seo extends Model
{
    use SoftDeletes;
    protected $table = 'seo';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['category', 'name', 'title', 'description', 'image', 'url', 'alt', 'keywords'];
    protected $appends = ['category_name'];
    public $timestamps = true;

    public function getCategoryNameAttribute()
    {
        switch ($this->category) {
            case 'web':
                return 'Sección de la web';
            case 'main':
                return 'Categorías Pricipales';
            case 'event':
                return 'Eventos';
            default:
                return 'Productos';
        }
    }

    public function getFullNameAttribute()
    {
        switch ($this->category) {
            case 'web':
                $data = collect([
                    'home' => 'Inicio',
                    'us' => 'Nosotros',
                    'event' => 'Eventos',
                    'history' => 'Historia',
                    'product' => 'Productos',
                    'service' => 'Servicios',
                    'contact' => 'Contactos',
                    'politic' => 'Politicas de Calidad',
                    'certification' => 'Certificaciones'
                ]);
                return $data->get($this->name);
            case 'main':
                return Category::where('slug', '=', $this->name)->first()->name;
            case 'product':
                return Product::where('slug', '=', $this->name)->first()->name;
            case 'event':
                return Event::where('slug', '=', $this->name)->first()->title;
        }
    }
}