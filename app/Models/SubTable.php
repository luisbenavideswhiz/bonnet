<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubTable extends Model
{
    use SoftDeletes;
    protected $table = 'sub_tables';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['meta_key', 'meta_value', 'meta_option', 'meta_link', 'reading_order'];
    public $timestamps = true;

    public static function getProductType()
    {
        return SubTable::where('meta_key', '=', 'product_type')->get();
    }

    public static function getSocialNetwork()
    {
        return SubTable::where('meta_key', '=', 'social_network')->get();
    }

    public static function getSpecialization()
    {
        return SubTable::where('meta_key', '=', 'specialization')->orderBy('reading_order')->get();
    }

    public static function getHowContact()
    {
        return SubTable::where('meta_key', '=', 'how_contact')->orderBy('reading_order')->get();
    }

    public static function getFooterEmail()
    {
        $row = SubTable::where('meta_key', '=', 'footer_email')->first();
        return !isset($row) ? '' : $row->meta_value;
    }

    public static function getFooterPhone()
    {
        $row = SubTable::where('meta_key', '=', 'footer_phone')->first();
        return !isset($row) ? '' : $row->meta_value;
    }

    public static function getFooterPhoneArray()
    {
        $row = SubTable::where('meta_key', '=', 'footer_phone')->first();
        return !isset($row) ? [] : explode(',', $row->meta_value);
    }

    public static function getFooterAddress()
    {
        $row = SubTable::where('meta_key', '=', 'footer_address')->first();
        return !isset($row) ? '' : $row->meta_value;
    }

    public static function getFooterDescription()
    {
        $row = SubTable::where('meta_key', '=', 'footer_description')->first();
        return !isset($row) ? '' : $row->meta_option;
    }

    public static function getFooterSocialNetwork()
    {
        $rows = SubTable::where('meta_key', '=', 'footer_social_network')->get();
        return $rows->count() < 0 ? '' : $rows;
    }

    public static function getFooterQuickLink()
    {
        $rows = SubTable::where('meta_key', '=', 'footer_quick_link')->get();
        return $rows->count() < 0 ? '' : $rows;
    }

    public static function getHeaderBannerTop()
    {
        $row = SubTable::where('meta_key', '=', 'header_banner_top')->first();
        return !isset($row) ? 0 : $row;
    }

    public static function getEventBanner()
    {
        return SubTable::where('meta_key', '=', 'event_banner')->first();
    }

    public static function getServiceBanner()
    {
        return SubTable::where('meta_key', '=', 'service_banner')->first();
    }

    public static function getSecondBannerCenter()
    {
        return SubTable::where('meta_key', '=', 'second_slider_center')->first();
    }

    public static function getSecondBannerRight()
    {
        return SubTable::where('meta_key', '=', 'second_slider_right')->first();
    }

    public static function getSecondBannerLeft()
    {
        return SubTable::where('meta_key', '=', 'second_slider_left')->first();
    }

    public static function getDescriptive()
    {
        return SubTable::where('meta_key', '=', 'descriptive')->first();
    }

    public static function getLink()
    {
        return SubTable::where('meta_key', '=', 'link')->first();
    }

    public static function getNameButton()
    {
        return SubTable::where('meta_key', '=', 'name_button')->first();
    }

    public static function getShowLink()
    {
        return SubTable::where('meta_key', '=', 'show_link')->first();
    }

    public static function getBannerTopMenuUs()
    {
        return SubTable::where('meta_key', '=', 'header_banner_top_us')->first();
    }

    public static function getBannerTopMenuProduct()
    {
        return SubTable::where('meta_key', '=', 'header_banner_top_product')->first();
    }

    public static function getBannerTopMenuContact()
    {
        return SubTable::where('meta_key', '=', 'header_banner_top_contact')->first();
    }

    public static function getBannerTopMenuBlog()
    {
        return SubTable::where('meta_key', '=', 'header_banner_top_blog')->first();
    }

    public static function getBannerTopMenuEvent()
    {
        return SubTable::where('meta_key', '=', 'header_banner_top_event')->first();
    }

    public static function getBannerTopMenuService()
    {
        return SubTable::where('meta_key', '=', 'header_banner_top_service')->first();
    }

    public function network()
    {
        return $this->hasOne(SubTable::class, 'id', 'meta_value');
    }

    public function product()
    {
        return $this->hasMany(Product::class, 'type_id');
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'specialization_id');
    }
}