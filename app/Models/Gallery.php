<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;

class Gallery extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $table = 'galleries';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['name', 'type', 'mime', 'description', 'path'];

    /**
     * @param $bytes
     * @return string
     */
    private static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }
        return round($bytes, 2) . ' ' . $units[$i];
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getThumbnailAttribute()
    {
        return url(str_replace(File::name('storage/' . $this->path) . '.' . File::extension('storage/' . $this->path), File::name('storage/' . $this->path) . '_80x80.' . File::extension('storage/' . $this->path), $this->path));
    }

    /**
     * @return string
     */
    public function getThumbnailSrcAttribute()
    {
        return public_path(str_replace(File::name($this->path) . '.' . File::extension($this->path), File::name($this->path) . '_80x80.' . File::extension($this->path), $this->path));
    }

    /**
     * @return string
     */
    public function getSizeAttribute()
    {
        return self::bytesToHuman(File::size($this->path));
    }

    /**
     * @return mixed
     */
    public function getMimetypeAttribute()
    {
        return File::mimeType($this->path);
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUrlAttribute()
    {
        return url($this->path);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public static function getAlliances()
    {
        return Gallery::where('type', '=', 'ALLIANCE')->get();
    }
}