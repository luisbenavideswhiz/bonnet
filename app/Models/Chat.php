<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $connection = 'chat';

    protected $table = 'mirrormx_customer_chat_user';
}
