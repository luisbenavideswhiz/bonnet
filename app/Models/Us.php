<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Us extends Model
{
    use SoftDeletes;
    protected $table = 'us';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['type', 'description', 'gallery_id'];
    public $timestamps = true;

    public function image()
    {
        return $this->belongsTo('App\Models\Gallery', 'gallery_id', 'id');
    }

    public static function getEnterprise()
    {
        return Us::where('type', '=', 'ENTERPRISE')->first();
    }

    public static function getMission()
    {
        return Us::where('type', '=', 'MISSION')->first();
    }

    public static function getVision()
    {
        return Us::where('type', '=', 'VISION')->first();
    }

    public static function getHolding()
    {
        return Us::where('type', '=', 'HOLDING')->first();
    }

    public static function getPolicy()
    {
        return Us::where('type', '=', 'POLICY')->first();
    }

    public static function getHistory()
    {
        return Us::where('type', '=', 'HISTORY')->first();
    }
}
