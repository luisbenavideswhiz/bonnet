<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{

    use SoftDeletes;
    protected $table = 'contacts';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $fillable = ['name', 'last_name', 'document', 'business_document', 'business_name',
        'specialization_id', 'email', 'phone', 'product_interest', 'message', 'how_contact', 'other', 'landing_id'];
    public $timestamps = true;

    public function specialization()
    {
        return $this->belongsTo(SubTable::class, 'specialization_id');
    }

    public function howContact()
    {
        return $this->belongsTo(SubTable::class, 'how_contact');
    }

    public function landing()
    {
        return $this->belongsTo(Landing::class);
    }

}