<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $table = 'events';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['title', 'slug', 'text', 'description', 'gallery_id', 'reading_order'];
    public $timestamps = true;

    public function image()
    {
        return $this->belongsTo('App\Models\Gallery', 'gallery_id', 'id');
    }

    public function gallery()
    {
        return $this->belongsToMany('App\Models\Gallery');
    }
}
