<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceDetail extends Model
{
    use SoftDeletes;
    protected $table = 'service_detail';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['service_id', 'title', 'description'];
    public $timestamps = true;

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }
}
