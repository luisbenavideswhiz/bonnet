<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterPost extends Model
{
    protected $table = 'newsletter_post';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['newsletter_id', 'post_id'];
    public $timestamps = true;

    public function newsletter()
    {
        return $this->hasOne(Newsletter::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
