<?php

namespace App\Exports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\FromCollection;

class ContactsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Contact::select('id', 'name', 'last_name', 'document', 'business_document', 'business_name', 'email', 'phone', 'product_interest', 'message', 'created_at')->get();
    }
}
