<?php

namespace App\Exports;

use App\Models\Chat;
use Maatwebsite\Excel\Concerns\FromCollection;

class ChatsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Chat::select('name', 'mail', 'last_activity', 'info')->get();
    }
}
