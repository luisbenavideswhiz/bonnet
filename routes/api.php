<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::get('brands', 'BrandController@productBrands');
Route::get('categories/list', 'CategoryController@home');
Route::get('categories', 'CategoryController@index');
Route::get('types', 'ProductController@productTypes');
Route::get('headquarters', 'HeadquarterController@index');
Route::get('specializations', 'SpecializationController@index');
Route::get('how-contacts', 'HowContactController@index');
Route::get('event/banner', 'EventController@banner');
Route::get('events', 'EventController@index');
Route::get('service/banner', 'ServiceController@banner');
Route::get('services', 'ServiceController@index');
Route::post('contact', 'ContactController@store');

Route::get('products/list', 'ProductController@home');
Route::get('products/search', 'ProductController@search');
Route::get('products/category/{slug}', 'ProductController@productCategory');
Route::apiResource('products', 'ProductController');

Route::group(['prefix' => 'home'], function () {
    Route::get('sliders', 'HomeController@slider');
});