<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('chat/download', 'Admin\ChatController@download')->name('chat.download');

Route::namespace('Web')->group(function () {
    Route::get('/proximamente', 'WebController@coming')->name('web.coming');

    Route::get('/', 'WebController@index')->name('web.home');
    Route::get('nosotros', 'WebController@about')->name('web.about');
    Route::get('contacto', 'ContactController@index')->name('web.contact');

    Route::get('contacto/gracias', 'WebController@thanks')->name('web.contact.thanks');
    Route::get('eventos', 'WebController@events')->name('web.events.index');
    Route::post('eventos/loadmore', 'WebController@event_ajax')->name('web.events.load');
    Route::get('evento/{slug}', 'WebController@eventDetail')->name('web.events.show');
    Route::get('servicios', 'WebController@services')->name('web.services.index');
    Route::get('productos', 'ProductController@index')->name('web.products');
    Route::get('producto/buscar', 'WebController@product_filter')->name('web.product_filter');
    Route::get('producto/{slug?}', 'WebController@product_detail')->name('web.product_detail');
    Route::get('categoria', 'WebController@category')->name('web.category');
    Route::get('politicas', 'WebController@politics')->name('web.politics');
    Route::get('historia', 'WebController@history')->name('web.history');
    Route::get('certificaciones', 'WebController@certifications')->name('web.certifications');
    Route::post('category-products', 'ProductController@moreProductForCategory');
    Route::get('blogs', 'WebController@posts')->name('web.posts');
    Route::get('blog/{slug}', 'WebController@post')->name('web.post');
    Route::post('comment/{post}', 'CommentController@store')->name('web.comment.store');
    Route::post('more-comment', 'CommentController@more')->name('web.comment.more');
});


Route::post('/login', 'Auth\LoginController@login')->name('login.verify');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout');
});

Route::get('newsletter', function () {
    $data['newsletter'] = \App\Models\Newsletter::find(1);
    return view('mailings.newsletter', $data);
});