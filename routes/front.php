<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('front.home');
Route::get('about', 'FrontController@about')->name('front.about');
Route::get('contact', 'FrontController@contact')->name('front.contact');
Route::get('events', 'FrontController@events')->name('front.events');
Route::get('event-detail', 'FrontController@event_detail')->name('front.event_detail');
Route::get('services', 'FrontController@services')->name('front.services');
Route::get('products', 'FrontController@products')->name('front.products');
Route::get('product-detail', 'FrontController@product_detail')->name('front.product_detail');
Route::get('product-filter', 'FrontController@product_filter')->name('front.product_filter');
Route::get('category', 'FrontController@category')->name('front.category');
Route::get('error-404', 'FrontController@error_404')->name('front.error_404');
Route::get('politics', 'FrontController@politics')->name('front.politics');
Route::get('certifications', 'FrontController@certifications')->name('front.certifications');
Route::get('search', 'FrontController@search')->name('front.search');
Route::get('no-product', 'FrontController@no_product')->name('front.no_product');
Route::get('thanks', 'FrontController@thanks')->name('front.thanks');