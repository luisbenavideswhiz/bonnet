<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', 'AdminController@login')->name('login');
});
Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'home'], function () {
        Route::get('/', 'AdminController@index')->name('admin.home');
        Route::get('/alliance', 'AdminController@alliance')->name('admin.alliance');
        Route::get('/footer', 'AdminController@footer')->name('admin.footer');
        Route::get('/event', 'AdminController@event')->name('event.banner');
        Route::get('/service', 'AdminController@service')->name('service.banner');
        Route::get('/header', 'AdminController@header')->name('admin.header');
        Route::get('/descriptive', 'AdminController@descriptive')->name('admin.descriptive');
        Route::post('/header', 'AdminController@postHeader')->name('admin.header.store');
        Route::post('/footer', 'AdminController@postFooter');
        Route::post('/event', 'AdminController@postEvent');
        Route::post('/service', 'AdminController@postService');
        Route::post('/descriptive', 'AdminController@postDescriptive')->name('admin.descriptive.store');
    });

    Route::group(['prefix' => 'table'], function () {
        Route::get('brand', 'BrandController@getList');
        Route::get('category', 'CategoryController@getList');
        Route::get('certificate', 'CertificateController@getList');
        Route::get('corporation', 'CorporationController@getList');
        Route::get('event', 'EventController@getList');
        Route::get('headquarter', 'HeadquarterController@getList');
        Route::get('how-contact', 'HowContactController@getList');
        Route::get('type', 'ProductTypeController@getList');
        Route::get('product', 'ProductController@getList');
        Route::get('service', 'ServiceController@getList');
        Route::get('slider', 'SliderController@getList');
        Route::get('specialization', 'SpecializationController@getList');
        Route::get('seo', 'SeoController@listData');
        Route::get('post', 'PostController@listData');
        Route::get('newsletter', 'NewsletterController@listData');
    });

    Route::group(['prefix' => 'reorder'], function () {
        Route::post('brand', 'BrandController@reorder');
        Route::post('category', 'CategoryController@reorder');
        Route::post('certificate', 'CertificateController@reorder');
        Route::post('corporation', 'CorporationController@reorder');
        Route::post('event', 'EventController@reorder');
        Route::post('headquarter', 'HeadquarterController@reorder');
        Route::post('how-contact', 'HowContactController@reorder');
        Route::post('type', 'ProductTypeController@reorder');
        Route::post('product', 'ProductController@reorder');
        Route::post('service', 'ServiceController@reorder');
        Route::post('slider', 'SliderController@reorder');
        Route::post('specialization', 'SpecializationController@reorder');
        Route::post('post', 'PostController@reorder');
    });


    Route::resource('brand', 'BrandController');
    Route::resource('category', 'CategoryController');
    Route::resource('certificate', 'CertificateController');
    Route::resource('corporation', 'CorporationController');
    Route::get('contact/download', 'ContactController@download')->name('contact.download');
    Route::resource('contact', 'ContactController');
    Route::resource('event', 'EventController');
    Route::resource('how-contact', 'HowContactController');
    Route::resource('product/detail', 'ProductDetailController');
    Route::resource('product/type', 'ProductTypeController');
    Route::resource('product', 'ProductController');
    Route::resource('profile', 'ProfileController');
    Route::resource('role', 'RoleController');
    Route::resource('service', 'ServiceController');
    Route::resource('second-slider', 'SecondSliderController');
    Route::post('slider/text-general', 'SliderController@textGeneralUpdate')->name('slider.text.general');
    Route::resource('slider', 'SliderController');
    Route::resource('specialization', 'SpecializationController');
    Route::resource('headquarter', 'HeadquarterController');
    Route::resource('user', 'UserController');
    Route::get('seo/section', 'SeoController@sectionData');
    Route::resource('seo', 'SeoController');
    Route::get('post/{id}/comments', 'PostController@comments')->name('post.comments');
    Route::resource('post', 'PostController');
    Route::get('comment/{id}', 'PostController@comment')->name('comment.show');
    Route::delete('comment/{id}', 'PostController@commentDelete')->name('comment.destroy');

    Route::get('newsletter/{id}/send', 'NewsletterController@send');
    Route::get('newsletter/{id}/preview', 'NewsletterController@preview');
    Route::resource('newsletter', 'NewsletterController');

    Route::group(['prefix' => 'us'], function () {
        Route::get('/enterprise', 'UsController@enterprise')->name('us.enterprise');
        Route::get('/mission', 'UsController@mission')->name('us.mission');
        Route::get('/vision', 'UsController@vision')->name('us.vision');
        Route::get('/holding', 'UsController@holding')->name('us.holding');
        Route::get('/policy', 'UsController@policy')->name('us.policy');
        Route::get('/history', 'UsController@history')->name('us.history');
        Route::post('/enterprise', 'UsController@storeEnterprise');
        Route::post('/holding', 'UsController@storeHolding');
        Route::post('/{type}', 'UsController@store')->name('us.store');
    });

    //Routes to Galleries
    Route::post('/gallery/{id}/update', 'GalleryController@update')->name('gallery.update');
    Route::post('/gallery/{type}', 'GalleryController@uploadImagesByType');
    Route::delete('/gallery/{type}/{id}', 'GalleryController@deleteByType');
    Route::post('/pdf/{type}', 'GalleryController@uploadPDFByType');

    //Routes to Images
    Route::post('images/uploader', 'ImageController@postUploader')->name('images.uploader');
});
