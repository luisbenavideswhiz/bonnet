<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('service_detail');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('service_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsignet();
            $table->string('title');
            $table->mediumText('description');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
