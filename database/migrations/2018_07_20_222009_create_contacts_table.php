<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration {

	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('last_name');
			$table->string('document');
			$table->string('business_document');
			$table->string('business_name');
			$table->integer('specialization_id');
			$table->string('email');
			$table->string('phone');
			$table->string('product_interest');
			$table->mediumText('message');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('contacts');
	}
}