<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubTablesTable extends Migration {

	public function up()
	{
		Schema::create('sub_tables', function(Blueprint $table) {
			$table->increments('id');
			$table->string('meta_key');
			$table->string('meta_value');
			$table->mediumText('meta_option')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('sub_tables');
	}
}