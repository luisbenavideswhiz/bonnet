<?php

use App\Models\Brand;
use App\Models\Category;
use App\Models\SubTable;
use Faker\Generator as Faker;

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    $brandIds = [];
    $brands = Brand::all();
    foreach ($brands as $brand) {
        array_push($brandIds, $brand->id);
    }

    $typeIds = [];
    $types = Subtable::getProductType();
    foreach ($types as $type) {
        array_push($typeIds, $type->id);
    }

    $categoryIds = [];
    $categories = Category::all();
    foreach ($categories as $category) {
        array_push($categoryIds, $category->id);
    }

    $name = $faker->word;
    return[
        'sku' => $faker->randomNumber(8),
        'name' => $name,
        'slug' => str_slug($name),
        'description' => $faker->paragraph,
        'tag' => str_replace(' ', ',', $name),
        'stock' => $faker->randomNumber(2),
        'status' => $faker->randomElement([0, 1, 2]),
        'type_id' => $faker->randomElement($typeIds),
        'brand_id' => $faker->randomElement($brandIds),
        'category_id' => $faker->randomElement($categoryIds),
        'gallery_id' => factory(\App\Models\Gallery::class)->states('product')->create()->id,
    ];

});