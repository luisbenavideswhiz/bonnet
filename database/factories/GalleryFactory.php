<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->state(App\Models\Gallery::class, 'event', function ($faker) {
    $image = $faker->image('public/storage/event', 548, 385);
    return [
        'type' => 'EVENT',
        'path' => str_replace('public/storage', 'storage', $image),
    ];
});

$factory->state(App\Models\Gallery::class, 'event-gallery', function ($faker) {
    $path = $faker->image('public/storage/gallery/event/' . factory(\App\Models\Event::class)->make()->id, 548, 385);
    return [
        'type' => 'EVENT',
        'path' => $path,
    ];
});

$factory->define(App\Models\Gallery::class, function (Faker $faker) {
    return [
        'name' => $faker->name . '.jpg',
        'description' => $faker->text(40),
        'mime' => 'image/jpeg'
    ];
});
