<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Headquarter::class, function (Faker $faker) {
    return [
        'name' => $faker->lastName,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'longitude' => $faker->longitude($min = -79, $max = -70),
        'latitude' => $faker->latitude($min = -13, $max = -10),
    ];
});
