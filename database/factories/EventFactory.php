<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Event::class, function (Faker $faker) {
    $title = $faker->text($maxNbChars = 20);
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'text' => '<p>' . $faker->text($maxNbChars = 350) . '</p>',
        'description' => '<p>' . $faker->text($maxNbChars = 600) . '</p>',
        'gallery_id' => factory(\App\Models\Gallery::class)->states('event')->create()->id,
    ];
});