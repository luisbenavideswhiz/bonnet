<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\ServiceDetail::class, function (Faker $faker) {
    return [
        'service_id' => factory(\App\Models\Service::class)->make()->id,
        'title' => $faker->text($maxNbChars = 20),
        'description' => '<p>' . $faker->text($maxNbChars = 600) . '</p>',
    ];
});
