<?php

use App\Models\SubTable;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SubTableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();

        // //Social Networks
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'facebook', 'meta_option' => '<span class="fa fa-facebook icon-facebook"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'twitter', 'meta_option' => '<span class="fa fa-twitter icon-twitter"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'tumblr', 'meta_option' => '<span class="fa fa-tumblr icon-tumblr"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'gplus', 'meta_option' => '<span class="fa fa-google-plus icon-google-plus"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'instagram', 'meta_option' => '<span class="fa fa-instagram icon-instagram"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'pinterest', 'meta_option' => '<span class="fa fa-pinterest-p icon-pinterest-p"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'snapchat', 'meta_option' => '<span class="fa fa-snapchat-ghost icon-snapchat-ghost"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'reddit', 'meta_option' => '<span class="fa fa-reddit-alien icon-reddit-alien"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'vine', 'meta_option' => '<span class="fa fa-vine icon-vine"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'telegram', 'meta_option' => '<span class="fa fa-telegram icon-telegram"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'youtube', 'meta_option' => '<span class="fa fa-youtube-play icon-youtube-play"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'twitch', 'meta_option' => '<span class="fa fa-twitch icon-twitch"></span>']
        // );
        // Subtable::create(
        //     ['meta_key' => 'social_network', 'meta_value' => 'linkedin', 'meta_option' => '<span class="fa fa-linkedin icon-linkedin"></span>']
        // );

        // //Specializations
        // Subtable::create(['meta_key' => 'specialization', 'meta_value' => 'Técnico']);
        // Subtable::create(['meta_key' => 'specialization', 'meta_value' => 'Universitario']);

        // //Product Type
        // for ($i = 0; $i < 5; $i++) {
        //     $name = $faker->firstNameFemale;
        //     Subtable::insert([
        //         'meta_key' => 'product_type',
        //         'meta_value' => $name,
        //         'meta_option' => str_slug($name),
        //         'created_at' => \Carbon\Carbon::now(),
        //         'updated_at' => \Carbon\Carbon::now()
        //     ]);
        // }

        // //Service Banner
        // Subtable::create(['meta_key' => 'event_banner', 'meta_value' => '', 'meta_option' => '<p>' . $faker->text($maxNbChars = 500) . '</p>']);

        // //Event Banner
        // Subtable::create(['meta_key' => 'service_banner', 'meta_value' => '', 'meta_option' => '<p>' . $faker->text($maxNbChars = 500) . '</p>']);

        // //Footer
        // Subtable::create(['meta_key' => 'footer_email', 'meta_value' => 'web@grupobonnett.com']);
        // Subtable::create(['meta_key' => 'footer_phone', 'meta_value' => '719-2121 / 446-1587 /446-0349']);
        // Subtable::create(['meta_key' => 'footer_address', 'meta_value' => 'Jr. Domingo Martinez Luján 1155 Surquillo, Lima, Peru']);
        // Subtable::create(['meta_key' => 'footer_description', 'meta_value' => '', 'meta_option' => '<p>Grupo Bonnett S.A naci&oacute; el 24 de setiembre de 1977, en la ciudad de la Merced, provincia de Chanchamayo, departamento de Jun&iacute;n - Per&uacute;.</p>']);
        // Subtable::create(['meta_key' => 'descriptive', 'meta_value' => '', 'meta_option' => '']);
        // Subtable::create(['meta_key' => 'header_banner_top_blog', 'meta_value' => 'Blog', 'meta_option' => '']);
    }
}
