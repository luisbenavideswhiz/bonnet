<?php

use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\Models\Event::class, 1)->create()->each(function ($u){
//            $u->gallery()->saveMany(factory(\App\Models\Gallery::class, 5)->states('event-gallery')->create());
//        });
        factory(App\Models\Event::class, 5)->create();
    }
}
