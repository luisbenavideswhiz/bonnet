<?php

use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade as Bouncer;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::allow('superadmin')->everything();

        Bouncer::allow('admin')->to('manage_general');
        Bouncer::allow('admin')->to('manage_us');
        Bouncer::allow('admin')->to('manage_category');
        Bouncer::allow('admin')->to('manage_product');
        Bouncer::allow('admin')->to('manage_service');
        Bouncer::allow('admin')->to('manage_event');
        Bouncer::allow('admin')->to('manage_specialization');
        Bouncer::allow('admin')->to('manage_how_contact');
        Bouncer::allow('admin')->to('manage_message');


        $user = \App\User::find(1);
        Bouncer::assign('superadmin')->to($user);

    }
}
