<?php

use App\Models\SubTable;
use Illuminate\Database\Seeder;

class SecondBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subtable::create(
            ['meta_key' => 'second_slider_center', 'meta_value' => 'Ideas para hacer del Perú un país sostenible']
        );
        Subtable::create(
            ['meta_key' => 'second_slider_right', 'meta_value' => 'Soporte técnico ', 'meta_option' => 'http://bonnett.fake/servicios']
        );
        Subtable::create(
            ['meta_key' => 'second_slider_left', 'meta_value' => 'Desarrollo de proyectos ', 'meta_option' => 'http://bonnett.fake/servicios']
        );
    }
}
