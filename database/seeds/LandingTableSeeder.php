<?php

use App\Models\Landing;
use Illuminate\Database\Seeder;

class LandingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Landing::create(['name' => 'Web', 'slug' => 'contacto']);
        Landing::create(['name' => 'Bombas Contraincendios', 'slug' => 'bombas-contraincendios']);
    }
}