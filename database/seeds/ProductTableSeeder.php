<?php

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubTable;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $brandIds = [];
        $brands = Brand::all();
        foreach ($brands as $brand) {
            array_push($brandIds, $brand->id);
        }

        $typeIds = [];
        $types = Subtable::getProductType();
        foreach ($types as $type) {
            array_push($typeIds, $type->id);
        }

        $categoryIds = [];
        $categories = Category::all();
        foreach ($categories as $category) {
            array_push($categoryIds, $category->id);
        }

        for ($i = 0; $i < 50; $i++) {
            $name = $faker->word;
            Product::insert([
                'sku' => $faker->randomNumber(8),
                'name' => $name,
                'slug' => str_slug($name),
                'description' => $faker->paragraph,
                'tag' => str_replace(' ', ',', $name),
                'stock' => $faker->randomNumber(2),
                'status' => $faker->randomElement([0, 1, 2]),
                'type_id' => $faker->randomElement($typeIds),
                'brand_id' => $faker->randomElement($brandIds),
                'category_id' => $faker->randomElement($categoryIds),
                'gallery_id' => null,
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ]);
        }
    }
}
