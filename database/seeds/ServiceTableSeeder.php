<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Service::class, 10);
//            ->create()->each(function ($u) {
//            $u->details()->saveMany(factory(App\Models\ServiceDetail::class, 5)->make());
//        });
    }
}
