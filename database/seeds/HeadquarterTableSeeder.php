<?php

use Illuminate\Database\Seeder;

class HeadquarterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Headquarter::class, 8)->create();
    }
}
