<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'last_name' => '',
            'email' => 'support@bonnett.pe',
            'password' => bcrypt('admin'),
        ]);
    }
}
