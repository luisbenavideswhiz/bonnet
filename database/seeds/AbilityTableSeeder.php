<?php

use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade as Bouncer;

class AbilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::ability()->create([
            'name' => 'manage_general',
            'title' => 'Gestionar Generales',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_us',
            'title' => 'Gestionar Nosotros',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_category',
            'title' => 'Gestionar Categorías',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_product',
            'title' => 'Gestionar Productos',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_service',
            'title' => 'Gestionar Servicios',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_event',
            'title' => 'Gestionar Eventos',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_specialization',
            'title' => 'Gestionar Niveles de Usuarios',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_how_contact',
            'title' => 'Gestionar Medios de Contacto',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_message',
            'title' => 'Gestionar Mensajes',
        ]);

        Bouncer::ability()->create([
            'name' => 'manage_user',
            'title' => 'Gestionar Usuarios',
        ]);
    }
}
