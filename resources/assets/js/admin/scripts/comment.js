$(function () {
    Form.form('form');
    $('table')
        .DataTable({'language': {'url': '/js/jquery.dataTables.spanish.json'}})
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/comment/' + response.data.id);
                $('#message').html(response.data.name)
            }).done(function () {
                $('#delete').modal('show');
            });
        })
        .on('click', 'a[href="#comments"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#_message').html(response.data.comment);
                }).done(function () {
                    $('#comments').modal('show');
            });
        });
});