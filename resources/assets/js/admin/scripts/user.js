$(function () {
    $('table')
        .DataTable({'language': {'url': '/js/jquery.dataTables.spanish.json'}})
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/user/' + response.data.id);
                $('#message').html(response.data.name)
            }).done(function () {
                $('#delete').modal('show');
            });
        })
});