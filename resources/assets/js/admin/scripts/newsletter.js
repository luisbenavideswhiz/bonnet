require('select2/dist/js/select2.full.min');
$(function () {
  $(document).on('click', '#send', function () {
    let _this = $(this);
    _this.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $.get(_this.data('url'), function (response) {
      if (response.status) {
        _this.html('<i class="fa fa-envelope"></i>');
        toastr.success(response.message, 'Correcto');
      }
    })
  });

  $('.select2').select2({
    placeholder: 'Seleccione blog',
    maximumSelectionLength: 3
  });

  window.table = $('#newsletters')
    .DataTable({
      'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
      'language': {'url': '/js/jquery.dataTables.spanish.json'},
      'processing': true,
      'serverSide': true,
      'ajax': {
        'url': '/admin/table/newsletter',
        'dataSrc': 'data',
        'dataFilter': function (data) {
          let json = $.parseJSON(data);
          json.recordsTotal = json.meta.total;
          json.recordsFiltered = json.meta.total;
          return JSON.stringify(json);
        }
      },
      'columns': [
        {'data': 'title'},
        {'data': 'id', className: 'acciones text-center'},
      ],
      'columnDefs': [
        {
          'targets': -1,
          'render': function (data, type, row, meta) {
            return '<a href="/admin/newsletter/' + row.id + '/preview" class="btn btn-default">' +
              '<i class="fa fa-eye"></i></a>\n' +
              '<button id="send" data-url="/admin/newsletter/' + row.id + '/send" class="btn btn-info">' +
              '<i class="fa fa-envelope"></i></button>\n' +
              '<a href="/admin/newsletter/' + row.id + '/edit" class="btn btn-warning">' +
              '<i class="fa fa-pencil-square-o"></i></a>\n' +
              '<a href="#delete" data-toggle="modal" class="btn btn-danger"\n' +
              'data-url="/admin/newsletter/' + row.id + '"><i class="fa fa-trash-o"></i></a>';
          }
        }
      ],
    })
    .on('click', 'a[href="#delete"]', function (e) {
      e.preventDefault();
      $.get($(this).data('url'), function (response) {
        $('#delete form').attr('action', '/admin/newsletter/' + response.data.id);
        $('#message').html(response.data.title)
      }).done(function () {
        $('#delete').modal('show');
      });
    })
});