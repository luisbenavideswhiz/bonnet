$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').prop('content')
        }
    });

    $('#fileupload')
        .bind('fileuploadsubmit', function (e, data) {
            var inputs = data.context.find(':input');
            if (inputs.filter(function () {
                return !this.value && $(this).prop('required');
            }).first().focus().length) {
                data.context.find('button').prop('disabled', false);
                return false;
            }
            data.formData = inputs.serializeArray();
        })
        .fileupload({
            url: '/admin/gallery/alliance',
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });

    $(document).on('click', '.edit', function (e) {
        e.preventDefault();
        var description = $(this).parent().parent().find('input[name="description"]').val();
        var token = $('meta[name="csrf-token"]').prop('content');
        $.post($(this).data('url'), {_token: token, description: description}, function (response) {
            toastr.success(response.message, 'Correcto');
        })
    });
});