$(function () {
    $('.tagsinput').tagsInput();
    $('#add-url').on('click', function (e) {
        e.preventDefault();
        if ($(document).find('input[name="url-link[]"]').length < 5) {
            $('.insert').append(
                '<div class="col-xs-12 display-flex justify-content-space-between m-bot15">\n' +
                '<input type="text" class="form-control w30" name="url_text[]" placeholder="Títlulo">\n' +
                '<input type="text" class="form-control w60" name="url_link[]" placeholder="Url">\n' +
                '<a href="#" class="btn btn-danger remove"><i class="fa fa-times"></i></a></div>'
            )
        }
    });
    $(document).on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent().remove();
    })
});