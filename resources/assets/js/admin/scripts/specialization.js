$(function () {
    var table = $('table')
        .DataTable({
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
            'language': {'url': '/js/jquery.dataTables.spanish.json'},
            'processing': true,
            'serverSide': true,
            'rowReorder': {dataSrc: 'reading_order'},
            'ajax': {'url': '/admin/table/specialization', 'dataSrc': 'specializations'},
            'columns': [
                {'data': 'reading_order', className: 'reorder text-center'},
                {'data': 'meta_value'},
                {'data': 'id', className: 'acciones text-center'},
            ],
            'columnDefs': [
                {orderable: false, targets: [0, 1, 2]},
                {
                    'targets': -1,
                    'render': function (data, type, row, meta) {
                        var html = '';
                        html =
                            '<a href="#update" data-toggle="modal" class="btn btn-warning"' +
                            'data-url="/admin/specialization/' + row.id + '/edit">' +
                            '<i class="fa fa-pencil-square-o"></i></a>\n' +
                            '<a href="#delete" data-toggle="modal" class="btn btn-danger"\n' +
                            'data-url="/admin/specialization/' + row.id + '"><i class="fa fa-trash-o"></i></a>';
                        return html;
                    }
                }
            ],
        })
        .on('click', 'a[href="#update"]', function () {
            $.get($(this).data('url'), function (response) {
                $('#update form').attr('action', '/admin/specialization/' + response.data.id);
                $('#name_u').val(response.data.meta_value);
            })
        })
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/specialization/' + response.data.id);
                $('#message').html(response.data.meta_value)
            }).done(function () {
                $('#delete').modal('show');
            });
        })
        .on('row-reorder', function (e, diff, edit) {
            var data;
            for (var i = 0, ien = diff.length; i < ien; i++) {
                var rowData = table.row(diff[i].node).data();
                if (edit.triggerRow.data().id === rowData.id) {
                    data = {id: rowData.id, order_new: diff[i].newData, order_old: diff[i].oldData};
                }
            }
            $.post('/admin/reorder/specialization', {data: data, _token: $('meta[name="csrf-token"]').attr('content')});
        });
});