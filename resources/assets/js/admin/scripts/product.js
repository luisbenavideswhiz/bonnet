$(function () {
    $('.tagsinput').tagsInput();

    var tableProduct = $('#product-table')
        .DataTable({
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
            'language': {'url': '/js/jquery.dataTables.spanish.json'},
            'processing': true,
            'serverSide': true,
            'rowReorder': {dataSrc: 'reading_order'},
            'ajax': {'url': '/admin/table/product', 'dataSrc': 'products'},
            'columns': [
                {'data': 'reading_order', className: 'reorder text-center'},
                {'data': 'name'},
                {'data': 'category.name'},
                {'data': 'brand.name'},
                {'data': 'status_name'},
                {'data': 'stock'},
                {'data': 'id', className: 'acciones text-center'},
            ],
            'columnDefs': [
                {orderable: false, targets: [0, 1, 2]},
                {
                    'targets': -1,
                    'render': function (data, type, row, meta) {
                        var html = '';
                        html =
                            '<a href="/admin/product/' + row.id + '/edit" class="btn btn-warning">' +
                            '<i class="fa fa-pencil-square-o"></i></a>\n' +
                            '<a href="/admin/product/detail/' + row.id + '/edit" class="btn btn-info">' +
                            '<i class="fa fa-cog"></i></a>\n' +
                            '<a href="#delete" data-toggle="modal" class="btn btn-danger"\n' +
                            'data-url="/admin/product/' + row.id + '"><i class="fa fa-trash-o"></i></a>';
                        return html;
                    }
                }
            ],
        })
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/product/' + response.data.id);
                $('#message').html(response.data.name)
            }).done(function () {
                $('#delete').modal('show');
            });
        })
        .on('row-reorder', function (e, diff, edit) {
            var data;
            for (var i = 0, ien = diff.length; i < ien; i++) {
                var rowData = tableProduct.row(diff[i].node).data();
                if (edit.triggerRow.data().id === rowData.id) {
                    data = {id: rowData.id, order_new: diff[i].newData, order_old: diff[i].oldData};
                }
            }
            $.post('/admin/reorder/product', {data: data, _token: $('meta[name="csrf-token"]').attr('content')});
        });

    // Detail
    var table = $('table');
    table.on('click', 'a[href="#delete-feature"]', function (e) {
        e.preventDefault();
        $.get($(this).data('url'), function (response) {
            $('#delete form').attr('action', '/admin/product/detail/' + response.data.id);
            $('#message').html(response.data.title)
        }).done(function () {
            $('#delete').modal('show');
        });
    });

    table.on('click', '.edit-detail', function (e) {
        e.preventDefault();
        $.get($(this).data('url'), function (response) {
            $('#product_detail_id').val(response.data.id);
            $('#title').val(response.data.title);
            CKEDITOR.instances.description.setData(response.data.description);
            if (response.data.image !== null) {
                $('#image').next().find('.fileupload-new.thumbnail').html('<img src="' + window.location.origin + response.data.image.path + '"/>')
            } else {
                $('#image').next().find('.fileupload-new.thumbnail').html('');
            }
            if (response.data.pdf !== null) {
                $('#pdf').find('.btn-white.btn-file').next().html('<div><img src="/img/admin/pdf.png" alt="" width="100px"><a href="#" data-url="/admin/gallery/' + response.data.pdf.type + '/' + response.data.pdf.id + '" class="btn btn-danger delete-pdf"><i class="fa fa-times"></i></a><br><span>' + response.data.pdf.name + '</span></div> ')
            } else {
                $('#pdf').find('.btn-white.btn-file').next().html('');
            }

        });
    });

    $(document).on('click', '.delete-pdf', function (e) {
        e.preventDefault();
        var element = $(this);
        $.post(element.data('url'), {
            _method: 'delete',
            _token: $('meta[name="csrf-token"]').attr('content')
        }).done(function () {
            element.parent().remove();
        });
    })
});