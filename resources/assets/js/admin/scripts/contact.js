require('bootstrap-datepicker');
require('bootstrap-datepicker/js/locales/bootstrap-datepicker.es');
import moment from 'moment';

$(function () {
  let oTable = $('table')
    .on('click', 'a[href="#message"]', function (e) {
      e.preventDefault();
      $.get($(this).data('url'), function (response) {
        $.each(response.data, function (i, v) {
          if (i !== 'message') {
            $('#_' + i).val(v);
          } else {
            $('#_message').html(v);
          }
        })
      });
    })
    .DataTable({'language': {'url': '/js/jquery.dataTables.spanish.json'}});


  $('#datepicker_from').datepicker({
    showOn: 'button',
    format: 'dd/mm/yyyy',
    language: 'es',
    autoclose: true,
  });

  $("#datepicker_to").datepicker({
    showOn: 'button',
    autoclose: true,
    format: 'dd/mm/yyyy',
    language: 'es',
  });

  $('.btn-filter-dates').click(function () {
    minDateFilter = new Date(moment($("#datepicker_from").val(), 'DD/MM/YYYY')).getTime();
    maxDateFilter = new Date(moment($("#datepicker_to").val(), 'DD/MM/YYYY')).getTime();
    oTable.draw();
  });
});

let minDateFilter = '';
let maxDateFilter = '';

$.fn.dataTableExt.afnFiltering.push(
  function (oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(moment(aData[5], 'DD/MM/YYYY')).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }
    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }
    return true;
  }
);