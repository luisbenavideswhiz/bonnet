$(function () {
    $('table')
        .DataTable({'language': {'url': '/js/jquery.dataTables.spanish.json'}})
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/role/' + response.data.id);
                $('#message').html(response.data.title)
            }).done(function () {
                $('#delete').modal('show');
            });
        });

    $('#all').click(function (e) {
        if (this.checked) {
            $(':checkbox').each(function () {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });

    $('input[name="ability[]"]').on('click', function (e) {
        if ($(this).is('checked') === false) {
            $('#all').prop('checked', false)
        }
    });
});