require('select2/dist/js/select2.full.min');

$(function () {
    $('.tagsinput').tagsInput();

    $('table')
        .DataTable({
            'language': {'url': '/js/jquery.dataTables.spanish.json'},
            'processing': true,
            'serverSide': true,
            'ajax': {
                'url': '/admin/table/seo',
                'dataSrc': 'data',
                'dataFilter': function (data) {
                    let json = $.parseJSON(data);
                    json.recordsTotal = json.meta.total;
                    json.recordsFiltered = json.meta.total;
                    return JSON.stringify(json);
                }
            },
            'columns': [
                {'data': 'category_name'},
                {'data': 'url'},
                {'data': 'title'},
                {'data': 'id', className: 'acciones text-center'},
            ],
            'columnDefs': [
                {
                    'targets': -1,
                    'render': function (data, type, row, meta) {
                        let html = '';
                        html =
                            '<a href="/admin/seo/' + row.id + '/edit" class="btn btn-warning">' +
                            '<i class="fa fa-pencil-square-o"></i></a>\n' +
                            '<a href="#delete" data-toggle="modal" class="btn btn-danger"\n' +
                            'data-url="/admin/seo/' + row.id + '"><i class="fa fa-trash-o"></i></a>';
                        return html;
                    }
                }
            ],
        })
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/seo/' + response.data.id);
                $('#message').html(response.data.title)
            }).done(function () {
                $('#delete').modal('show');
            });
        });

    $('#category').on('change', function (e) {
        $('#name').val(null).trigger('change');
    });

    $('#name').select2({
        ajax: {
            url: '/admin/seo/section',
            data: function (params) {
                return {
                    search: params.term,
                    filter: $('#category').val(),
                };
            },
            processResults: function (data) {
                return {
                    results: data.data
                };
            }
        }
    });
});