$(function () {
    var table = $('table')
        .DataTable({
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
            'language': {'url': '/js/jquery.dataTables.spanish.json'},
            'processing': true,
            'serverSide': true,
            'rowReorder': {dataSrc: 'reading_order'},
            'ajax': {'url': '/admin/table/service', 'dataSrc': 'services'},
            'columns': [
                {'data': 'reading_order', className: 'reorder text-center'},
                {'data': 'title'},
                {'data': 'id', className: 'acciones text-center'},
            ],
            'columnDefs': [
                {orderable: false, targets: [0, 1, 2]},
                {
                    'targets': -1,
                    'render': function (data, type, row, meta) {
                        var html = '';
                        html =
                            '<a href="/admin/service/' + row.id + '/edit" class="btn btn-warning">' +
                            '<i class="fa fa-pencil-square-o"></i></a>\n' +
                            '<a href="#delete" data-toggle="modal" class="btn btn-danger"\n' +
                            'data-url="/admin/service/' + row.id + '"><i class="fa fa-trash-o"></i></a>';
                        return html;
                    }
                }
            ],
        })
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/service/' + response.data.id);
                $('#message').html(response.data.title)
            }).done(function () {
                $('#delete').modal('show');
            });
        })
        .on('row-reorder', function (e, diff, edit) {
            var data;
            for (var i = 0, ien = diff.length; i < ien; i++) {
                var rowData = table.row(diff[i].node).data();
                if (edit.triggerRow.data().id === rowData.id) {
                    data = {id: rowData.id, order_new: diff[i].newData, order_old: diff[i].oldData};
                }
            }
            $.post('/admin/reorder/service', {data: data, _token: $('meta[name="csrf-token"]').attr('content')});
        });

    $(document).on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent().remove();
    })
});