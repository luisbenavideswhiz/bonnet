$(function () {
    Form.form('form', true);
    Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper', '.modal-btn-confirm', '#input_image', 1080, 780);

    let table = $('#post-table')
        .DataTable({
            'lengthMenu': [[10, 25, 50], [10, 25, 50]],
            'language': {'url': '/js/jquery.dataTables.spanish.json'},
            'processing': true,
            'serverSide': true,
            'rowReorder': {dataSrc: 'reading_order'},
            'ajax': {
                'url': '/admin/table/post',
                'dataSrc': 'data',
                'dataFilter': function (data) {
                    let json = $.parseJSON(data);
                    json.recordsTotal = json.meta.total;
                    json.recordsFiltered = json.meta.total;
                    return JSON.stringify(json);
                }
            },
            'columns': [
                {'data': 'reading_order', className: 'reorder text-center'},
                {'data': 'title'},
                {'data': 'id', className: 'acciones text-center'},
            ],
            'columnDefs': [
                {orderable: false, targets: [0, 1, 2]},
                {
                    'targets': -1,
                    'render': function (data, type, row, meta) {
                        return '<a href="/admin/post/' + row.id + '/comments" class="btn btn-info">' +
                            '<i class="fa fa-comments-o"></i></a>\n' +
                            '<a href="/admin/post/' + row.id + '/edit" class="btn btn-warning">' +
                            '<i class="fa fa-pencil-square-o"></i></a>\n' +
                            '<a href="#delete" data-toggle="modal" class="btn btn-danger"\n' +
                            'data-url="/admin/post/' + row.id + '"><i class="fa fa-trash-o"></i></a>';
                    }
                }
            ],
        })
        .on('click', 'a[href="#delete"]', function (e) {
            e.preventDefault();
            $.get($(this).data('url'), function (response) {
                $('#delete form').attr('action', '/admin/post/' + response.data.id);
                $('#message').html(response.data.title)
            }).done(function () {
                $('#delete').modal('show');
            });
        })
        .on('row-reorder', function (e, diff, edit) {
            let data;
            for (let i = 0, ien = diff.length; i < ien; i++) {
                let rowData = table.row(diff[i].node).data();
                if (edit.triggerRow.data().id === rowData.id) {
                    data = {id: rowData.id, order_new: diff[i].newData, order_old: diff[i].oldData};
                }
            }
            $.post('/admin/reorder/post', {data: data, _token: $('meta[name="csrf-token"]').attr('content')});
        });

    $(document).on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    $('.tagsinput').tagsInput();
});