try {
    window.$ = window.jQuery = require('jquery');
    require('datatables.net-rowreorder')(window, $);
    //require('bootstrap-sass');
} catch (e) {

}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./vendor/bootstrap.min');
require('./vendor/jquery.dcjqaccordion.2.7');
require('./vendor/jquery.scrollTo.min');
require('./vendor/jquery.nicescroll');
require('./vendor/jquery.sparkline');
require('./vendor/bootstrap-fileupload');
require('./vendor/jquery.tagsinput');

// right slidebars
require('./vendor/slidebars.min');

// common scripts for all pages
require('./vendor/common-scripts');

require('./vendor/sparkline-chart');
require('./vendor/count');

var bodyId = $('body').prop('id');
require('./scripts/' + bodyId);