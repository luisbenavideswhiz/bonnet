require('./plugins');

$('.form-input input , .form-select select, .form-textarea textarea')
    .bind("input load change paste keyup", function () {
        if ($(this).val()) {
            $(this).closest('.form-control').addClass('active');

        } else {
            $(this).closest('.form-control').removeClass('active');
        }
    })
    .on('focus', function (e) {
        $(this).closest('.form-control').addClass('focus');
    })
    .on('blur', function (e) {
        $(this).closest('.form-control').removeClass('focus');
    });

$('.form-control').each(function () {
    if ($(this).find('input').val()) {
        $(this).find('input').addClass('active');
    }
    if ($(this).find('select').val()) {
        $(this).find('select').addClass('active');
    }
    if ($(this).find('textarea').val()) {
        $(this).find('textarea').addClass('active');
    }
});

$('form').on('submit', function (e) {
    e.preventDefault();
    $('.form-control').removeClass('error');
    $('.error-label').remove();
    $.ajax({
        url: $(this).prop('action'),
        type: $(this).prop('method'),
        data: $(this).serialize(),
        success: function (response) {
            if (!response.status) {
                $.each(response.data, function (i, v) {
                    $('#' + i).parent().parent().addClass('error');
                    $('#' + i).parent().append('<span class="error-label">' + v + '</span>');
                })
            } else {
                $('.form').addClass('d-none');
                $('.message').removeClass('d-none');
            }
        }
    });
});
