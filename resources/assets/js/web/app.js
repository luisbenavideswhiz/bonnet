/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./plugins');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import axios from 'axios';
import mapComponent from './components/MapComponent.vue';
import homeProductComponent from './components/HomeProductComponent.vue';
import homeFilterComponent from './components/HomeFilterComponent.vue';
import homeSliderComponent from './components/HomeSliderComponent.vue';
import productFilterComponent from './components/ProductFilterComponent.vue';
import productSearchComponent from './components/ProductSearchComponent.vue';
import productRelatedComponent from './components/ProductRelatedComponent.vue';
import 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'webui-popover/dist/jquery.webui-popover.js';


const find_map = new Vue({
    el: '#find_map',
    components: {
        'google-map': mapComponent
    },

});

const home = new Vue({
    el: '#home',
    components: {
        'home-products': homeProductComponent,
        'home-filters': homeFilterComponent,
        'home-slider': homeSliderComponent,
    }
});

const product_search = new Vue({
    el: '#product_search',
    components: {
        'product-filter': productFilterComponent,
        'home-products': homeProductComponent,
    }
});

const product_detail = new Vue({
    el: '#product_detail',
    components: {
        'product-related': productRelatedComponent,
    }
});

const contact = new Vue({
    el: '#contact',
    data() {
        return {
            name: '',
            last_name: '',
            document: '',
            business_document: '',
            business_name: '',
            specialization_id: '',
            other: '',
            email: '',
            phone: '',
            product_interest: '',
            how_contact: '',
            message: '',
            errors: [],
        }
    },
    watch: {
        specialization_id: function (val) {
            if (this.specialization_id == 0 && this.specialization_id != '') {
                document.querySelector('.other-group').style.display = 'block';
            } else {
                document.querySelector('.other-group').style.display = 'none';
            }
        }

    },
    methods: {
        onSubmit() {
            let contact_data = new FormData();
            contact_data.append('name', this.name);
            contact_data.append('last_name', this.last_name);
            contact_data.append('document', this.document);
            contact_data.append('specialization_id', this.specialization_id);
            contact_data.append('email', this.email);
            contact_data.append('phone', this.phone);
            contact_data.append('product_interest', this.product_interest);
            contact_data.append('how_contact', this.how_contact);
            contact_data.append('message', this.message);
            if (this.business_document.length) {
                contact_data.append('business_document', this.business_document);
            }
            if (this.business_name.length) {
                contact_data.append('business_name', this.business_name);
            }
            if (this.specialization_id == 0) {
                contact_data.append('other', this.other);
            }
            axios.post('/api/contact', contact_data).then(response => {
                if (response.data.status == false) {
                    this.errors = response.data.data;
                } else {
                    window.location.href = '/contacto/gracias'
                }
            })
        }
    }
});

function bannerFull() {
    let ww = $(window).width();
    let bh = $(window).height() - $('.header').height();
    if (ww > 992) {
        $('.banner-cnt').css('height', bh);
        $('.banner-item').css('height', bh);
    } else {
        $('.banner-item').css('height', 'auto');
    }
}

function backToTop() {
    let scrollTop = $(window).scrollTop();
    if (scrollTop > 400) {
        $('.back-top').addClass('show');
    } else {
        $('.back-top').removeClass('show');
    }
};

var scrollPos = 0;

function menuScroll() {
    let curScrollPos = $(window).scrollTop();
    if (curScrollPos > 500) {
        $('.header-scroll').addClass('open');
    } else {
        $('.header-scroll').removeClass('open');
    }
    scrollPos = curScrollPos;
};

/*function loading() {
    $('.loader').fadeOut();
    $('body').removeClass('loading');
}*/

bannerFull();
$(window).on('load', function () {
    bannerFull();
    //loading();
});

$(window).on('resize', function () {
    bannerFull();
});

$(window).on('scroll', function () {
    backToTop();
    menuScroll();
});

$('.product-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    centerMode: true,
    infinite: true
});

$('.alliance-carousel').slick({
    infinite: true,
    variableWidth: true,
    autoplay: true,
    autoplaySpeed: 0,
    speed: 2000,
    cssEase: 'linear',
    responsive: [
        {
            breakpoint: 768,
            centerMode: true,
        }
    ]
});

$('.category-carousel').slick({
    infinite: true,
    autoplay: true,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 768,
            centerMode: true,
        }
    ]
});

$('.product-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    dots: true
});

$('.gallery-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    mobileFirst: true,
    responsive: [
        {
            breakpoint: 768,
            settings: 'unslick'
        }
    ]
});

$('.event-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    mobileFirst: true,
    responsive: [
        {
            breakpoint: 768,
            settings: 'unslick'
        }
    ]
});

$('.service-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    mobileFirst: true,
    responsive: [
        {
            breakpoint: 768,
            settings: 'unslick'
        }
    ]
});

$('.banner-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    autoplay: true,
});

$('.image').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false
});

$('.header').append('<div class="header-scroll"></div>');
$('.header-scroll').append($('.header-primary').html());

$('.header-mobile').append('<div class="header-sidebar"></div>');
$('.header-sidebar').append($('.header-primary .logo').clone());
$('.header-sidebar').append('<a href="javascript:void(0)" class="menu-close"><span class="icon-close"></span></a>');
$('.header-sidebar').append($('.header-primary .menu-primary').html());

$('.header-filter .dropdown-button').on('click', function (e) {
    $(this).closest('.header-filter').addClass('active');
    $(this).parent().find('.dropdown-list').toggle();
});

$('.header-filter .dropdown-item a').on('click', function (e) {
    var drot = $(this).text();
    var dval = $(this).data('value');
    $('.header-filter .form-select select').removeAttr('disabled')
    $('.header-filter .form-select select').val(dval);
    $('.header-filter .dropdown-list').hide();
    $(this).closest('.header-filter').find('.form-dropdown').addClass('active');
    if (drot.length < 10) {
        $(this).closest('.header-filter').find('.dropdown-button .label-search').text(drot);
    } else {
        $(this).closest('.header-filter').find('.dropdown-button .label-search').text(drot.substring(0, 10) + '...');
    }
});

$(document).on('click', '.product-filter-sidebar .filter-title', function (e) {
    $('.product-filter-sidebar').toggleClass('open');
    $('.product-filter-collapse').slideToggle();
});
$(document).on('click', '.filter-widget .filter-widget-title', function (e) {
    $(this).closest('.filter-widget').toggleClass('open');
    $(this).closest('.filter-widget').find('.filter-widget-collapse').slideToggle();
});
$('.footer-item .footer-item-title').on('click', function (e) {
    $(this).closest('.footer-item').toggleClass('open');
    $(this).closest('.footer-item').find('.footer-item-collapse').slideToggle();
});

$('.form-input input , .form-select select, .form-textarea textarea').bind("input load change paste keyup", function () {
    var value = $(this).val();
    if ($(this).val()) {
        $(this).closest('.form-control').addClass('active');

    } else {
        $(this).closest('.form-control').removeClass('active');
    }
});

$('.form-control').each(function (index) {
    if ($(this).find('input').val()) {
        $(this).find('input').addClass('active');
    }
    if ($(this).find('select').val()) {
        $(this).find('select').addClass('active');
    }
    if ($(this).find('textarea').val()) {
        $(this).find('textarea').addClass('active');
    }
});


$('.form-input input, .form-select select, .form-textarea textarea').on('focus', function (e) {
    $(this).closest('.form-control').addClass('focus');
});

$('.form-input input, .form-select select, .form-textarea textarea').on('blur', function (e) {
    $(this).closest('.form-control').removeClass('focus');
});

$('.form-checkbox a').on('click', function (e) {
    $(this).addClass('active');
});

$('.faq-link').on('click', function (e) {
    $('.faq-content').stop(0).slideUp();
    $('.faq-item').removeClass('active');
    $(this).closest('.faq-item').find('.faq-content').stop(0).slideToggle();
    $(this).closest('.faq-item').toggleClass('active');
});

$('.tab .tab-nav a').on('click', function (e) {
    var itab = $(this).parent().index();
    $('.tab-nav li').removeClass('active');
    $('.tab-pane').removeClass('active');
    $('.tab .tab-pane').eq(itab).addClass('active');
    $(this).parent().addClass('active');
});

$('.back-top').on('click', function (e) {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
});

$('.location-item a').on('click', function (e) {
    console.log('active');
    $('.location-item').removeClass('active');
    $(this).parent().addClass('active');
});

$('.btn-mission').on('click', function (e) {
    $('.about-vision').hide();
    $('.about-mission').show();
});

$('.btn-vision').on('click', function (e) {
    $('.about-vision').show();
    $('.about-mission').hide();
});

$('.header-mobile .button-search').on('click', function (e) {
    $('.header-mobile-search').slideToggle();
});

$('.header-mobile .button-menu').on('click', function (e) {
    $('.header-sidebar').addClass('open');
});

$('.header-sidebar .menu-close').on('click', function (e) {
    $('.header-sidebar').removeClass('open');
});

$('.header-filter .btn-search').on('click', function (e) {
    if($("#category").val() == 'Categorias') {
        $('.error').html("Debe seleccionar una categoria.").css('display', 'block')
    }else if($("#search").val() == ''){
        $('.error').html("Debe ingresar una palabra.").css('display', 'block')
    }else {
        if ($(this).closest('.header-filter').find('#search').val().length) {
            $(this).closest('.header-filter').find('.form-input').removeClass('error');
            $(this).closest('.header-filter').find('form').submit();
        } else {
            $(this).closest('.header-filter').find('.form-input').addClass('error');
        }
    }
    
});

$("#search").keyup(function(){
    window.addEventListener("keypress", function(event){
        if (event.keyCode == 13){
            $('.btn-search').click()
        }
    },true);
})

var media768 = window.matchMedia('(min-width: 768px)');
if (media768.matches) {
    window.addEventListener("keypress", function(event){
        if (event.keyCode == 13){
            if($("#category").val() == 'Categorias') {
                $('.error').html("Debe seleccionar una categoria.").css('display', 'block')
            }else if($("#search").val() == ''){
                $('.error').html("Debe ingresar una palabra.").css('display', 'block')
            }
            event.preventDefault();
        }
    }, false);
}

if ($('#products_category').length > 0) {
    var page = 2;
    $(window).scroll(function () {
        var token = $('meta[name="csrf-token"]').prop('content');
        var content = $('section.product-categories');
        var loading = '<div class="loading text-center" style="margin-top: -64px"><img src="/img/loading.svg" alt=""></div>';
        if (Math.ceil($(window).scrollTop()) === $(document).height() - $(window).height()) {

            if (typeof request !== 'undefined') {
                window.request.abort();
                window.request = null;
            }
            if ($('.loading').length === 0) {
                content.append(loading);
            }
            window.request = $.post('/category-products', {_token: token, page: page}, function (response) {
                if (response.status) {
                    $('.product-carousel').slick('unslick');
                    content.append(response.data);
                    page++;
                }
            }).done(function (response) {
                if (response.status) {
                    $('.product-carousel').slick({
                        slidesToShow: 1, slidesToScroll: 1, variableWidth: true,
                        centerMode: true, infinite: true
                    });
                }
                $('.loading').remove();
            });
        }
    });
}

$('a#share').webuiPopover();

let frmComment = $('#form-comment');
if (frmComment.length > 0) {
    frmComment.on('submit', function (e) {
        e.preventDefault();
        $('.form-control').removeClass('error');
        $('.error-label').remove();
        $.ajax({
            url: frmComment.prop('action'),
            type: frmComment.prop('method'),
            data: frmComment.serialize(),
            success: function (response) {
                if (!response.status) {
                    $.each(response.data, function (i, v) {
                        $('#' + i).parent().parent().addClass('error');
                        $('#' + i).parent().append('<span class="error-label">' + v + '</span>');
                    });
                } else {
                    $('#media').html(response.data);
                    frmComment[0].reset();
                }
            }
        });
    });
    let page = 2;
    $('#more-comments').on('click', function (e) {
        e.preventDefault();
        let token = $('meta[name="csrf-token"]').prop('content');
        let content = $('#media');
        $.post('/more-comment', {_token: token, page: page, slug: $(this).data('slug')}, function (response) {
            if (response.status) {
                content.append(response.data);
                page++;
            }
        });
    })
}

$('.menu-link-hover').on('click', function() {
    var media768 = window.matchMedia('(max-width: 768px)');

    if (media768.matches) {
        $('.sub-menu').css('display', 'none');
        $('.li-sub-menu').slideToggle();
    }else {
        $('.li-sub-menu').css('display', 'none');
        $('.sub-menu').slideToggle();
    }
    
})