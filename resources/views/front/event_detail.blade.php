@extends('front.layouts.app')

@section('content')
    <section class="event-detail-banner" style="background: url({{ asset('img/event-detail.jpg') }})">
        <div class="container clearfix">
            <div class="title">
                <h2>Feria 2017: Convención Agrícola Tecnoagro</h2>
            </div>
        </div>
    </section>
    <section class="event-detail-desc">
        <div class="container">
            <div class="event-detail-text">
                <p>GRUPO BONNETT S.A. como empresa líder en el sector hidráulico ofrece una gran diversidad de equipos de bombeo, prestándole especial atención tanto a sus necesidades de calidad, competitividad y cumplimiento.</p>
                <p>&nbsp;</p>
                <p>En nuestra presentación a la Feria participamos con nuestros productos de las principales marcas del mundo con la mejor tecnología e innovación en ingeniería Hidráulica, como son PEDROLLO, IDEAL, NASTEC, CRI entre otros. Empresas que se dedican por muchas generaciones a optimizar el manejo del agua en la agricultura no solamente en extracción y conducción, también para modernos sistemas de riego.</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>Presentamos grandes novedades como los equipos de bombeo accionados con energía solar, bombas de eje vertical para pozos profundos, bombas inteligentes ahorradores de energía, etc.</p>
                <p>&nbsp;</p>
                <p>#GrupoBonnett #BonnettEnFeria</p>
            </div>
        </div>
    </section>
    <section class="event-detail-gallery">
        <div class="container">
            <ul class="gallery-list gallery-carousel">
                <li class="gallery-item"><img src="{{ asset('img/gallery-01.jpg') }}" alt=""></li>
                <li class="gallery-item"><img src="{{ asset('img/gallery-01.jpg') }}" alt=""></li>
                <li class="gallery-item"><img src="{{ asset('img/gallery-01.jpg') }}" alt=""></li>
                <li class="gallery-item"><img src="{{ asset('img/gallery-01.jpg') }}" alt=""></li>
                <li class="gallery-item"><img src="{{ asset('img/gallery-01.jpg') }}" alt=""></li>
                <li class="gallery-item"><img src="{{ asset('img/gallery-01.jpg') }}" alt=""></li>
            </ul>
        </div>
    </section>
    <section class="event-last sb">
        <div class="container">
            <div class="title">
                <h2>Eventos Recientes</h2>
            </div>
            <div class="event-list event-carousel">
                <figure class="event-item">
                    <a href="{{ route('front.event_detail') }}">
                        <div class="event-photo">
                            <img src="{{ asset('img/event-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="event-name">
                            <span>Evento 02</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="event-item">
                    <a href="{{ route('front.event_detail') }}">
                        <div class="event-photo">
                            <img src="{{ asset('img/event-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="event-name">
                            <span>Evento 02</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="event-item">
                    <a href="{{ route('front.event_detail') }}">
                        <div class="event-photo">
                            <img src="{{ asset('img/event-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="event-name">
                            <span>Evento 02</span>
                        </figcaption>
                    </a>
                </figure>
            </div>
            <div class="event-button">
                <a href="{{ route('front.events') }}" class="button"><span>Ver más</span></a>
            </div>
        </div>
    </section>
@endsection
