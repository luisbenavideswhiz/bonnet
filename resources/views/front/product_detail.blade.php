@extends('front.layouts.app')

@section('content')
    <section class="product-detail">
        <div class="container">
            <div class="product-title">
                <div class="title">
                    <h1>Electrobomba drenaje VXC</h1>
                </div>
                <nav class="breadcrumb">
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="#">Productos</a></li>
                        <li class="breadcrumb-item"><a href="#">Electrobombas</a></li>
                        <li class="breadcrumb-item">Drenaje</li>
                    </ul>
                </nav>
            </div>
            <div class="clearfix">
                <div class="left">
                    <div class="product-img">
                        <ul class="product-img-list product-detail-slider">
                            <li class="product-img-item">
                                <img src="{{ asset('img/product-02.jpg') }}" alt="">
                            </li>
                            <li class="product-img-item">
                                <img src="{{ asset('img/product-02.jpg') }}" alt="">
                            </li>
                            <li class="product-img-item">
                                <img src="{{ asset('img/product-02.jpg') }}" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="right">
                    <div class="product-desc">
                        <div class="product-summary">
                            <p>La electrobomba de drenaje VXC está construida en hierro fundido con espesor de
                                material consistente, gran robustez, alta resistencia y duración en el tiempo.</p>
                        </div>
                        <div class="product-id">
                            <p>ID: <span>PEDROLLO-VXC </span></p>
                        </div>
                        <div class="product-quota">
                            <p>Productos Disponibles: <span>10</span></p>
                        </div>
                    </div>
                    <div class="product-help">
                        <div class="product-contact">
                            <a href="{{ route('front.contact') }}" class="button"><span>Contactar</span></a>
                        </div>
                        <div class="product-cataloge">
                            <a href="#" class="button"><span>Cátalogo</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="product-description">
        <div class="container">
            <div class="tab">
                <ul class="tab-nav">
                    <li class="active"><a href="javascript:void(0)">Descripción</a></li>
                    <li><a href="javascript:void(0)">Modelos</a></li>
                    <li><a href="javascript:void(0)">Curvas</a></li>
                </ul>
                <ul class="tab-content">
                    <li class="tab-pane active">
                        <strong>CARACTERÍSTICAS</strong>
                        <ul>
                            <li>Nueva versión completamente rediseñada.</li>
                            <li>Alto grado de confiabilidad, doble sello mecánico.</li>
                            <li>Gran resistencia a la oxidación.</li>
                            <li>Caudal mejorado a un 30% de la versión anterior.</li>
                            <li>Rodete del tipo Vortex en acero inoxidable.</li>
                            <li>Temperatura max. 40°C.</li>
                        </ul>
                    </li>
                    <li class="tab-pane">
                        <p>easas</p>
                    </li>
                    <li class="tab-pane">
                        <p>4224</p>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="product-category sb">
        <div class="container">
            <div class="title">
                <h2>Productos Similares</h2>
            </div>
        </div>
        <div class="product-list product-slider">
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
        </div>
        <div class="product-category-btn">
            <a href="#" class="button"><span>Ver más</span></a>
        </div>
    </section>
@endsection
