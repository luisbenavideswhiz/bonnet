@extends('front.layouts.app')

@section('content')
    <section class="service-info summary">
        <div class="container clearfix">
            <div class="left">
                <div class="title">
                    <h2>Servicios</h2>
                </div>
            </div>
            <div class="right">
                <p>GRUPO BONNETT S.A. nació hace más de 39 años, para ayudar a las personas a solucionar problemas en la industria, la agricultura, la construcción y en la vida cotidiana. Desde su fundación mantiene una filosofía empresarial de crecimiento  colectivo e individual entre sus clientes, socios comerciales y colaboradores, con  valores muy arraigados en nuestra organización como la confianza, versatilidad, proximidad y eficiencia.    </p>
            </div>
        </div>
    </section>
    <section class="service-detail first sb">
        <div class="container">
            <div class="service-desc">
                <div class="title">
                    <h2>Ing. y Desarrollo de Proyectos</h2>
                </div>
                <div class="text">
                    <p>En BSH (Bonnett Servicios Hidráulicos) contamos con asesores especialistas en atender todo tipo de requerimiento y soporte hidráulico, así como el abastecimiento de repuestos para extender la vida útil de los productos que comercializamos.</p>
                    <p>Nuestro personal técnico está altamente calificado y capacitado para ejecutar los siguientes servicios:  </p>
                </div>
            </div>
            <div class="clearfix">
                <div class="left">
                    <div class="service-img">
                        <img src="{{ asset('img/home-about.jpg') }}" alt="">
                    </div>
                </div>
                <div class="right">
                    <div class="service-faq">
                        <ul class="faq-list">
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="service-detail second sb">
        <div class="container">
            <div class="service-desc">
                <div class="title">
                    <h2>Soporte Técnico</h2>
                </div>
                <div class="text">
                    <p>En BSH (Bonnett Servicios Hidráulicos) contamos con asesores especialistas en atender todo tipo de requerimiento y soporte hidráulico, así como el abastecimiento de repuestos para extender la vida útil de los productos que comercializamos.</p>
                    <p>Nuestro personal técnico está altamente calificado y capacitado para ejecutar los siguientes servicios:  </p>
                </div>
            </div>
            <div class="clearfix">
                <div class="right">
                    <div class="service-img">
                        <img src="{{ asset('img/home-about.jpg') }}" alt="">
                    </div>
                </div>
                <div class="left">
                    <div class="service-faq">
                        <ul class="faq-list">
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                            <li class="faq-item">
                                <a class="faq-link" href="javascript:void(0)">
                                    <h3>Visita Técnica In Situ</h3>
                                </a>
                                <div class="faq-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
