@extends('front.layouts.app')

@section('content')
    <section class="politic">
        <div class="container">
            <div class="title">
                <h2>Política de Calidad</h2>
            </div>
            <div class="politic-text">

                <p>Grupo Bonnett S.A., es una organización dedicada a la comercialización de equipos y accesorios para movimiento de fluidos para todo tipo de industrias, de
                    construcción, agricultura,  minería e industria en general y tiene como Política de Calidad, satisfacer las expectativas de nuestros Clientes manteniendo un alto
                    estándar de calidad de nuestros bienes y servicios.</p>

                <p>La Dirección promueve y respalda de forma visible y rigurosa la implementación de esta política, a través de:</p>

                <ul>
                    <li>El cumplimiento de todos los requisitos aplicables y otros compromisos que la organización asuma.</li>

                    <li>Promoviendo y asignando recursos suficientes para la capacitación técnica, entrenamiento en el desarrollo de las habilidades y competencias de los
                    colaboradores, así como en la sensibilización constante con asuntos relacionados con el sistema de gestión de calidad.</li>

                    <li>Entregar los bienes y servicios a nuestros clientes en forma eficiente para alcanzar el máximo nivel de satisfacción y propender la mejora continua en el
                    desarrollo de las operaciones de la empresa.</li>

                    <li>La presente política estará disponible, será conocida por los miembros de la organización, estará a disposición de las partes interesadas y será revisada
                    para su continua adecuación.</li>
                </ul>
                <p>&nbsp;</p>
                <p>GRUPO BONNETT S.A. es una empresa ética, que respeta las normas legales y actúa con responsabilidad social.</p>

                <p style="text-align: right">Dirección general</p>
            </div>
            <div class="politic-button">
                <a href="{{ route('front.home') }}" class="button"><span>Volver al inicio</span></a>
            </div>
        </div>
    </section>
@endsection
