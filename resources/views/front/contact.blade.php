@extends('front.layouts.app')

@section('content')
    <section class="contact">
        <div class="container">
            <div class="title">
                <h2>Contáctanos</h2>
            </div>
            <div class="contact-form form">
                <form action="">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-control">
                                    <div class="form-label">
                                        <label for="">Nombre</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control">
                                    <div class="form-label">
                                        <label for="">Apellido</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control">
                                    <div class="form-label">
                                        <label for="">DNI</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control">
                                    <div class="form-label">
                                        <label for="">RUC</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control">
                                    <div class="form-label">
                                        <label for="">Nombre de la empresa</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control">
                                    <div class="form-label">
                                        <label for="">Nivel de especialización</label>
                                    </div>
                                    <div class="form-select">
                                        <select name="" id="">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-control">
                                        <div class="form-label">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-control">
                                        <div class="form-label">
                                            <label for="">Celular</label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-control">
                                        <div class="form-label">
                                            <label for="">Producto de interés</label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-control">
                                        <div class="form-label">
                                            <label for="">¿Cómo nos contactó?</label>
                                        </div>
                                        <div class="form-select">
                                            <select name="" id="">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="form-label">
                                <label for="">Escribe tu mensaje</label>
                            </div>
                            <div class="form-textarea">
                                <textarea name="" id="" cols="30" rows="10" ></textarea>
                            </div>
                        </div>
                        <div class="form-button">
                            <button class="button"><span>Enviar</span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section class="find-us sb" id="find_map">
        <div class="container">
            <div class="title">
                <h2>Encuentranos en:</h2>
            </div>
            <google-map></google-map>
        </div>
    </section>
@endsection

@section('modal')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTLq7Y83-b11wtp2cu4GQakrZsd90D7Qk"></script>
@endsection
