@extends('front.layouts.app')

@section('content')
    <section class="certification">
        <div class="container">
            <div class="title">
                <h2>Nuestras Certificaciones</h2>
            </div>
            <ul class="certification-list">
                <li class="certification-item">
                    <img src="{{ asset('img/certificate-01.jpg') }}" alt="">
                </li>
                <li class="certification-item">
                    <img src="{{ asset('img/certificate-01.jpg') }}" alt="">
                </li>
                <li class="certification-item">
                    <img src="{{ asset('img/certificate-01.jpg') }}" alt="">
                </li>
                <li class="certification-item">
                    <img src="{{ asset('img/certificate-01.jpg') }}" alt="">
                </li>
            </ul>
            <div class="certification-button">
                <a href="{{ route('front.home') }}" class="button"><span>Volver al inicio</span></a>
            </div>
        </div>
    </section>
@endsection
