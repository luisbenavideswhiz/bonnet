@extends('front.layouts.app')

@section('content')
    <section class="banner">
        <ul class="banner-slider">
            <li class="banner-item" style="background-image: url({{ asset('img/banner.jpg') }})">
                <div class="container">
                    <div class="banner-info">
                        <div class="banner-title">
                            <h3>Electrobombas</h3>
                        </div>
                        <div class="banner-text">
                            <p>Adipiscing elit. Nulla quam velit, vulputate ph<br> vulputate eu phare  adipiscilit, vulputate eu phare  adipisci</p>
                        </div>
                        <div class="banner-button">
                            <a href="#" class="button"><span>Entérate de más</span></a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="banner-item" style="background-image: url({{ asset('img/banner.jpg') }})">
                <div class="container">
                    <div class="banner-info">
                        <div class="banner-title">
                            <h3>Electrobombas</h3>
                        </div>
                        <div class="banner-text">
                            <p>Adipiscing elit. Nulla quam velit, vulputate ph<br> vulputate eu phare  adipiscilit, vulputate eu phare  adipisci</p>
                        </div>
                        <div class="banner-button">
                            <a href="#" class="button"><span>Entérate de más</span></a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="banner-item" style="background-image: url({{ asset('img/banner.jpg') }})">
                <div class="container">
                    <div class="banner-info">
                        <div class="banner-title">
                            <h3>Electrobombas</h3>
                        </div>
                        <div class="banner-text">
                            <p>Adipiscing elit. Nulla quam velit, vulputate ph<br> vulputate eu phare  adipiscilit, vulputate eu phare  adipisci</p>
                        </div>
                        <div class="banner-button">
                            <a href="#" class="button"><span>Entérate de más</span></a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    <section class="home-about">
        <div class="container">
            <div class="title">
                <h2>Grupo Bonnett</h2>
            </div>
            <div class="clearfix">
                <div class="right">
                    <div class="home-img">
                        <img src="{{ asset('img/home-about.jpg') }}" alt="">
                    </div>
                </div>
                <div class="left">
                    <div class="home-text">
                        <p>GRUPO BONNETT S.A. nació hace más de 39 años, para ayudar a las personas
                            a solucionar problemas en la industria, la agricultura, la construcción y en la vida
                            cotidiana. Desde su fundación mantiene una filosofía empresarial de crecimiento
                            colectivo e individual entre sus clientes, socios comerciales y colaboradores, con
                            valores muy arraigados en nuestra organización como la confianza, versatilidad,
                            proximidad y eficiencia.</p>
                    </div>
                    <ul class="home-list">
                            <li>Confianza</li>
                            <li>Versatilidad</li>
                            <li>Proximidad</li>
                            <li>Eficiencia</li>
                    </ul>
                    <div class="home-button">
                        <a href="#" class="button"><span>Seguír leyendo</span></a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="home-service sb">
        <div class="container">
            <div class="title">
                <h2>Servicios</h2>
            </div>
            <div class="clearfix">
                <div class="left">
                    <div class="home-img">
                        <img src="{{ asset('img/home-about.jpg') }}" alt="">
                    </div>
                </div>
                <div class="right">
                    <ul class="home-list service-carousel">
                        <li class="home-list-item">
                            <div class="home-list-icon">
                                <span class="icon-users"></span>
                            </div>
                            <div class="home-list-info">
                                <div class="home-list-title">
                                    <h3>Ing. Y Desarrollo de Proyectos</h3>
                                </div>
                                <div class="home-list-text">
                                    <p>Nuestro equipo I.D.P. (Ingeniería y Desarrollo de Proyectos) cuenta con
                                        ingenieros y asesores especializados en diseñar, analizar y ejecutar proyectos
                                        de sistema de bombeo de agua para el sector civil, industrial y agrícola.</p>
                                </div>
                            </div>
                        </li>
                        <li class="home-list-item">
                            <div class="home-list-icon">
                                <span class="icon-users"></span>
                            </div>
                            <div class="home-list-info">
                                <div class="home-list-title">
                                    <h3>Ing. Y Desarrollo de Proyectos</h3>
                                </div>
                                <div class="home-list-text">
                                    <p>Nuestro equipo I.D.P. (Ingeniería y Desarrollo de Proyectos) cuenta con
                                        ingenieros y asesores especializados en diseñar, analizar y ejecutar proyectos
                                        de sistema de bombeo de agua para el sector civil, industrial y agrícola.</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="home-product sb">
        <div class="title">
            <h2>Nuestros Productos</h2>
        </div>
        <nav class="product-filter-nav">
            <ul class="product-filter-list">
                <li class="product-filter-item"><a class="button" href=""><span>Electrobombas</span></a></li>
                <li class="product-filter-item"><a class="button" href=""><span>Electrobombas</span></a></li>
                <li class="product-filter-item"><a class="button" href=""><span>Accesorios Hidraulicos</span></a></li>
                <li class="product-filter-item"><a class="button" href=""><span>Para Estacion de Servicio GLP</span></a></li>
                <li class="product-filter-item"><a class="button" href=""><span>Maquinarias</span></a></li>
                <li class="product-filter-item"><a class="button" href=""><span>Piscinas</span></a></li>
                <li class="product-filter-item"><a class="button" href=""><span>Equipo Fotovoltaicos</span></a></li>
                <li class="product-filter-item"><a class="button" href=""><span>Tratamiento de Agua</span></a></li>
            </ul>
        </nav>
        <div class="product-list product-slider">
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
        </div>
        <div class="product-button">
            <a href="{{ route('front.products') }}" class="button"><span>Ver más</span></a>
        </div>
    </section>
    <section class="home-alliance sb">
        <div class="container">
            <div class="title">
                <h2>Nuestros Aliados</h2>
            </div>
            <ul class="alliance-list">
                <li class="alliance-item"><img src="{{ asset('img/logo-01.svg') }}" width="262" alt=""></li>
                <li class="alliance-item"><img src="{{ asset('img/logo-02.svg') }}" width="183" alt=""></li>
                <li class="alliance-item"><img src="{{ asset('img/logo-03.svg') }}" width="124" alt=""></li>
                <li class="alliance-item"><img src="{{ asset('img/logo-04.svg') }}" width="170" alt=""></li>
                <li class="alliance-item"><img src="{{ asset('img/logo-05.svg') }}" width="160" alt=""></li>
            </ul>
        </div>
    </section>
@endsection

