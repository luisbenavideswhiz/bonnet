@extends('front.layouts.app')

@section('content')
    <section class="about-we summary">
        <div class="container clearfix">
            <div class="left">
                <div class="title">
                    <h2>Nosotros</h2>
                </div>
            </div>
            <div class="right">
                <p>GRUPO BONNETT S.A. nació hace más de 39 años, para ayudar a las personas a solucionar problemas en la industria, la agricultura, la construcción y en la vida cotidiana. Desde su fundación mantiene una filosofía empresarial de crecimiento  colectivo e individual entre sus clientes, socios comerciales y colaboradores, con  valores muy arraigados en nuestra organización como la confianza, versatilidad, proximidad y eficiencia.    </p>
            </div>
        </div>
    </section>
    <section class="about-mv">
        <div class="clearfix">
            <div class="left">
                <div class="about-vision">
                    <h3>Visión</h3>
                    <p>Ser una empresa sólida e innovadora, comprometida con la
                        satisfacción total de sus clientes, colaboradores y socios
                        comerciales, con políticas orientadas a una fuerte estructura
                        de servicio y responsabilidad social.
                    </p>
                </div>
            </div>
            <div class="right">
                <div class="about-mission">
                    <h3>Misión</h3>
                    <p>Proveer soluciones integrales para el movimiento de fluidos,
                        con una amplia línea de productos de gran calidad y alta eficiencia,
                        con experiencia, responsabilidad y profesionalismo.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="about-values">
        <div class="container">
            <div class="about-title title">
                <h2>Valores</h2>
            </div>
            <div class="clearfix">
                <div class="left">
                    <div class="about-img">
                        <img src="{{ asset('img/about-values.jpg') }}" alt="">
                    </div>
                </div>
                <div class="right">

                    <ul class="value-list">
                        <li>
                            <h3>Confianza</h3>
                            <p>En GRUPO BONNETT S.A creemos que el éxito se basa en la relación de confianza entre nuestros colaboradores, accionistas, clientes y socios comerciales logrando así establecer un vínculo estrecho que beneficie el desarrollo de sus proyectos.</p>
                        </li>
                        <li>
                            <h3>Versatilidad</h3>
                            <p>Es parte del día a día del GRUPO BONNETT S.A. responder ante cualquier desafío y estar dispuesto a encontrar la mejor solución a las necesidades de nuestros clientes.</p>
                        </li>
                        <li>
                            <h3>Proximidad</h3>
                            <p>GRUPO BONNETT S.A. se ha comprometido desde sus inicios en mantener un vínculo cercano con sus clientes para entender sus necesidades y ser parte de sus proyectos.</p>
                        </li>
                        <li>
                            <h3>Eficiencia</h3>
                            <p>GRUPO BONNETT S.A. comercializa productos de alta eficiencia con el menor costo de vida útil del mercado, a través de profesionales especializados que cuidarán su valiosa inversión. </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
