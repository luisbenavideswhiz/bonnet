@extends('front.layouts.app')

@section('content')
    <section class="event-info summary">
        <div class="container clearfix">
            <div class="left">
                <div class="title">
                    <h2>Eventos</h2>
                </div>
            </div>
            <div class="right">
                <p>GRUPO BONNETT S.A. nació hace más de 39 años, para ayudar a las personas a solucionar problemas en la industria, la agricultura, la construcción y en la vida cotidiana. Desde su fundación mantiene una filosofía empresarial de crecimiento  colectivo e individual entre sus clientes, socios comerciales y colaboradores, con  valores muy arraigados en nuestra organización como la confianza, versatilidad, proximidad y eficiencia.    </p>
            </div>
        </div>
    </section>
    <section class="event-detail first sb">
        <div class="container clearfix">
            <div class="left">
                <div class="event-img">
                    <img src="{{ asset('img/home-about.jpg') }}" alt="">
                </div>
            </div>
            <div class="right">
                <div class="title">
                    <h2>Evento 1</h2>
                </div>
                <div class="event-text">
                    <p>GRUPO BONNETT S.A. nació hace más de 39 años, para ayudar a las personas a solucionar problemas en la industria, la agricultura, la construcción y en la vida cotidiana. Desde su fundación mantiene una filosofía empresarial de crecimiento  colectivo e individual entre sus clientes, socios comerciales y colaboradores, con  valores muy arraigados en nuestra organización como la confianza, versatilidad, proximidad y eficiencia.    </p>
                </div>
                <div class="event-button">
                    <a href="{{ route('front.event_detail') }}" class="button"><span>Leer más</span></a>
                </div>
            </div>
        </div>
    </section>
    <section class="event-detail second sb">
        <div class="container clearfix">
            <div class="right">
                <div class="event-img">
                    <img src="{{ asset('img/home-about.jpg') }}" alt="">
                </div>
            </div>
            <div class="left">
                <div class="title">
                    <h2>Evento 1</h2>
                </div>
                <div class="event-text">
                    <p>GRUPO BONNETT S.A. nació hace más de 39 años, para ayudar a las personas a solucionar problemas en la industria, la agricultura, la construcción y en la vida cotidiana. Desde su fundación mantiene una filosofía empresarial de crecimiento  colectivo e individual entre sus clientes, socios comerciales y colaboradores, con  valores muy arraigados en nuestra organización como la confianza, versatilidad, proximidad y eficiencia.    </p>
                </div>
                <div class="event-button">
                    <a href="{{ route('front.event_detail') }}" class="button"><span>Leer más</span></a>
                </div>
            </div>
        </div>
    </section>
    <section class="event-detail first sb">
        <div class="container clearfix">
            <div class="left">
                <div class="event-img">
                    <img src="{{ asset('img/home-about.jpg') }}" alt="">
                </div>
            </div>
            <div class="right">
                <div class="title">
                    <h2>Evento 1</h2>
                </div>
                <div class="event-text">
                    <p>GRUPO BONNETT S.A. nació hace más de 39 años, para ayudar a las personas a solucionar problemas en la industria, la agricultura, la construcción y en la vida cotidiana. Desde su fundación mantiene una filosofía empresarial de crecimiento  colectivo e individual entre sus clientes, socios comerciales y colaboradores, con  valores muy arraigados en nuestra organización como la confianza, versatilidad, proximidad y eficiencia.    </p>
                </div>
                <div class="event-button">
                    <a href="{{ route('front.event_detail') }}" class="button"><span>Leer más</span></a>
                </div>
            </div>
        </div>
    </section>
    <div class="load-more">
        <a href="javascript:void(0)" class="button"><span>Ver más</span></a>
    </div>
@endsection
