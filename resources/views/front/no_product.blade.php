@extends('front.layouts.app')

@section('content')

<section class="search-product">
    <div class="container">
        <div class="search-product-message">
            <div class="title">
                <h2>Lo sentimos.<br>
                    No se encontró el producto</h2>
            </div>
            <div class="text">
                <p>Tal vez le interesa:</p>
            </div>
        </div>
        <div class="product-list">
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
            <figure class="product-item">
                <a href="{{ route('front.product_detail') }}">
                    <div class="product-photo">
                        <img src="{{ asset('img/product-01.jpg') }}" alt="">
                    </div>
                    <figcaption class="product-name">
                        <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                    </figcaption>
                </a>
            </figure>
        </div>
        <div class="search-product-button">
            <a href="javascript:void(0)" class="button"><span>Vér mas</span></a>
        </div>

    </div>
</section>

@endsection