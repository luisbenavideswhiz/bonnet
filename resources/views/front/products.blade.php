@extends('front.layouts.app')

@section('content')
    <section class="product-categories">
        <div class="product-category category-1">
            <div class="container">
                <div class="title">
                    <h2>Electrobombas</h2>
                    {{--<a class="button-filter" href="{{ route('front.product_filter') }}"><span class="icon-filter"></span> Filtro Detallado</a>--}}
                </div>
            </div>
            <div class="product-list product-slider">
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
            </div>
            <div class="product-category-btn">
                <a href="{{ route('front.product_filter') }}" class="button"><span>Ver más</span></a>
            </div>
        </div>
        <div class="product-category category-2 sb">
            <div class="container">
                <div class="title">
                    <h2>Accesorios Hidráulicos</h2>
                </div>
            </div>
            <div class="product-list product-slider">
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
            </div>
            <div class="product-category-btn">
                <a href="{{ route('front.product_filter') }}" class="button"><span>Ver más</span></a>
            </div>
        </div>
        <div class="product-category category-3 sb">
            <div class="container">
                <div class="title">
                    <h2>Para Estación de Servicio GLP</h2>
                </div>
            </div>
            <div class="product-list product-slider">
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
                <figure class="product-item">
                    <a href="{{ route('front.product_detail') }}">
                        <div class="product-photo">
                            <img src="{{ asset('img/product-01.jpg') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                        </figcaption>
                    </a>
                </figure>
            </div>
            <div class="product-category-btn">
                <a href="{{ route('front.product_filter') }}" class="button"><span>Ver más</span></a>
            </div>
        </div>
    </section>
@endsection
