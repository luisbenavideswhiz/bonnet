@extends('front.layouts.app')

@section('content')
    <section class="thank">
        <div class="container">
            <div class="thank-img">
                <img src="{{ asset('img/send.png') }}" width="347" alt="">
            </div>
            <div class="title">
                <h2>Su mensaje fue enviado con éxito.</h2>
            </div>
            <ul class="button-group">
                <li>
                    <a class="button" href="{{ route('front.home') }}"><span>Inicio</span></a>
                </li>
                <li>
                    <a class="button" href="{{ route('front.products') }}"><span>Productos</span></a>
                </li>
            </ul>
        </div>
    </section>
@endsection
