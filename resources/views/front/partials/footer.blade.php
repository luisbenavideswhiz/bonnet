<footer class="footer">
    <div class="footer-info">
        <div class="container">
            <a href="javascript:void(0)" class="back-top"></a>
            <div class="footer-about footer-item">
                <h3>Grupo Bonnet</h3>
                <p>Grupo Bonnett S.A nació el 24 de
                        setiembre de 1977, en la ciudad
                        de la Merced, provincia de
                        Chanchamayo, departamento
                        de Junín - Perú.</p>
            </div>
            <div class="footer-links footer-item">
                <h3>Enlaces Rapidos</h3>
                <ul>
                    <li><a href="{{ route('front.certifications') }}">Certificaciones</a></li>
                    <li><a href="{{ route('front.politics') }}">Política de Calidad</a></li>
                    <li><a href="{{ route('front.contact') }}">Sucursales</a></li>
                </ul>
            </div>
            <div class="footer-links footer-products footer-item">
                <h3>Productos</h3>
                <ul>
                    <li><a href="#">Electrobombas</a></li>
                    <li><a href="#">Accesorios hidaulicos</a></li>
                    <li><a href="#">Para Estacón de servicio GLP</a></li>
                    <li><a href="#">Certificaciones</a></li>
                    <li><a href="#">Maquinarias</a></li>
                    <li><a href="#">Piscinas</a></li>
                    <li><a href="#">Tratamiento de agua</a></li>
                    <li><a href="#">Equipos fotovoltaicos</a></li>
                </ul>
            </div>
            <div class="footer-social footer-item">
                <h3>Redes Sociales</h3>
                <ul class="social-list">
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-facebook"></span></a>
                    </li>
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-twitter"></span></a>
                    </li>
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-instagram"></span></a>
                    </li>
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-linkedin"></span></a>
                    </li>
                </ul>
            </div>

            <div class="footer-contact footer-item">
                <div class="footer-item-title">
                    <h3>Contáctenos</h3>
                </div>
                <div class="footer-item-collapse">
                    <ul class="contact-list">
                        <li class="icon-mobile"><a href="tel:+5117192121">719-2121</a> / <a href="tel:+5114461587">446-1587</a> /
                            <a href="tel:+5114460349">446-0349</a></li>
                        <li class="icon-mail"><a href="mailto:web@grupobonnett.com">web@grupobonnett.com</a></li>
                        <li class="icon-map">Jr. Domingo Martinez Luján 1155
                            Surquillo, Lima, Peru</li>
                    </ul>
                </div>
                <ul class="social-list">
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-facebook"></span></a>
                    </li>
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-twitter"></span></a>
                    </li>
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-instagram"></span></a>
                    </li>
                    <li class="social-item">
                        <a href="#" target="_blank"><span class="icon-linkedin"></span></a>
                    </li>
                </ul>
            </div>
            <div class="footer-links-2 footer-item">
                <div class="footer-item-title">
                    <h3>Enlaces Rapidos</h3>
                </div>
                <div class="footer-item-collapse">
                    <ul>
                        <li><a href="{{ route('front.certifications') }}">Certificaciones</a></li>
                        <li><a href="{{ route('front.politics') }}">Política de Calidad</a></li>
                        <li><a href="{{ route('front.contact') }}">Sucursales</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <ul>
                <li>Grupo Bonnett - Todos los derechos reservados 2018</li>
                <li>Powered by <a href="#" target="_blank">WHIZ</a></li>
            </ul>
        </div>
    </div>
</footer>