<header class="header">
    <a href="http://bonnettretail.com/" class="header-top">
        <div class="container">
            <p>¿Interesado en algún producto?<span class="icon icon-right"></span></p>
        </div>
    </a>
    <div class="header-cnt">
        <div class="container clearfix">
            <div class="left">
                <div class="logo">
                    <a href="{{ route('front.home') }}"><img src="{{ asset('img/logo.png') }}" width="260" height="80" alt="Logo"></a>
                </div>
            </div>
            <div class="right">
                <div class="header-filter">
                    <form action="{{ route('front.product_filter') }}" method="GET">
                        <div class="form-dropdown">
                            <div class="form-select">
                                <select name="category" id="category">
                                    <option>Categorias</option>
                                    <option value="1">Electrobombas</option>
                                    <option value="2">Accesorios Hidráulicos</option>
                                    <option value="3">Para estación de Servicio GLP</option>
                                    <option value="4">Maquinarias</option>
                                    <option value="5">Piscinas</option>
                                    <option value="6">Equipos Fotovoltaicos</option>
                                    <option value="7">Tatamiento de agua</option>
                                </select>
                            </div>
                            <a class="dropdown-button" href="javascript:void(0)">Categorias</a>
                            <ul class="dropdown-list">
                                <li class="dropdown-item"><a href="javascript:void(0)">Electrobombas</a></li>
                                <li class="dropdown-item"><a href="javascript:void(0)">Accesorios Hidráulicos</a></li>
                                <li class="dropdown-item"><a href="javascript:void(0)">Para estación de Servicio GLP</a></li>
                                <li class="dropdown-item"><a href="javascript:void(0)">Maquinarias</a></li>
                                <li class="dropdown-item"><a href="javascript:void(0)">Piscinas</a></li>
                                <li class="dropdown-item"><a href="javascript:void(0)">Equipos Fotovoltaicos</a></li>
                                <li class="dropdown-item"><a href="javascript:void(0)">Tatamiento de agua</a></li>
                            </ul>
                        </div>
                        <div class="form-input">
                            <input type="text" id="search" name="q" placeholder="Estoy buscando...">
                        </div>
                        <div class="form-button">
                            <button><span class="icon-search"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mobile">
        <div class="container">
            <div class="clearfix">
                <div class="left">
                    <a class="button-menu" href="javascript:void(0)">
                        <span class="bar1"></span>
                        <span class="bar2"></span>
                        <span class="bar3"></span>
                    </a>
                    <div class="logo">
                        <a href="{{ route('front.home') }}"><img src="{{ asset('img/logo-min.png') }}" width="32" height="32" alt="Logo"></a>
                    </div>
                    <div class="title">
                        <span>Titulo</span>
                    </div>
                </div>
                <div class="right">
                    <a href="javascript:void(0)" class="button-search"><span class="icon-search"></span></a>
                </div>
            </div>
            <div class="header-mobile-search">
                <div class="form-input">
                    <input type="search" name="search" placeholder="Estoy buscando…">
                </div>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <a href="javascript:void(0)" class="menu-close"><span class="icon-close"></span></a>
        <div class="logo">
            <a href="{{ route('front.home') }}"><img src="{{ asset('img/logo-white.png') }}" width="32" height="32" alt="Logo"></a>
        </div>
        <div class="menu">
            <nav class="menu-primary">
                <ul class="menu-list">
                    <li class="menu-item active">
                        <a class="menu-link" href="{{ route('front.about') }}">Nosotros</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('front.products') }}">Productos</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('front.services') }}">Servicios</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('front.contact') }}">Contacto</a>
                    </li>
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('front.events') }}">Eventos</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>

<div class="header-scroll">
    <a href="http://bonnettretail.com/" class="header-top">
        <div class="container">
            <p>¿Interesado en algún producto?<span class="icon icon-right"></span></p>
        </div>
    </a>
    <div class="header-bottom">
        <div class="container clearfix">
            <div class="left">
                <div class="logo">
                    <a href="{{ route('front.home') }}"><img src="{{ asset('img/logo-min.png') }}" width="58" height="58" alt="Logo"></a>
                </div>
            </div>
            <div class="right">
                <nav class="menu-secondary">
                    <ul class="menu-list">
                        <li class="menu-item active">
                            <a class="menu-link" href="{{ route('front.about') }}">Nosotros</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{ route('front.products') }}">Productos</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{ route('front.services') }}">Servicios</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{ route('front.contact') }}">Contacto</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{ route('front.events') }}">Eventos</a>
                        </li>
                    </ul>
                </nav>
                <div class="header-filter minify">
                    <div class="form-dropdown">
                        <a class="dropdown-button" href="javascript:void(0)">Categorias</a>
                        <ul class="dropdown-list">
                            <li class="dropdown-item"><a href="javascript:void(0)">Electrobombas</a></li>
                            <li class="dropdown-item"><a href="javascript:void(0)">Accesorios Hidráulicos</a></li>
                            <li class="dropdown-item"><a href="javascript:void(0)">Para estación de Servicio GLP</a></li>
                            <li class="dropdown-item"><a href="javascript:void(0)">Maquinarias</a></li>
                            <li class="dropdown-item"><a href="javascript:void(0)">Piscinas</a></li>
                            <li class="dropdown-item"><a href="javascript:void(0)">Equipos Fotovoltaicos</a></li>
                            <li class="dropdown-item"><a href="javascript:void(0)">Tatamiento de agua</a></li>
                        </ul>
                    </div>
                    <div class="form-input">
                        <input type="text" placeholder="Estoy buscando...">
                    </div>
                    <div class="form-button">
                        <button><span class="icon-search"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>