@extends('front.layouts.app')

@section('content')
    <section class="error-404">
        <div class="container">
            <div class="error-404-img">
                <img src="{{ asset('img/404.png') }}" width="625" alt="">
            </div>
            <div class="title">
                <h2>Lo sentimos, la página que está<br>
                    buscando no existe.</h2>
            </div>
            <ul class="button-group">
                <li>
                    <a class="button" href="{{ route('front.home') }}"><span>Inicio</span></a>
                </li>
                <li>
                    <a class="button" href="{{ route('front.products') }}"><span>Productos</span></a>
                </li>
            </ul>
        </div>
    </section>
@endsection
