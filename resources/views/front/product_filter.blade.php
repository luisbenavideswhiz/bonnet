@extends('front.layouts.app')

@section('content')
    <section class="product-filter">
        <div class="container">
            <nav class="product-filter-nav">
                <ul class="product-filter-list">
                    <li class="product-filter-item active"><a class="button" href=""><span>Electrobombas</span></a></li>
                    <li class="product-filter-item"><a class="button" href=""><span>Electrobombas</span></a></li>
                    <li class="product-filter-item"><a class="button" href=""><span>Accesorios Hidraulicos</span></a></li>
                    <li class="product-filter-item"><a class="button" href=""><span>Para Estacion de Servicio GLP</span></a></li>
                    <li class="product-filter-item"><a class="button" href=""><span>Maquinarias</span></a></li>
                    <li class="product-filter-item"><a class="button" href=""><span>Piscinas</span></a></li>
                    <li class="product-filter-item"><a class="button" href=""><span>Equipo Fotovoltaicos</span></a></li>
                    <li class="product-filter-item"><a class="button" href=""><span>Tratamiento de Agua</span></a></li>
                </ul>
            </nav>
            <div class="clearfix">
                <div class="left">
                    <div class="product-filter-sidebar">
                        <div class="filter-title">
                            <span class="icon-filter"></span> Filtro Detallado
                        </div>
                        <div class="product-filter-collapse">
                            <div class="filter-widget">
                                <div class="filter-widget-title">
                                    <h3>Tipos</h3>
                                </div>
                                <div class="filter-widget-collapse">
                                    <ul class="filter-widget-list">
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_1"><label for="fi_1"><span class="checkbox"></span> Periféricas</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_2"><label for="fi_2"><span class="checkbox"></span> Centrífugas</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_3"><label for="fi_3"><span class="checkbox"></span> Autocebantes</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_4"><label for="fi_4"><span class="checkbox"></span> Multitapicas</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_5"><label for="fi_5"><span class="checkbox"></span> Sumeraibles</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_6"><label for="fi_6"><span class="checkbox"></span> Pera Drenajes</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_7"><label for="fi_7"><span class="checkbox"></span> De Acero Inoxidable</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="filter-widget">
                                <div class="filter-widget-title">
                                    <h3>Marca</h3>
                                </div>
                                <div class="filter-widget-collapse">
                                    <ul class="filter-widget-list">
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_8"><label for="fi_8"><span class="checkbox"></span> Nastec</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_9"><label for="fi_9"><span class="checkbox"></span> Pedrollo</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_10"><label for="fi_10"><span class="checkbox"></span> CRI</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_11"><label for="fi_11"><span class="checkbox"></span> Ideal</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_12"><label for="fi_12"><span class="checkbox"></span> Franklin</label>
                                            </div>
                                        </li>
                                        <li class="filter-widget-item">
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="fi_13"><label for="fi_13"><span class="checkbox"></span> Jinko Solar</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <div class="product-list">
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                        <figure class="product-item">
                            <a href="{{ route('front.product_detail') }}">
                                <div class="product-photo">
                                    <img src="{{ asset('img/product-01.jpg') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>Electrobomba Centrífuga  HF Alto Caudal</span>
                                </figcaption>
                            </a>
                        </figure>
                    </div>
                    <div class="product-pagination">
                        <ul class="pagination">
                            <li><a href="#"><span class="icon icon-arrow-left"></span></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><span class="icon icon-arrow-right"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
