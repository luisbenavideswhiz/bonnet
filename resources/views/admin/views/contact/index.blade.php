@extends('admin.layout.base')

@section('bodyId')id="contact"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Mensajes</header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <div id="date_filter" class="row">
                                <div class="col-md-2">
                                    <span id="date-label-from" class="date-label">Desde: </span>
                                    <input class="date_range_filter date form-control" type="text" id="datepicker_from"/>
                                </div>
                                <div class="col-md-2">
                                    <span id="date-label-to" class="date-label">Hasta:</span>
                                    <input class="date_range_filter date form-control" type="text" id="datepicker_to"/>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <button type="button" class="btn-filter-dates btn btn-primary">Filtrar fechas</button>
                                </div>
                                <div class="col-md-6">
                                    <br>
                                    <a href="{{ url('admin/contact/download') }}" class="btn btn-success pull-right">Descargar Excel</a> 
                                </div>
                            </div>
                            <table class="display table table-bordered table-striped data-table">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Nombre de la Empresa</th>
                                    <th>Especialización</th>
                                    <th>Otros</th>
                                    <th>Fecha</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contacts as $key => $value)
                                    <tr>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->last_name }}</td>
                                        <td>{{ $value->business_name }}</td>
                                        <td>{{ $value->specialization_id != 0 ? $value->specialization->meta_value: 'Otros' }}</td>
                                        <td>{{ $value->other }}</td>
                                        <td>{{ $value->created_at->format('d/m/Y h:i a') }}</td>
                                        <td class="text-center">
                                            <a href="#message" data-toggle="modal"
                                               data-url="{{ route('contact.show', ['id'=>$value->id]) }}"
                                               class="btn btn-info message"><i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    @component('admin.partials.modals.message')
        @slot('data')
            <strong>DNI :</strong><input id="_document" class="form-control" disabled>
            <strong>Correo :</strong><input id="_email" class="form-control" disabled>
            <strong>Celular :</strong><input id="_phone" class="form-control" disabled>
            <strong>Producto de interés :</strong><input id="_product_interest" class="form-control" disabled>
            <strong>Mensaje :</strong><textarea id="_message" class="form-control" rows="5" disabled></textarea>
        @endslot
    @endcomponent
@endsection