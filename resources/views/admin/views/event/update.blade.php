@extends('admin.layout.base')

@section('bodyId')id="event"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('event.update', ['id'=>$event->id]) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar evento</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Título</label>
                                                <input class="form-control m-bot15" type="text" name="title"
                                                       value="{{ $event->title }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Descripción corta</label>
                                                <textarea name="text"
                                                          class="ckeditor form-control">{{ $event->text }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Descripción completa</label>
                                                <textarea name="description"
                                                          class="ckeditor form-control">{{ $event->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('event.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        @component('admin.partials.upload-image', ['path' => isset($event->image) ? $event->image->path : ''])
                                            @slot('title')Las imágenes deben de medir 740x560 px @endslot
                                            @slot('inputName') image @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('admin.partials.message')

                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-8">
                <div class="panel">
                    <header class="panel-heading">Galeria
                        <span class="tools pull-right"><a href="javascript:;" class="fa fa-chevron-down"></a></span>
                    </header>
                    <div class="panel-body">
                        @component('admin.partials.upload-gallery',
                        [
                            'gallery'=> $event->gallery,
                            'type'=>'EVENT',
                            'is_image' => true
                        ])
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script id="template-upload" type="text/x-tmpl">@include('admin.templates.multiple-upload')</script>
    <script id="template-download" type="text/x-tmpl">@include('admin.templates.multiple-display')</script>
    @include('admin.partials.modals.cropper_image')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper', '.modal-btn-confirm', '#input_image', 740, 560);
    </script>

    {{--Vendor to gallery--}}
    <script src="{{ asset('js/vendor/file-uploader/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/tmpl.min.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/load-image.all.min.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/canvas-to-blob.min.js') }}"></script>

    <script src="{{ asset('js/vendor/file-uploader/jquery.blueimp-gallery.min.js') }}"></script>

    <script src="{{ asset('js/vendor/file-uploader/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-ui.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').prop('content')
                }
            });

            $('#fileupload').fileupload({
                url: '/admin/gallery/event',
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                formData: {
                    id: {{ $event->id }}
                }
            });
        });
    </script>
@endsection