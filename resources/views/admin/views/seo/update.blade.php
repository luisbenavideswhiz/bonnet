@extends('admin.layout.base')

@section('bodyId')id="seo"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('seo.update', ['id'=> $seo->id]) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar SEO</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Categorías</label>
                                                <select class="form-control" id="category" name="category">
                                                    <option value="">Seleccionar</option>
                                                    <option value="web" @if($seo->category === 'web')selected @endif>
                                                        Sección de la web
                                                    </option>
                                                    <option value="main"
                                                            @if($seo->category === 'main')selected @endif>
                                                        Categorías Pricipales
                                                    </option>
                                                    <option value="product"
                                                            @if($seo->category === 'product')selected @endif>
                                                        Productos
                                                    </option>
                                                    <option value="event"
                                                            @if($seo->category === 'event')selected @endif>
                                                        Eventos
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Sección</label>
                                                <select class="form-control" id="name" name="name">
                                                    <option value="">Seleccionar</option>
                                                    <option value="{{ $seo->name }}"
                                                            selected>{{ $seo->full_name }}</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Título</label>
                                                <input type="text" class="form-control" name="title"
                                                       value="{{ $seo->title }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Url</label>
                                                <input type="text" class="form-control" name="url"
                                                       value="{{ $seo->url }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Descripción</label>
                                                <input type="text" class="form-control" name="description"
                                                       value="{{ $seo->description }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Alt</label>
                                                <input type="text" class="form-control" name="alt"
                                                       value="{{ $seo->alt }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">keywords (Separados por enter)</label>
                                                <input type="text" class="form-control tagsinput" name="keywords"
                                                       value="{{ $seo->keywords }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('seo.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    @component('admin.partials.upload-image', ['path'=> $seo->image])
                                        @slot('title')La imagen debe ser de 540x560 px @endslot
                                        @slot('inputName')image @endslot
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </section>
    @include('admin.partials.modals.cropper_image')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/jquery.tagsinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper',
            '.modal-btn-confirm', '#input_image', 540, 560);
    </script>
@endsection