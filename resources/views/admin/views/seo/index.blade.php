@extends('admin.layout.base')

@section('bodyId')id="seo"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">SEO
                        <a href="{{ route('seo.create') }}" class="pull-right btn btn-success">Nuevo
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered">
                                <thead>
                                <tr>
                                    <th>Categoría</th>
                                    <th>URL</th>
                                    <th>Título</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    @component('admin.partials.modals.delete')
        @slot('title') Eliminar producto @endslot
        @slot('message') Seguro de eliminar el producto : @endslot
    @endcomponent
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection