@extends('admin.layout.base')

@section('bodyId')id="slider"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('second-slider.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar Banner Secundario</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Texto central</label>
                                                <input class="form-control m-bot15" type="text" name="text_center"
                                                       value="{{ isset($text_center) ? $text_center->meta_value : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Texto izquierda</label>
                                                <input class="form-control m-bot15" type="text" name="text_left"
                                                       value="{{ isset($text_left) ? $text_left->meta_value : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Link izquierda</label>
                                                <input class="form-control m-bot15" type="text" name="link_left"
                                                       value="{{ isset($text_left) ? $text_left->meta_option : ''}}"
                                                       placeholder="http://..">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Texto derecha</label>
                                                <input class="form-control m-bot15" type="text" name="text_right"
                                                       value="{{ isset($text_right) ? $text_right->meta_value : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Link derecha</label>
                                                <input class="form-control m-bot15" type="text" name="link_right"
                                                       value="{{ isset($text_right) ? $text_right->meta_option : ''}}"
                                                       placeholder="http://..">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('admin.alliance') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection