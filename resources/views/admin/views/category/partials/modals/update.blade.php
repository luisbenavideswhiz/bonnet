<div class="modal fade modal-dialog-center" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar Categoría</h4>
                </div>
                <form method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-lg-6">
                                @component('admin.partials.upload-image')
                                    @slot('title')Las imágenes deben de medir 540x640 px @endslot
                                    @slot('inputName')image @endslot
                                @endcomponent
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name">Nombre :</label>
                                    <input type="text" class="form-control" id="name_u" name="name">
                                </div>

                                <div class="form-group">
                                    <label for="description">Descripción :</label>
                                    <input type="text" class="form-control" id="description_u" name="description">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" type="button">Cerrar</a>
                        <button class="btn btn-warning"> Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>