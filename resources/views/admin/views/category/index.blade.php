@extends('admin.layout.base')

@section('bodyId')id="category"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Categorias
                        <a href="#create" class="pull-right btn btn-success" data-toggle="modal">Nuevo
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered" id="category_table">
                                <thead>
                                <tr>
                                    <th style="width: 70px;">Orden</th>
                                    <th>Nombre</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    @include('admin.views.category.partials.modals.create')
    @include('admin.views.category.partials.modals.update')
    @include('admin.partials.modals.cropper_image')

    @component('admin.partials.modals.delete')
        @slot('title')Eliminar Categoría @endslot
        @slot('message')Seguro de eliminar la categoría : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper', '.modal-btn-confirm', '#input_image', 540, 640);
        Form.form('form', true);
    </script>
@endsection