@extends('admin.layout.base')

@section('bodyId')id="user"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Usuarios
                        <a href="{{ route('user.create') }}" class="pull-right btn btn-success">Nuevo Usuario
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped data-table">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Correo</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $value)
                                    <tr>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->last_name }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('user.edit', ['id'=> $value->id]) }}"
                                               class="btn btn-warning"><i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <a href="#delete" data-toggle="modal" class="btn btn-danger"
                                               data-url="{{ route('user.show', ['id'=> $value->id]) }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

    @component('admin.partials.modals.delete')
        @slot('title') Eliminar usero @endslot
        @slot('message') Seguro de eliminar al usuario : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection