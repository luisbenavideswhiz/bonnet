@extends('admin.layout.base')

@section('bodyId')id="user"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('user.update',['id'=> $user->id]) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar Usuario</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Nombres</label>
                                                        <input class="form-control m-bot15" type="text" name="name"
                                                               value="{{ $user->name }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Apellidos</label>
                                                        <input class="form-control m-bot15" type="text"
                                                               name="last_name" value="{{ $user->last_name }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Rol del usuario</label>
                                                <select class="form-control m-bot15" name="role">
                                                    <option value="">Seleccione</option>
                                                    @foreach($roles as $key => $value)
                                                        <option value="{{ $value->name }}"
                                                                @if(count($user->getRoles()) > 0)
                                                                @if($user->getRoles()[0] === $value->name)
                                                                selected @endif @endif
                                                        >{{ $value->title }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Correo</label>
                                                <input class="form-control m-bot15" type="text" name="email"
                                                       value="{{ $user->email }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Contraseña (genérica)</label>
                                                <input class="form-control m-bot15" type="password" name="password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('user.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection