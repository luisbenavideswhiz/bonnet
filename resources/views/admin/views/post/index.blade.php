@extends('admin.layout.base')
@section('bodyId')id="post"@endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">Blogs
                        <a href="{{ route('post.create') }}" class="pull-right btn btn-success">Nuevo
                            <i class="fa fa-plus"></i></a></div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped" id="post-table">
                                <thead>
                                <tr>
                                    <th style="width: 60px;">Orden</th>
                                    <th>Título</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @component('admin.partials.modals.delete')
        @slot('title') Eliminar blog @endslot
        @slot('message') Seguro de eliminar el blog : @endslot
    @endcomponent
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
@endsection