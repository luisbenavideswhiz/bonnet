@extends('admin.layout.base')
@section('bodyId')id="post"@endsection
@section('content')
    <section class="wrapper">
        <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Nuevo Blog</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Título</label>
                                                <input class="form-control m-bot15" type="text" name="title">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Ruta</label>
                                                <input class="form-control m-bot15" type="text" name="slug">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Descripción corta</label>
                                                <textarea name="short_description"
                                                          class="form-control m-bot15"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Detalle</label>
                                                <textarea name="description"
                                                          class="ckeditor form-control m-bot15"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">Etiquetas (Separados por enter)</label>
                                                <input type="text" class="form-control m-bot15 tagsinput" name="tags">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('post.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        @component('admin.partials.upload-image')
                                            @slot('title') La imagen debe ser de 1080 x 780 @endslot
                                            @slot('inputName')image @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    @include('admin.partials.modals.cropper_image')
@endsection

@section('js')
    {{--Cropper--}}
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>
    {{--Form--}}
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>

    <script>
        $(function() {
            if (typeof CKEDITOR != 'undefined') {
                CKEDITOR.replace('short_description', {
                    wordcount: {
                        showParagraphs: false,
                        showWordCount: false,
                        showCharCount: true,
                        countSpacesAsChars: false,
                        countHTML: false,
                        maxCharCount: 130
                    }
                });
            }
        });
    </script>
@endsection