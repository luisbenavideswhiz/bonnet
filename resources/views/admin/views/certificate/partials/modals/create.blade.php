<div class="modal fade modal-dialog-center" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Nuevo Certificado</h4>
                </div>
                <form action="{{ route('certificate.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                @component('admin.partials.upload-image')
                                    @slot('title')Las imágenes deben de medir 270x380 px @endslot
                                    @slot('inputName')image @endslot
                                @endcomponent
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name">Nombre :</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" type="button">Cerrar</a>
                        <button class="btn btn-warning"> Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>