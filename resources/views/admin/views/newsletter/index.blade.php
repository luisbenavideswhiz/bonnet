@extends('admin.layout.base')
@section('bodyId')id="newsletter"@endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <header class="panel-heading">Newsletters
                        <a href="{{ route('newsletter.create') }}" class="pull-right btn btn-success">Nuevo
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="adv-table">
                            <table id="newsletters" class="display table table-bordered">
                                <thead>
                                <tr>
                                    <th>Título</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @component('admin.partials.modals.delete')
        @slot('title') Eliminar newsletter @endslot
        @slot('message') Seguro de eliminar el newsletter : @endslot
    @endcomponent
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection