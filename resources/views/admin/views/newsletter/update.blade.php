@extends('admin.layout.base')

@section('bodyId')id="newsletter"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('newsletter.update', ['id'=>$newsletter->id]) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar newsletter</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Título</label>
                                                <input class="form-control m-bot15" type="text" name="title"
                                                       value="{{ $newsletter->title }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Descripción</label>
                                                <textarea class="form-control" name="description"
                                                          rows="5">{{ $newsletter->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Link</label>
                                                <input class="form-control m-bot15" type="text" name="link"
                                                       value="{{ $newsletter->link }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Nombre del botón</label>
                                                <input class="form-control m-bot15" type="text" name="button_name"
                                                       value="{{ $newsletter->button_name }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Asunto</label>
                                                <input class="form-control m-bot15" type="text" name="affair"
                                                       value="{{ $newsletter->affair }}">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Texto previo</label>
                                                <input class="form-control m-bot15" type="text" name="preview_text"
                                                       value="{{ $newsletter->preview_text }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Blogs (max 3)</label>
                                                <select name="posts[]" class="form-control select2" multiple>
                                                    @forelse($posts as $key => $value)
                                                        <option value="{{ $value->id }}"
                                                                @if(in_array($value->id, $newsletter->posts->pluck('id')->toArray())) selected @endif>{{ $value->title }}</option>
                                                    @empty @endforelse
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('newsletter.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-body bg-blue">
                            <strong class="text-white">
                                Haz clic en guardar, luego regresa al menú anterior
                                para enviar el mail a la Base de datos.
                            </strong>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen banner secundario</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        @component('admin.partials.upload-image', ['path'=> $newsletter->image])
                                            @slot('title')Las imágenes deben de medir 700x340 px @endslot
                                            @slot('inputName') image @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    @include('admin.partials.modals.cropper_image')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper',
            '.modal-btn-confirm', '#input_image', 700, 340);
    </script>
@endsection