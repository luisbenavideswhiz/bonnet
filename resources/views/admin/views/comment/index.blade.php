@extends('admin.layout.base')
@section('bodyId')id="comment"@endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">Blog : {{ $post->title }}
                        <a href="{{ route('post.index') }}" class="pull-right btn btn-default">
                            <i class="fa fa-arrow-left"></i> Regresar</a>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th>Fecha</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($post->comments as $key => $value)
                                    <tr>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td>{{ $value->created_at->format('d/m/Y h:s A') }}</td>
                                        <td class="text-center">
                                            <a href="#comments" data-toggle="modal" class="btn btn-warning"
                                               data-url="{{ route('comment.show', ['id'=> $value->id]) }}">
                                                <i class="fa fa-comment-o"></i></a>
                                            <a href="#delete" data-toggle="modal" class="btn btn-danger"
                                               data-url="{{ route('comment.show', ['id'=> $value->id]) }}">
                                                <i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @component('admin.partials.modals.delete')
        @slot('title') Eliminar comentario @endslot
        @slot('message') Seguro de eliminar el comentario de : @endslot
    @endcomponent
    @component('admin.partials.modals.comment')
        @slot('data')
            <strong>Mensaje :</strong><textarea id="_message" class="form-control" rows="5" disabled></textarea>
        @endslot
    @endcomponent
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
@endsection