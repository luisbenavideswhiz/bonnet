@extends('admin.layout.base')

@section('bodyId')id="footer"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ url('/admin/home/footer') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar Footer</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Correo</label>
                                                <input class="form-control m-bot15" type="text" name="email"
                                                       value="{{ isset($email) ? $email : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Dirección</label>
                                                <input class="form-control m-bot15" type="text" name="address"
                                                       value="{{ isset($address) ? $address : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Teléfonos</label>
                                                <input class="form-control m-bot15 tagsinput" type="text" name="phone"
                                                       value="{{ isset($phone) ? $phone : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">Grupo Bonnett</label>
                                                <textarea name="description"
                                                          class="ckeditor form-control">{{ isset($description) ? $description : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('admin.alliance') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading" style="height: 55px">Redes sociales
                            <button type="button" class="btn btn-primary pull-right" id="add-network">
                                <i class="fa fa-plus"></i> Agregar
                            </button>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group"></div>
                                </div>
                            </div>
                            <div class="content-network">
                                @forelse($socialNetwork as $key => $value)
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group m-bot15">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-white dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        {!! $value->network->meta_option !!} {{ $value->network->meta_value }}
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        @foreach($networks as $network)
                                                            <li>
                                                                <a href="#" data-id="{{ $network->id }}">
                                                                    {!! $network->meta_option !!} {{ $network->meta_value }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    <input type="hidden" name="social_network_id[]"
                                                           value="{{ $value->meta_value }}">
                                                </div>
                                                <input type="text" name="social_network[]" class="form-control"
                                                       value="{{ $value->meta_option }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-left: 0">
                                            <button type="button" class="btn btn-danger delete-network">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </div>
                                @empty
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="input-group m-bot15">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-white dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        Seleccione <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        @foreach($networks as $network)
                                                            <li>
                                                                <a href="#" data-id="{{ $network->id }}">
                                                                    {!! $network->meta_option !!} {{ $network->meta_value }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    <input type="hidden" name="social_network_id[]">
                                                </div>
                                                <input type="text" name="social_network[]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-left: 0">
                                            <button type="button" class="btn btn-danger delete-network">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <header class="panel-heading" style="height: 55px"> Enlaces rápidos
                            <button type="button" class="btn btn-primary pull-right" id="add-url">
                                <i class="fa fa-plus"></i> Agregar
                            </button>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row insert">
                                        @forelse($quickLink as $key => $value)
                                            <div class="col-xs-12 display-flex justify-content-space-between m-bot15">
                                                <input type="text" class="form-control w30" name="url_text[]"
                                                       placeholder="Títlulo" value="{{ $value->meta_value }}">
                                                <textarea class="form-control w60" name="url_link[]"
                                                          placeholder="Detalle">{{ $value->meta_option }}</textarea>
                                                <a href="#" class="btn btn-danger remove" style="height: 34px">
                                                    <i class="fa fa-times"></i></a>
                                            </div>
                                        @empty
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
        Form.addNetwork('#add-network', '.content-network', '.row');
        Form.deleteNetwork('.delete-network', '.dropdown-toggle');
        Form.selectInputGroup('.input-group-btn');
    </script>
@endsection