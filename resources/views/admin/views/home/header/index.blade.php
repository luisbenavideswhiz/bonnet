@extends('admin.layout.base')

@section('bodyId')id="slider"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('admin.header.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Header</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <header class="panel-heading">Enlace a compras online
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:void(0);"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <span>Texto:</span>
                                                <input type="text" class="form-control" name="meta_option"
                                                       value="{{ $banner_top->meta_option }}">
                                            </div>
                                            <div class="form-group">
                                                <span>Link:</span>
                                                <input type="text" class="form-control" name="meta_link"
                                                       value="{{ $banner_top->meta_link }}">
                                            </div>
                                            <div class="form-group">
                                                <span>Destacar:
                                                    <input type="checkbox" name="meta_value" value="1"
                                                           @if($banner_top->meta_value != 0)checked
                                                           @endif style="margin-left: 5px">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <header class="panel-heading">Menú de navegación
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:void(0);"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <span>Nosotros:</span>
                                                <input type="text" class="form-control" name="us"
                                                       value="{{ isset($menu_us)?$menu_us->meta_value:"" }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <span>Productos:</span>
                                                <input type="text" class="form-control" name="product"
                                                       value="{{ isset($menu_product)?$menu_product->meta_value:"" }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <span>Servicios:</span>
                                                <input type="text" class="form-control" name="service"
                                                       value="{{ isset($menu_service)?$menu_service->meta_value:"" }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <span>Contacto:</span>
                                                <input type="text" class="form-control" name="contact"
                                                       value="{{ isset($menu_contact)?$menu_contact->meta_value:"" }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <span>Eventos:</span>
                                                <input type="text" class="form-control" name="event"
                                                       value="{{ isset($menu_event)?$menu_event->meta_value:"" }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <span>Blog:</span>
                                                <input type="text" class="form-control" name="blog"
                                                       value="{{ isset($menu_blog)?$menu_blog->meta_value:"" }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('admin.header') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection