@extends('admin.layout.base')

@section('bodyId')id="alliance"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Home</header>
                </section>
            </div>
        </div>

        <div class="row wa_home">
            <div class="col-sm-12">
                <div class="panel">
                    <header class="panel-heading">Aliados</header>
                    <div class="panel-body">
                        <p>Las imágenes deben de medir 800px x 200px</p>
                        @component('admin.partials.upload-gallery-alliance', [
                            'gallery'=> $gallery,'type'=>'ALLIANCE','is_image'=> true])
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script id="template-upload" type="text/x-tmpl">@include('admin.templates.multiple-upload-alliance')</script>
    <script id="template-download" type="text/x-tmpl">@include('admin.templates.multiple-display-alliance')</script>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    {{--Vendor to gallery--}}
    <script src="{{ asset('js/vendor/file-uploader/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/tmpl.min.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/load-image.all.min.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/canvas-to-blob.min.js') }}"></script>

    <script src="{{ asset('js/vendor/file-uploader/jquery.blueimp-gallery.min.js') }}"></script>

    <script src="{{ asset('js/vendor/file-uploader/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-ui.js') }}"></script>
@endsection