@extends('admin.layout.base')

@section('bodyId')id="us"@endsection

@section('content')
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <header class="panel-heading">General - Nuestras Líneas</header>
                </div>
            </div>
        </div>

        <form action="{{ route('admin.descriptive.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <textarea name="description" class="ckeditor form-control"
                                                >{{ isset($descriptive)? $descriptive->meta_option : ''}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="show_link">Mostrar Boton</label>
                                                <input type="checkbox" name="show_link" id="show_link" class="form-control" value="1" @if($show_link->meta_option == 1) checked @endif>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="name_button">Nombre del boton</label>
                                                <input type="text" name="name_button" id="name_button" class="form-control" value="{{ isset($name_button)? $name_button->meta_option : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="link">Link del boton</label>
                                                <input type="text" name="link" id="link" class="form-control" value="{{ isset($link)? $link->meta_option : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('admin.alliance') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection