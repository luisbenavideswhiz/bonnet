<div class="modal fade modal-dialog-center" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar Especialización</h4>
                </div>
                <form method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="name_u">Nombre :</label>
                            <input type="text" class="form-control" id="name_u" name="name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" type="button">Cerrar</a>
                        <button class="btn btn-warning"> Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>