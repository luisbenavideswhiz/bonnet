@extends('admin.layout.base')

@section('bodyId')id="specialization"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Especializaciones
                        <a href="#create" class="pull-right btn btn-success" data-toggle="modal">Nuevo
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 60px">Orden</th>
                                    <th>Nombre</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    @include('admin.views.specialization.partials.modals.create')
    @include('admin.views.specialization.partials.modals.update')

    @component('admin.partials.modals.delete')
        @slot('title')Eliminar especialización @endslot
        @slot('message')Seguro de eliminar la especialización : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection