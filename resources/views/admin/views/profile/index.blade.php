@extends('admin.layout.base')

@section('bodyId')id="profile"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Perfil</header>
                </section>
            </div>
        </div>
        <div class="row wa_home">
            <div class="col-sm-3">
                <div class="panel">
                    <header class="panel-heading">Imagen</header>
                    <div class="panel-body">
                        <div class="profile-img">
                            <img src="{{ url(isset($profile->image)? $profile->image->path: 'img/admin/user.png') }}"
                                 alt="" width="100%">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="panel">
                    <header class="panel-heading">Datos Personales</header>
                    <div class="panel-body">
                        <form action="{{ route('profile.store') }}" class="profile-form" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $profile->id }}">
                            <div class="form-group">
                                <label class="col-lg-2">Nombres:</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="name"
                                           value="{{ $profile->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Apellidos:</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{ $profile->last_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Email:</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"
                                           value="{{ $profile->email }}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Imagen:</label>
                                <div class="col-lg-6">
                                    <input type="file" class="form-control" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6 col-lg-offset-2 mtop20">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('admin.alliance') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-sm-9">
                <div class="panel">
                    <header class="panel-heading">Cambiar Contraseña
                        <span class="tools pull-right"><a href="javascript:;" class="fa fa-chevron-down"></a></span>
                    </header>
                    <div class="panel-body">
                        <form action="{{ route('profile.update', ['id'=> $profile->id]) }}" class="profile-form"
                              method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label class="col-lg-2">Contraseña actual:</label>
                                <div class="col-lg-6">
                                    <input type="password" class="form-control" name="old_password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Nueva Contraseña:</label>
                                <div class="col-lg-6">
                                    <input type="password" class="form-control" name="new_password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Confirmar Contraseña:</label>
                                <div class="col-lg-6">
                                    <input type="password" class="form-control" name="confirm_password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6 col-lg-offset-2 mtop20">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('admin.alliance') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
    </script>
@endsection