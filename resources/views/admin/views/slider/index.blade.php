@extends('admin.layout.base')

@section('bodyId')id="slider"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-heading">Banners
                        <a href="{{ route('slider.create') }}" class="pull-right btn btn-success">Nuevo
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-heading">Texto General
                        <span class="tools pull-right"><a href="javascript:;" class="fa fa-chevron-down"></a></span>
                    </div>
                    <div class="panel-body">
                        <form action="{{ route('slider.text.general') }}" method="POST">
                            <div class="col-lg-10">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" class="form-control" name="meta_value"
                                           value="{{ is_null($general_text)? '': $general_text->meta_value }}">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="submit" class="btn btn-success pull-right"> Actualizar</button>
                            </div>
                        </form>
                    </div>
                </section>
                <div class="panel">
                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 60px">Order</th>
                                    <th>Nombre</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    @component('admin.partials.modals.delete')
        @slot('title')Eliminar slider @endslot
        @slot('message')Seguro de eliminar el slider : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection