@extends('admin.layout.base')

@section('bodyId')id="product"@endsection

@section('content')
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <header class="panel-heading">Definir detalle de: <span class="bold">{{ $product->name }}</span><br>
                        <span style="font-size: 13px">Has click en guardar para poder agregar un nuevo detalle.</span>
                    </header>
                </div>
            </div>
        </div>
        <form action="{{ url('/admin/product/detail') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $product->id }}">
            <input type="hidden" id="product_detail_id" name="product_detail_id">
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-9">
                                            <div class="form-group">
                                                <label class="m-bot15">Nombre de detalle</label>
                                                <input type="text" class="form-control m-bot15" id="title" name="title">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mtop20">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="m-bot15">Descripción</label>
                                                <textarea class="ckeditor form-control" id="description" name="description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('product.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Archivo PDF</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="controls col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload" id="pdf">
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new">
                                                <i class="fa fa-paper-clip"></i> Seleccione PDF</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Camabiar</span>
                                            <input type="file" class="default" name="pdf" accept="application/pdf"/>
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                                           style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        @component('admin.partials.upload-image')
                                            @slot('title')Las imágenes deben de medir 270x270 px @endslot
                                            @slot('inputName')image @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <div class="panel">
            <header class="panel-heading">Características
                <span class="tools pull-right"><a href="javascript:;" class="fa fa-chevron-down"></a></span>
            </header>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th class="text-center" style="width: 20%">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($features as $key => $value)
                        <tr>
                            <td>{{ $value->title }}</td>
                            <td class="acciones text-center">
                                <a href="#" data-url="{{ route('detail.show', ['id'=> $value->id]) }}"
                                   class="btn btn-warning edit-detail"><i class="fa fa-pencil-square-o"></i>
                                </a>
                                <a href="#delete-feature" data-toggle="modal" class="btn btn-danger"
                                   data-url="{{ url('/admin/product/detail/'.$value->id) }}">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @include('admin.partials.modals.cropper_image')

    @component('admin.partials.modals.delete')
        @slot('title')Eliminar característica @endslot
        @slot('message')Seguro de eliminar : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper', '.modal-btn-confirm', '#input_image', 270, 270);
    </script>
@endsection