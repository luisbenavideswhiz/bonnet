@extends('admin.layout.base')

@section('bodyId')id="product"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ url('/admin/product') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Nuevo Producto</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input class="form-control m-bot15" type="text" name="name">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Tipo de producto</label>
                                                <select name="type_id" class="form-control">
                                                    <option value="">Seleccionar</option>
                                                    @foreach($productTypes as $key => $value)
                                                        <option value="{{ $value->id }}">{{ $value->meta_value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Destacar producto</label>
                                                <select class="form-control m-bot15" name="status">
                                                    <option value="">Seleccionar</option>
                                                    <option value="0">Borrador</option>
                                                    <option value="1">Publicado</option>
                                                    <option value="2">Destacado</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Marca</label>
                                                <select class="form-control m-bot15" name="brand_id">
                                                    <option value="">Seleccionar</option>
                                                    @foreach($brands as $key => $value)
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Categoría</label>
                                                <select name="category_id" class="form-control">
                                                    <option value="">Seleccionar</option>
                                                    @foreach($categories as $key => $value)
                                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Modelo</label>
                                                <input type="text" class="form-control" name="model">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">Descripción</label>
                                                <textarea name="description" class="ckeditor form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">Etiquetas (Separados por enter)</label>
                                                <input type="text" class="form-control m-bot15 tagsinput" name="tag">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ url('/admin/product') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>SKU</label>
                                        <input class="form-control m-bot15" type="text" name="sku">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Stock</label>
                                        <input type="text" class="form-control" name="stock">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen principal</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        @component('admin.partials.upload-image')
                                            @slot('title')Las imágenes deben de medir 540x560 px @endslot
                                            @slot('inputName')image @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('admin.partials.message')

                    <div class="panel">
                        <header class="panel-heading">Archivo PDF</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="controls col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new">
                                                <i class="fa fa-paper-clip"></i> Seleccione PDF</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Camabiar</span>
                                            <input type="file" class="default" name="pdf" accept="application/pdf"/>
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                                           style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    @include('admin.partials.modals.cropper_image')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper', '.modal-btn-confirm', '#input_image', 540, 560);
    </script>
@endsection