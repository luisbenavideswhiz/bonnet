@extends('admin.layout.base')

@section('bodyId')id="product-type"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Tipos de producto
                        <a href="#create" class="pull-right btn btn-success" data-toggle="modal">
                            Nuevo <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 60px">Orden</th>
                                    <th>Nombre</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    @include('admin.views.product.type.partials.modals.create')
    @include('admin.views.product.type.partials.modals.update')

    @component('admin.partials.modals.delete')
        @slot('title')Eliminar tipo de producto @endslot
        @slot('message')Seguro de eliminar la tipo de producto : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection