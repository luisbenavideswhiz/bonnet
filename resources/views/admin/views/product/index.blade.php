@extends('admin.layout.base')

@section('bodyId')id="product"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Productos
                        <a href="{{ url('/admin/product/create') }}" class="pull-right btn btn-success">Nuevo
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered" id="product-table">
                                <thead>
                                <tr>
                                    <th style="width: 60px">Order</th>
                                    <th>Nombre</th>
                                    <th>Categoría</th>
                                    <th>Marca</th>
                                    <th>Estado</th>
                                    <th>Stock</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@foreach($products as $key => $value)--}}
                                {{--<tr>--}}
                                {{--<td>{{ $value->name }}</td>--}}
                                {{--<td>{{ $value->brand->name }}</td>--}}
                                {{--<td>{{ $value->status_name }}</td>--}}
                                {{--<td>{{ $value->stock }}</td>--}}
                                {{--<td class="acciones text-center">--}}
                                {{--<a href="{{ url('/admin/product/'.$value->id.'/edit') }}"--}}
                                {{--class="btn btn-warning"><i class="fa fa-pencil-square-o"></i>--}}
                                {{--</a>--}}
                                {{--<a href="{{ url('/admin/product/detail/'.$value->id.'/edit') }}"--}}
                                {{--class="btn btn-info"><i class="fa fa-cog"></i>--}}
                                {{--</a>--}}
                                {{--<a href="#delete" data-toggle="modal" class="btn btn-danger"--}}
                                {{--data-url="{{ url('/admin/product/'.$value->id) }}">--}}
                                {{--<i class="fa fa-trash-o"></i>--}}
                                {{--</a>--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--@endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

    @component('admin.partials.modals.delete')
        @slot('title')
            Eliminar producto
        @endslot
        @slot('message')
            Seguro de eliminar el producto :
        @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection