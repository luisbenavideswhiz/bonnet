@extends('admin.layout.base')

@section('bodyId')id="product"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('product.update', ['id'=>$product->id]) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar Producto</header>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input class="form-control m-bot15" type="text" name="name"
                                                       value="{{ $product->name }}">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Tipo de producto</label>
                                                <select name="type_id" class="form-control">
                                                    <option value="">Seleccionar</option>
                                                    @foreach($productTypes as $key => $value)
                                                        <option value="{{ $value->id }}"
                                                                @if($product->type_id == $value->id)
                                                                selected='selected' @endif>
                                                            {{ $value->meta_value }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Destacar producto</label>
                                                <select class="form-control m-bot15" id="status" name="status">
                                                    <option value="">Seleccionar</option>
                                                    <option value="0"
                                                            @if($product->status == 0) selected="selected" @endif>
                                                        Borrador
                                                    </option>
                                                    <option value="1"
                                                            @if($product->status == 1) selected="selected" @endif>
                                                        Publicado
                                                    </option>
                                                    <option value="2"
                                                            @if($product->status == 2) selected="selected" @endif>
                                                        Destacado
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Marca</label>
                                                <select class="form-control m-bot15" id="brand_id" name="brand_id">
                                                    <option value="">Seleccionar</option>
                                                    @foreach($brands as $key => $value)
                                                        <option value="{{ $value->id }}"
                                                                @if($product->brand_id == $value->id)
                                                                selected='selected' @endif>{{ $value->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Categoría</label>
                                                <select name="category_id" class="form-control">
                                                    <option value="">Seleccionar</option>
                                                    @foreach($categories as $key => $value)
                                                        <option value="{{ $value->id }}"
                                                                @if($product->category_id == $value->id)
                                                                selected='selected' @endif>
                                                            {{ $value->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <div class="form-group">
                                                <label>Modelo</label>
                                                <input type="text" class="form-control" name="model"
                                                       value="{{ $product->model }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">Descripción</label>
                                                <textarea id="description" name="description"
                                                          class="ckeditor form-control">{{ $product->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">Etiquetas (Separados por enter)</label>
                                                <input type="text" class="form-control m-bot15 tagsinput" name="tag"
                                                       value="{{ $product->tag }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ url('/admin/product') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>SKU</label>
                                        <input class="form-control m-bot15" type="text" name="sku"
                                               value="{{ $product->sku }}">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Stock</label>
                                        <input type="text" class="form-control" name="stock"
                                               value="{{ $product->stock }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen principal</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        @component('admin.partials.upload-image', ['path'=> isset($product->image) ? $product->image->path : ''])
                                            @slot('title')Las imágenes deben de medir 540x560 px @endslot
                                            @slot('inputName') image @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('admin.partials.message')

                    <div class="panel">
                        <header class="panel-heading">Archivo PDF</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="controls col-md-9">
                                    @if(!is_null($product->pdf))
                                        <img src="{{ asset('img/admin/pdf.png') }}" width="100" alt="">
                                        <a href="#" data-url="/admin/gallery/product/{{ $product->pdf->id }}"
                                        class="btn btn-danger delete-pdf"><i class="fa fa-times"></i></a>
                                        <p>{{ $product->pdf->name }}</p>
                                    @endif
                                </div>
                                <div class="controls col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new">
                                                <i class="fa fa-paper-clip"></i> Seleccione PDF</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Camabiar</span>
                                            <input type="file" class="default" name="pdf" accept="application/pdf"/>
                                        </span>
                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                                           style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-8">
                <div class="panel">
                    <header class="panel-heading">Galeria
                        <span class="tools pull-right"><a href="javascript:;" class="fa fa-chevron-down"></a></span>
                    </header>
                    <div class="panel-body">
                        @component('admin.partials.upload-gallery',[
                            'gallery'=> $product->gallery,'type'=>'PRODUCT','is_image' => true
                        ])
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script id="template-upload" type="text/x-tmpl">@include('admin.templates.multiple-upload')</script>
    <script id="template-download" type="text/x-tmpl">@include('admin.templates.multiple-display')</script>
    @include('admin.partials.modals.cropper_image')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper', '.modal-btn-confirm', '#input_image', 540, 560);
    </script>

    {{--Vendor to gallery--}}
    <script src="{{ asset('js/vendor/file-uploader/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/tmpl.min.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/load-image.all.min.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/canvas-to-blob.min.js') }}"></script>

    <script src="{{ asset('js/vendor/file-uploader/jquery.blueimp-gallery.min.js') }}"></script>

    <script src="{{ asset('js/vendor/file-uploader/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('js/vendor/file-uploader/jquery.fileupload-ui.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').prop('content')
                }
            });

            $('#fileupload').fileupload({
                url: '/admin/gallery/product',
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                formData: {
                    id: {{ $product->id }}
                }
            });

            $('#pdfupload').fileupload({
                url: '/admin/pdf/product',
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                maxFileSize: 32000000,
                acceptFileTypes: /(\.|\/)(pdf)$/i,
                formData: {
                    id: {{ $product->id }}
                }
            });
        });
    </script>
@endsection