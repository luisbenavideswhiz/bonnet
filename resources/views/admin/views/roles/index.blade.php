@extends('admin.layout.base')

@section('bodyId')id="role"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Roles
                        <a href="{{ route('role.create') }}" class="pull-right btn btn-success">Nuevo Rol
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped data-table">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Título</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $key => $value)
                                    <tr>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->title }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('role.edit', ['id'=> $value->id]) }}"
                                               class="btn btn-warning"><i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <a href="#delete" data-toggle="modal" class="btn btn-danger"
                                               data-url="{{ route('role.show', ['id'=> $value->id]) }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

    @component('admin.partials.modals.delete')
        @slot('title') Eliminar Rol @endslot
        @slot('message') Seguro de eliminar el rol de : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>Form.form('form');</script>
@endsection