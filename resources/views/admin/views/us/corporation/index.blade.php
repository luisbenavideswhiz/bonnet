@extends('admin.layout.base')

@section('bodyId')id="corporation"@endsection

@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">Corporaciones
                        <a href="{{ route('corporation.create') }}" class="pull-right btn btn-success">Nuevo
                            <i class="fa fa-plus"></i>
                        </a>
                    </header>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 60px">Orden</th>
                                    <th>Título</th>
                                    <th class="text-center" style="width: 20%">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

    @component('admin.partials.modals.delete')
        @slot('title')Eliminar corporación @endslot
        @slot('message')Seguro de eliminar la corporación : @endslot
    @endcomponent

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection