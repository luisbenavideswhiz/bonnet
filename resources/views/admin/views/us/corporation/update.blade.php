@extends('admin.layout.base')

@section('bodyId')id="slider"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ route('corporation.update', ['id'=> $corporation->id]) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Editar Banner</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input class="form-control m-bot15" type="text" name="title"
                                                       value="{{ $corporation->title }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="">Descripción</label>
                                                <textarea name="description"
                                                          class="ckeditor form-control">{{ $corporation->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('corporation.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <header class="panel-heading">Imagen principal</header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        @component('admin.partials.upload-image', ['path'=> $corporation->image->path])
                                            @slot('title')Las imágenes deben de medir 170x56.17 px @endslot
                                            @slot('inputName') image @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    @include('admin.partials.modals.cropper_image')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery-cropper.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form', true);
        Form.imageEditor('#cropper_modal', '.fileupload', '#image_cropper', '.modal-btn-confirm', '#input_image', 170, 56.17);
    </script>
@endsection