@extends('admin.layout.base')

@section('bodyId')id="us"@endsection

@section('content')
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <header class="panel-heading">Nueva Sede</header>
                </div>
            </div>
        </div>

        <div class="row">
            <form action="{{ route('headquarter.store') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="location" id="location">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input type="text" class="form-control m-bot15" name="name">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label>Telefono</label>
                                                <input type="text" class="form-control m-bot15" name="phone">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Correo</label>
                                                <input type="text" class="form-control m-bot15" name="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Dirección</label>
                                                <input type="text" class="form-control m-bot15" name="address">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('headquarter.index') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-sm-6">
                <div class="panel">
                    <header class="panel-heading">Mapa</header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <section class="panel">
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <input type="text" id="gmap_geocoding_address" class=" form-control"
                                                   placeholder="Dirección..."/>
                                        </div>
                                    </form>
                                    <br>
                                    <div id="gmap_geocoding" class="gmaps"></div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('js')
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCt3-aLL5MF1iaRTiFXgz8CeomI9xCeJcE"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/googlemaps-script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection