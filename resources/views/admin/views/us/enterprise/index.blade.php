@extends('admin.layout.base')

@section('bodyId')id="us"@endsection

@section('content')
    <section class="wrapper">
        <form action="{{ url('/admin/us/enterprise') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <header class="panel-heading">Nosotros - Resumen de la Empresa</header>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <textarea name="description" class="ckeditor form-control"
                                                >{{ isset($enterprise)? $enterprise->description : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="{{ route('admin.alliance') }}"
                                               class="btn btn-danger btn-block">Cancelar</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-success btn-block">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
    <script>
        Form.form('form');
    </script>
@endsection