<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.assets.metas')
    @include('admin.assets.css')
    @include('admin.assets.ie9')
    @section('css')
    @show
</head>

<body @section('bodyId')@show>

<section id="container">
    @include('admin.partials.header')
    @include('admin.partials.aside')
    <section id="main-content">
        @yield('content')
    </section>
    @include('admin.partials.footer')
</section>

@include('admin.assets.js')

@section('js')
@show
<script>
    if (typeof CKEDITOR != 'undefined') {
        CKEDITOR.tools.getCsrfToken = function () {
            return $('meta[name="csrf-token"]').attr('content');
        };
    }
</script>
</body>
</html>