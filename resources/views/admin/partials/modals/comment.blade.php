<div class="modal fade modal-dialog-center in" id="comments" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Datos del mensaje</h4>
                </div>
                <div class="modal-body">{{ $data }}</div>
                <div class="modal-footer">
                    <a data-dismiss="modal" class="btn btn-default" style="width: 30%">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
</div>