<div class="modal fade modal-dialog-center in" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">{{ $title }}</h4>
                </div>
                <form action="" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="modal-body">
                        <label>{{ $message }} <span id="message" class="bold"></span></label>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default" style="width: 30%">Cerrar</a>
                        <button class="btn btn-warning"> Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>