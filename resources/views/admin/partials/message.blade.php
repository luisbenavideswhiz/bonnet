<div class="panel" style="background-color: #002347; color: #fff">
    <header class="panel-body">
        Para poder cargar la galería de fotos, primero debes asignar una imagen de portada.
        Recuerda editar la imagen de portada a la medida adecuada haciendo clic en "editar".
    </header>
</div>