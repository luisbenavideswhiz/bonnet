<form action="" id="{{ empty($id)?'fileupload':$id }}">
    <div class="row">
        <div class="col-md-12">
            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <div class="row fileupload-buttonbar">
                <div class="col-lg-12">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Agregar archivos...</span>
                        <input type="file" name="gallery[]" multiple>
                    </span>
                    <button type="submit" class="btn btn-primary start">
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Subir</span>
                    </button>
                    <button type="reset" class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancelar</span>
                    </button>
                    <button type="button" class="btn btn-danger delete">
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Eliminar</span>
                    </button>
                    <input type="checkbox" class="toggle">
                    <!-- The global file processing state -->
                    <span class="fileupload-process"></span>
                </div>
                <!-- The global progress state -->
                <div class="col-lg-12 fileupload-progress fade">
                    <!-- The global progress bar -->
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                         aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                    <!-- The extended global progress state -->
                    <div class="progress-extended">&nbsp</div>
                </div>
            </div>
            <!-- The table listing the files available for upload/download -->
            <table role="presentation" class="table table-striped">
                <tbody class="files">
                @foreach($gallery as $file)
                    @if(file_exists($file->path))
                        <tr class="template-download fade in">
                            <td>
                                <span class="preview">
                                    @if ($file->thumbnail && $is_image)
                                        <a href="{{ url($file->path) }}" title="" download="" data-gallery>
                                            <img src="{{ $file->thumbnail }}"></a>
                                    @endif
                                </span>
                            </td>
                            <td style="vertical-align: middle">
                                <p class="name">
                                    <a href="{{ url($file->path) }}" title="{{ $file->name }}"
                                       download="{{ $file->name }}" data-gallery> {{ $file->name }}</a>
                                </p>
                            </td>
                            <td style="vertical-align: middle"><span class="size">{{ $file->size }}</span></td>
                            <td style="vertical-align: middle">
                                <button class="btn btn-danger delete" data-type="DELETE"
                                        data-url="{{ url('/admin/gallery/'.$type.'/'.$file->id) }}">
                                    <i class="glyphicon glyphicon-trash"></i><span>Eliminar</span>
                                </button>
                                <input type="checkbox" name="delete" value="1" class="toggle">
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</form>