<!--header start-->
<header class="header white-bg">
    <div class="sidebar-toggle-box">
        <i class="fa fa-bars"></i>
    </div>
    <!--logo start-->
    {{--<a href="/admin" class="logo">Bon<span>nett</span></a>--}}
    <a href="/admin" class="logo"><img src="{{ asset('img/admin/logo.png') }}" alt="" height="48"></a>
    <!--logo end-->
    <div class="top-nav ">
        <!--search & user info start-->
        <ul class="nav pull-right top-menu">
        {{--<li>--}}
        {{--<input type="text" class="form-control search" placeholder="Buscar">--}}
        {{--</li>--}}
        <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <img alt="" src="{{ asset(isset(Auth::user()->image)? Auth::user()->image->path: 'img/admin/user.png') }}"
                         width="29px" height="29px"
                         style="padding: 2px">
                    <span class="username">{{ Auth::user()->name }}</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended logout" style="width: 160px!important;">
                    <div class="log-arrow-up"></div>
                    <li>
                        <a href="{{ route('profile.index') }}" class="btn-info"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}" class="btn-warning"><i class="fa fa-key"></i> Log Out</a>
                    </li>
                </ul>
            </li>
            <!-- user login dropdown end -->
        </ul>
        <!--search & user info end-->
    </div>
</header>
<!--header end-->