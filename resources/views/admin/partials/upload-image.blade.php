<span id="{{ $inputName }}" class="popover-validate">{{ $title }}</span>
<div class="fileupload fileupload-new" data-provides="fileupload">
    <div class="fileupload-new thumbnail" style="width: 100%; height: 150px;">
        @if(isset($path))
            <img src="{{ url($path) }}" alt=""/>
        @else
            {{--<img src="{{ asset ('img/team/default.png') }}" alt="" style="width: 150px; height: 140px;"/>--}}
        @endif
    </div>
    <div class="fileupload-preview fileupload-exists thumbnail"
         style="width: 100%; max-height: 150px; line-height: 20px;">
    </div>
    <div class="text-center">
        <span class="btn btn-white btn-file">
            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Seleccionar imagen</span>
            <span class="fileupload-exists"><i class="fa fa-undo"></i> Cambiar imagen</span>
            <input type="file" class="default"/>
        </span>
        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
            <i class="fa fa-trash"></i> Eliminar
        </a>
        <button type="button" id="cropper_up" class="btn btn-warning fileupload-exists" data-toggle="modal"
                data-target="#cropper_modal" style="margin-top: 3px"><i class="fa fa-edit"></i> Editar
        </button>
    </div>
</div>