<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            @can('manage_general')
                <li class="sub-menu">
                    <a href="javascript:void(0)"
                       class="{{ Route::is('admin.*', 'slider.*','certificate.*', 'second-slider.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Generales</span>
                    </a>
                    <ul class="sub">
                        <li class="{{ Route::is('admin.alliance') ? 'active' : '' }}">
                            <a href="{{ route('admin.alliance') }}">Aliados</a>
                        </li>
                        <li class="{{ Route::is('admin.header') ? 'active' : '' }}">
                            <a href="{{ route('admin.header') }}">Header</a>
                        </li>
                        <li class="{{ Route::is('slider.*') ? 'active' : '' }}">
                            <a href="{{ route('slider.index') }}">Banner Principal</a>
                        </li>
                        <li class="{{ Route::is('second-slider.*') ? 'active' : '' }}">
                            <a href="{{ route('second-slider.index') }}">Banner Secundario</a>
                        </li>
                        <li class="{{ Route::is('admin.descriptive') ? 'active' : '' }}">
                            <a href="{{ route('admin.descriptive') }}">Nuestras Líneas</a>
                        </li>
                        <li class="{{ Route::is('certificate.*') ? 'active' : '' }}">
                            <a href="{{ route('certificate.index') }}">Certificados</a>
                        </li>
                        <li class="{{ Route::is('admin.footer') ? 'active' : '' }}">
                            <a href="{{ route('admin.footer') }}">Footer</a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('manage_us')
                <li class="sub-menu">
                    <a href="javascript:void(0)"
                       class="{{ Route::is('us.*', 'headquarter.*', 'corporation.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Nosotros</span>
                    </a>
                    <ul class="sub">
                        <li class="{{ Route::is('us.enterprise') ? 'active' : '' }}">
                            <a href="{{ route('us.enterprise') }}">Empresa</a>
                        </li>
                        <li class="{{ Route::is('us.mission') ? 'active' : '' }}">
                            <a href="{{ route('us.mission') }}">Misión</a>
                        </li>
                        <li class="{{ Route::is('us.vision') ? 'active' : '' }}">
                            <a href="{{ route('us.vision') }}">Visión</a>
                        </li>
                        <li class="{{ Route::is('us.holding') ? 'active' : '' }}">
                            <a href="{{ route('us.holding') }}">Valores</a>
                        </li>
                        <li class="{{ Route::is('us.policy') ? 'active' : '' }}">
                            <a href="{{ route('us.policy') }}">Politicas</a>
                        </li>
                        <li class="{{ Route::is('us.history') ? 'active' : '' }}">
                            <a href="{{ route('us.history') }}">Historia</a>
                        </li>
                        <li class="{{ Route::is('headquarter.*') ? 'active' : '' }}">
                            <a href="{{ route('headquarter.index') }}">Sedes</a>
                        </li>
                        <li class="{{ Route::is('corporation.*') ? 'active' : '' }}">
                            <a href="{{ route('corporation.index') }}">Corporaciones</a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('manage_category')
                <li>
                    <a href="{{ route('category.index') }}" class="{{ Route::is('category.index') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Categorias</span>
                    </a>
                </li>
            @endcan
            @can('manage_product')
                <li class="sub-menu">
                    <a href="javascript:void(0)"
                       class="{{ Route::is('product.*', 'brand.*', 'type.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Productos</span>
                    </a>
                    <ul class="sub">
                        <li class="{{ Route::is('product.*') ? 'active' : '' }}">
                            <a href="{{ route('product.index') }}">Listado</a>
                        </li>
                        <li class="{{ Route::is('brand.*') ? 'active' : '' }}">
                            <a href="{{ route('brand.index') }}">Marcas</a>
                        </li>
                        <li class="{{ Route::is('type.*') ? 'active' : '' }}">
                            <a href="{{ route('type.index') }}">Tipos</a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('manage_service')
                <li class="sub-menu">
                    <a href="javascript:void(0)" class="{{ Route::is('service.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Servicios</span>
                    </a>
                    <ul class="sub">
                        <li class="{{ Route::is('service.index', 'service.create', 'service.edit') ? 'active' : '' }}">
                            <a href="{{ route('service.index') }}">Listado</a>
                        </li>
                        <li class="{{ Route::is('service.banner') ? 'active' : '' }}">
                            <a href="{{ route('service.banner') }}">Encabezado</a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('manage_event')
                <li class="sub-menu">
                    <a href="javascript:void(0)" class="{{ Route::is('event.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Eventos</span>
                    </a>
                    <ul class="sub">
                        <li class="{{ Route::is('event.index', 'event.create', 'event.edit') ? 'active' : '' }}">
                            <a href="{{ route('event.index') }}">Listado</a>
                        </li>
                        <li class="{{ Route::is('event.banner') ? 'active' : '' }}">
                            <a href="{{ route('event.banner') }}">Encabezado</a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('manage_specialization')
                <li>
                    <a href="{{ route('specialization.index') }}"
                       class="{{ Route::is('specialization.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Nivel de Usuarios</span>
                    </a>
                </li>
            @endcan
            @can('manage_how_contact')
                <li>
                    <a href="{{ route('how-contact.index') }}" class="{{ Route::is('how-contact.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Medios de contacto</span>
                    </a>
                </li>
            @endcan
            @can('manage_message')
                <li>
                    <a href="{{ route('contact.index') }}" class="{{ Route::is('contact.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Mensajes</span>
                    </a>
                </li>
            @endcan
            @can('manage_users')
                <li class="sub-menu">
                    <a href="javascript:void(0)" class="{{ Route::is('user.*', 'role.*') ? 'active' : '' }}">
                        <i class="fa fa-caret-right"></i><span>Usuarios</span>
                    </a>
                    <ul class="sub">
                        <li class="{{ Route::is('user.*') ? 'active' : '' }}">
                            <a href="{{ route('user.index') }}">Listado</a>
                        </li>
                        <li class="{{ Route::is('role.*') ? 'active' : '' }}">
                            <a href="{{ route('role.index') }}">Roles</a>
                        </li>
                    </ul>
                </li>
            @endcan
            <li>
                <a href="{{ route('post.index') }}" class="{{ Route::is('post.*') ? 'active' : '' }}">
                    <i class="fa fa-caret-right"></i><span>Blog</span>
                </a>
            </li>
            <li>
                <a href="{{ route('newsletter.index') }}" class="{{ Route::is('newsletter.*') ? 'active' : '' }}">
                    <i class="fa fa-caret-right"></i><span>Newsletter</span>
                </a>
            </li>
            <li>
                <a href="{{ route('seo.index') }}" class="{{ Route::is('seo.*') ? 'active' : '' }}">
                    <i class="fa fa-caret-right"></i><span>SEO</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->