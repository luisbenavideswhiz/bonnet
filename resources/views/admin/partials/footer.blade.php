<!--footer start-->
<footer class="site-footer">
    <div class="text-center">
        {{ date('Y') }} &copy; GRUPO BONNETT Powered by WHIZ
        <a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>
    </div>
</footer>
<!--footer end-->