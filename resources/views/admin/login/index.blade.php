<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
    <title>Grupo Bonnett | Admin</title>
    @include('admin.assets.css')
</head>
<body id="login" class="login-body">
<div class="no-container">
    <div id="customer-chat-widget" class="customer-chat customer-chat-login customer-chat-widget login-form">
        <img class="logo" src="{{ asset('img/logo.svg') }}" width="260">
        <div class="customer-chat-header">
            <div class="customer-chat-header-title">Grupo Bonnett | Administrador</div>
        </div>

        <div id="customer-chat-content-login-form" class="customer-chat-content">
            <div class="customer-chat-content-info">Por favor inicie sesión usando el siguiente formulario</div>

            <form action="{{route('login.verify')}}" method="POST">
                {{ csrf_field() }}
                <div class="customer-chat-content-message-input">
                    <input type="text" name="email" class="customer-chat-content-message-input-field"
                           placeholder="Email"/>
                </div>
                <div class="customer-chat-content-message-input">
                    <input type="password" name="password" class="customer-chat-content-message-input-field"
                           placeholder="Contraseña"/>
                </div>
                <div class="customer-chat-content-row">
                    <button type="submit" id="customer-chat-login-start" class="customer-chat-content-button">
                        Iniciar sesión <i class="fa fa-arrow-circle-o-right" style="margin: 3px 0 0 3px;"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@include('admin.assets.js')
<script type="text/javascript" src="{{ asset('js/vendor/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/scripts/form.js') }}"></script>
<script>
    Form.form('form');
</script>
</body>
</html>