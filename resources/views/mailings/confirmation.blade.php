<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <style type="text/css">
        div, p, a, li, td {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            min-width: 100% !important;
            margin: 0;
            padding: 0;
        }

        .hidesumary {
            display: none;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .bgtd {
            background: #ffffff;
        }

        .ExternalClass * {
            line-height: 110%;
        }

        img {
            display: block;
            line-height: 100%;
            border: 0;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        table {
            border-collapse: collapse !important;
            border-spacing: 0;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        table td {
            border-collapse: collapse;
        }
    </style>
</head>
<body bgcolor="#F2F2F2" marginleft="0" marginright="0" margintop="0"
      style="width:100% !important; Margin:0; padding:0;">
<table style="max-width:600px;min-width:320px" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"
       align="center">
    <tr>
        <td>
            <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td>
                        <img src="{{ asset('img/mailings/header.png') }}" width="100%" alt="" style="display: block;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="86%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td height="60"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 16px; color: #002347;"
                                    align="center"><strong>TUS DATOS FUERON ENVIADOS</strong></td>
                            </tr>
                            <tr>
                                <td height="44"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 16px; color: #002347;"
                                    align="center">Hola, <strong>{{ $contact->name }}:</strong></td>
                            </tr>
                            <tr>
                                <td height="16"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347;"
                                    align="center">Gracias por elegirnos, en seguida te enviaremos la cotización.
                                </td>
                            </tr>
                            <tr>
                                <td height="62"></td>
                            </tr>
                            <tr>
                                <td height="62" align="center">
                                    <a href="https://grupobonnett.com/productos"><img src="{{ asset('img/mailings/button.png') }}" alt=""
                                                     style="display: block;max-width: 100%;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td height="92"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="86%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; color: #FF4141; font-size: 10px;"
                                    align="center"><a href="#" style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Nuestra web</font></a> | <a href="#"
                                                                                            style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Productos</font></a> | <a href="#"
                                                                                          style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Servicios</font></a> | <a href="#"
                                                                                          style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Eventos</font></a></td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; color: #002347;"
                                    align="center">© {{ \Carbon\Carbon::now()->format('Y') }} Grupo Bonnett.<br>All rights reserved.
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 10px;" align="center"><a
                                            href="#" style="text-decoration: none"><font color="#FF4141">Términos y
                                            condiciones</font></a></td>
                            </tr>
                            <tr>
                                <td height="100"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
