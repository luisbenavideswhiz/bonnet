<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <style type="text/css">
        div, p, a, li, td {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            min-width: 100% !important;
            margin: 0;
            padding: 0;
        }

        .hidesumary {
            display: none;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .bgtd {
            background: #ffffff;
        }

        .ExternalClass * {
            line-height: 110%;
        }

        img {
            display: block;
            line-height: 100%;
            border: 0;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        table {
            border-collapse: collapse !important;
            border-spacing: 0;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        table td {
            border-collapse: collapse;
        }
    </style>
</head>
<body yahoo bgcolor="#F2F2F2" marginleft="0" marginright="0" margintop="0"
      style="width:100% !important; Margin:0; padding:0;">
<table class="bigcontent" style="max-width:600px;min-width:320px" width="100%" cellspacing="0" cellpadding="0"
       border="0" bgcolor="#ffffff" align="center">
    <tr>
        <td>
            <!--[if (gte mso 9)|(IE)]>
            <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
            <![endif]-->
            <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td>
                        <img src="{{ asset('img/mailings/header.png') }}" width="100%" alt="" style="display: block;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="86%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td height="60"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 16px; color: #002347;"
                                    align="center"><strong>TIENES UN NUEVO MENSAJE</strong></td>
                            </tr>
                            <tr>
                                <td height="44"></td>
                            </tr>

                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347;"
                                    align="center">Hola, un usuario desea contactarse contigo:
                                </td>
                            </tr>
                            <tr>
                                <td height="62"></td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; padding-top: 0px; padding-bottom: 0px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table width="100%">
                                        <tr>
                                            <td width="50%" valign="top" align="right">
                                                <table width="179" cellpadding="0" cellspacing="0" border="0"
                                                       align="right">
                                                    <tr>
                                                        <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Nombre:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->name }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    <td width="50%" valign="top" align="left">
                                        <table width="179" cellpadding="0" cellspacing="0" border="0" align="left">
                                            <tr>
                                                <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Apellido:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->last_name }}
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; padding-top: 0px; padding-bottom: 0px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table width="100%">
                                        <tr>
                                            <td width="50%" valign="top" align="right">
                                                <table width="179" cellpadding="0" cellspacing="0" border="0"
                                                       align="right">
                                                    <tr>
                                                        <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            DNI:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->document }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    <td width="50%" valign="top" align="left">
                                        <table width="179" cellpadding="0" cellspacing="0" border="0" align="left">
                                            <tr>
                                                <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            RUC:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->business_document }}
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; padding-top: 0px; padding-bottom: 0px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table width="100%">
                                        <tr>
                                            <td width="50%" valign="top" align="right">
                                                <table width="179" cellpadding="0" cellspacing="0" border="0"
                                                       align="right">
                                                    <tr>
                                                        <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Nombre de la empresa:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->business_name }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    <td width="50%" valign="top" align="left">
                                        <table width="179" cellpadding="0" cellspacing="0" border="0" align="left">
                                            <tr>
                                                <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Nivel de especialización:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->specialization->meta_value }}
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; padding-top: 0px; padding-bottom: 0px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table width="100%">
                                        <tr>
                                            <td width="50%" valign="top" align="right">
                                                <table width="179" cellpadding="0" cellspacing="0" border="0"
                                                       align="right">
                                                    <tr>
                                                        <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Email:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;word-break:break-all;">
                                                            {{ $contact->email }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    <td width="50%" valign="top" align="left">
                                        <table width="179" cellpadding="0" cellspacing="0" border="0" align="left">
                                            <tr>
                                                <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Celular:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->phone }}
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; padding-top: 0px; padding-bottom: 0px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table width="100%">
                                        <tr>
                                            <td width="50%" valign="top" align="right">
                                                <table width="179" cellpadding="0" cellspacing="0" border="0"
                                                       align="right">
                                                    <tr>
                                                        <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Producto interés:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->product_interest }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    <td width="50%" valign="top" align="left">
                                        <table width="179" cellpadding="0" cellspacing="0" border="0" align="left">
                                            <tr>
                                                <td>
                                    <![endif]-->
                                    <table style="width: 100%; max-width: 258px; display: inline-block; vertical-align: top; padding-left: 15px; padding-right: 15px"
                                           cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                            Medio de contacto:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 27px;">
                                                            {{ $contact->howContact->meta_value }}
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 15px; padding-right: 15px">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #8F8F8F;">
                                                Mensaje:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10"></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #002347; line-height: 19px;">
                                                {{ $contact->message }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="92"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="86%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; color: #FF4141; font-size: 10px;"
                                    align="center"><a href="#" style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Nuestra web</font></a> | <a href="#"
                                                                                            style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Productos</font></a> | <a href="#"
                                                                                          style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Servicios</font></a> | <a href="#"
                                                                                          style="text-decoration: none; color: #FF4141;"><font
                                                color="#FF4141">Eventos</font></a></td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; color: #002347;"
                                    align="center">© {{ \Carbon\Carbon::now()->format('Y') }} Grupo Bonnett.<br>All rights reserved.
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Geneva, sans-serif; font-size: 10px;" align="center"><a
                                            href="#" style="text-decoration: none"><font color="#FF4141">Términos y
                                            condiciones</font></a></td>
                            </tr>
                            <tr>
                                <td height="100"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->

        </td>
    </tr>
</table>
</body>
</html>
