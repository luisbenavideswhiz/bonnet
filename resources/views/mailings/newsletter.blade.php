<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/><!--[if !mso]>
		<!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/><!--
		<![endif]--><!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title></title>
    <style type="text/css">
        div, p, a, li, td {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body {
            min-width: 100% !important;
            Margin: 0;
            padding: 0;
        }

        #outlook a {
            padding: 0;
        }

        img {
            display: block;
            line-height: 100%;
            border: 0;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        table {
            border-collapse: collapse !important;
            border-spacing: 0;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        h1, h3, p, a {
            color: #002347;
            font-family: Helvetica, sans-serif;
        }

        table td {
            border-collapse: collapse;
        }

        @media only screen and (min-device-width: 320px) and (max-device-width: 1024px) {
            a[href^="tel"], a[href^="sms"], a {
                color: inherit !important;
                cursor: default !important;
                text-decoration: none !important;
            }
        }

        @media only screen and (max-width: 480px) {
            img[class="img100pc"] {
                width: 100%;
                height: auto;
            }

            table[class="hide"], td[class="hide"], span[class="hide"], br[class="hide"] {
                display: none !important;
            }

            table[class="w100pc"] {
                width: 100%;
                margin-bottom: 10px;
            }

            td[class="w10"] {
                width: 10px !important;
            }

            td[class="h30"] {
                height: 30px !important;
            }

            td[class="h20"] {
                height: 20px !important;
            }

            td[class="h15"] {
                height: 15px !important;
            }

            td[class="h10"] {
                height: 10px !important;
            }

            td[class="txtcenter"] {
                text-align: center !important;
            }

            span[class="block"] {
                display: block !important;
            }
        }

        @media only screen and (max-width: 320px) {
        }

        div[style*="margin: 16px 0"] {
            margin: 0 !important;
            font-size: 100% !important;
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css" media="all">
        td[class="outlook-f"] {
            width: 244px !important;
        }

        td[class="outlook-c"] {
            width: 500px !important;
        }

        @media only screen and (max-width: 480px) {
            td[class="outlook-f"] {
                width: 100% !important
            }
        }
    </style>
    <![endif]-->
</head>
<body bgcolor="#f2f2f2" marginleft="0" marginright="0" margintop="0"
      style="margin: 0px; padding: 0px; width: 100% !important;">
<p><span class="hidesumary" style="display: none;">Aviso de aprobaci�n</span></p>
<table align="center" bgcolor="#f2f2f2" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td>
            <table align="center" bgcolor="#f2f2f2" border="0" cellpadding="0" cellspacing="0" class="w100pc"
                   width="720">
                <tr>
                    <td width="84"></td>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td height="50"></td>
                            </tr>
                        </table>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td height="10"></td>
                            </tr>
                        </table>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="  border-radius: 0px; width: 700px;background: #ffffff;">
                            <tbody>
                            <tr>
                                <td height="50">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="50">&nbsp;</td>
                                            <td>
                                                <table align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <img src="{{ asset('img/logo.png') }}" alt="" width="200px"
                                                                 height="67">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="50">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50">&nbsp;</td>
                                            <td></td>
                                            <td width="50">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50">&nbsp;</td>
                                            <td>
                                                <table style="margin: 0 auto;">
                                                    <tbody>
                                                    <tr>
                                                        <td width="50"></td>
                                                        <td>
                                                            <h1 style="color: #002347; font-family:Helvetica,sans-serif; font-size: 24px; font-weight: normal; margin: 0; text-align: center;">
                                                                Te monstramos nuestras <br>
                                                                últimas noticias
                                                            </h1>
                                                        </td>
                                                        <td width="50"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50"></td>
                                                        <td align="center">
                                                            <p style="padding-top: 10px">
                                                                {{ $newsletter->description }}
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="50">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50">&nbsp;</td>
                                            <td width="50">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="40"></td>
                                            <td>
                                                <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                       width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <td width="230">
                                                            <div style="background-color: #dddddd; height: 1px;"></div>
                                                        </td>
                                                        <td width="230">
                                                            <div style="background-color: #dddddd; height: 1px;"></div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="40"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="40">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        @foreach($newsletter->posts as $post)
                                            <tr>
                                                <td width="50">&nbsp;</td>
                                                <td>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                           width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="text-align: center; vertical-align: top; font-size: 0px;">
                                                                <table align="left" cellpadding="0"
                                                                       cellspacing="0" width="100%"
                                                                       style="border:1px solid #D0D0D0;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td width="250">
                                                                            <img src="{{ asset($post->image) }}"
                                                                                 alt="" style="width: inherit"/>
                                                                        </td>
                                                                        <td>&nbsp;</td>
                                                                        <td valign="top" width="100%">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0" width="100%"
                                                                                   style="color: #393939; font-family: Helvetica,sans-serif; font-size: 14px;font-style: normal; padding: 15px;">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td height="15">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="15">&nbsp;</td>
                                                                                    <td align="left">
                                                                                        <h3 style="color: #002347; font-size: 18px; font-weight: normal">
                                                                                            {{ $post->title }}
                                                                                        </h3>
                                                                                    </td>
                                                                                    <td width="15">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="15">&nbsp;</td>
                                                                                    <td align="left" height="110">
                                                                                        <p>
                                                                                            {!! $post->short_description !!}
                                                                                        </p>
                                                                                    </td>
                                                                                    <td width="15">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="10">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="15">&nbsp;</td>
                                                                                    <td height="34" align="left">
                                                                                        <a href="{{ route('web.post', ['slug'=>$post->slug]) }}"
                                                                                           style="border: 1px solid #002347; font-size: 14px; color: #002347;border-radius: 25px; padding: 8px 24px; text-decoration: none;">
                                                                                            LEER MÁS</a>
                                                                                    </td>
                                                                                    <td width="15">&nbsp;</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 14px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 14px;">&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="50">&nbsp;</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="50" align="center" valign="middle">
                                    <a href="{{ route('web.posts') }}"
                                       style="border-radius: 25px; padding:10px 200px; text-decoration: none; background-image: linear-gradient(258deg, #ff4141, #0050dc); color: #fff;">
                                        LEER MÁS</a>
                                </td>
                            </tr>
                            <tr>
                                <td height="28"></td>
                            </tr>
                            <tr>
                                <td height="340" align="center" style="background-color: #4F4F4F">
                                    <img src="{{ asset($newsletter->image) }}" height="340">
                                </td>
                            <tr>
                                <td height="6" style="background-image:linear-gradient(258deg, #ff4141, #0050dc)"></td>
                            </tr>
                            <tr>
                                <td height="212" style="background-color: #002347;">
                                    <table width="100%" align="center">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tbody>
                                                    <td width="190"></td>
                                                    <td width="35">
                                                        <a href="https://www.facebook.com/bonnettsa/"><img
                                                                src="{{ asset('img/mailings/facebook.png') }}"
                                                                alt=""></a>
                                                    </td>
                                                    <td width="35">
                                                        <a href="https://twitter.com/bonnettsa"><img
                                                                src="{{ asset('img/mailings/twiter.png') }}"
                                                                alt=""></a>
                                                    </td>
                                                    <td width="35">
                                                        <a href="#"><img src="{{ asset('img/mailings/instagram.png') }}"
                                                                         alt=""></a>
                                                    </td>
                                                    <td width="35">
                                                        <a href="https://www.linkedin.com/company/grupo-bonnett"><img
                                                                src="{{ asset('img/mailings/linkedin.png') }}"
                                                                alt=""></a>
                                                    </td>
                                                    <td width="190"></td>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <p style="color: #fff; font-weight: normal; font-size: 14px; text-align: center">
                                                                719-2121 / 446-1587 /446-0349
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p style="color: #fff; font-weight: normal; font-size: 14px; text-align: center">
                                                                web@grupobonnett.com
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p style="color: #fff; font-weight: normal; font-size: 14px; text-align: center">
                                                                Jr. Domingo Martinez Luján 1155 Surquillo, Lima, Peru
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="44" style="background-color: #001933;" align="center" valign="middle">
                                    <p style="color: #385573; font-size: 12px">
                                        Grupo Bonnett - Todos los derechos
                                        reservados {{ \Carbon\Carbon::now()->format('Y') }} &nbsp; | &nbsp; Powered by
                                        WHIZ
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="84"></td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
