@extends('web.layouts.app')

@section('content')
    <section class="product-detail">
        <div class="container">
            <div class="product-title">
                <div class="title"><h1>{{ $product->name }}</h1></div>
                <nav class="breadcrumb">
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="{{ route('web.products') }}">Productos</a></li>
                        <li class="breadcrumb-item">
                            <a href="/producto/buscar?brand[]={{ $product->brand->slug }}">{{ $product->brand->name }}</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/producto/buscar?type[]={{ $product->type->meta_option }}">{{ $product->type->meta_value }}</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="clearfix">
                <div class="left">
                    <div class="product-img">
                        <ul class="product-img-list product-slider">
                            @if($product->gallery->count() == 0)
                                <img src="{{ asset(isset($product->image) ? $product->image->path: 'http://placehold.it/540x382') }}"
                                     alt="">
                            @else
                                @foreach($product->gallery as $key => $value)
                                    <li class="product-img-item"><img src="{{ asset($value->path) }}" alt=""></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="right">
                    <div class="product-desc">
                        <div class="product-summary">{!! $product->description !!}</div>
                        {{-- <div class="product-id"><p>ID: <span>{{ $product->sku }}</span></p></div>
                        <div class="product-quota"><p>Productos Disponibles:<span>{{ $product->stock }}</span></p></div> --}}
                    </div>
                    <div class="product-help">
                        <div class="product-contact">
                            <a href="{{ route('web.contact') }}" class="button"><span>Contactar</span></a>
                        </div>
                        @if(!is_null($product->pdf))
                            <div class="product-cataloge">
                                <a href="{{ $product->pdf->path }}" target="_lblank" class="button"><span>Catálogo</span></a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($product->detail->count() > 0)
        <section class="product-description">
            <div class="container">
                <div class="tab">
                    <ul class="tab-nav">
                        @foreach($product->detail as $key => $value)
                            <li class="@if($key == 0) active @endif">
                                <a href="javascript:void(0)">{{ $value->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <ul class="tab-content">
                        @foreach($product->detail as $key => $value)
                            <li class="tab-pane @if($key == 0) active @endif">{!! $value->description !!}</li>
                            @if(isset($value->pdf))
                                <h5>Descargar
                                    <a href="{{ $value->pdf->path }}" download
                                       style="border-bottom-style: solid">aquí</a>
                                </h5>
                            @endif
                            @if(isset($value->image))
                                <img src="{{ $value->image->path }}" alt="">
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>
    @endif
    <section class="product-category sb" id="product_detail">
        <div class="container">
            <div class="title"><h2>Productos Similares</h2></div>
        </div>
        <product-related></product-related>
    </section>
@endsection
