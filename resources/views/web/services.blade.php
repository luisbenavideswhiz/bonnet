@extends('web.layouts.app')

@section('content')
    <section class="service-info summary">
        <div class="container clearfix">
            <div class="left">
                <div class="title"><h2>Servicios</h2></div>
            </div>
            <div class="right">{!! $banner->count() > 0 ? $banner->meta_option : 'No hay datos!!' !!}</div>
        </div>
    </section>
    @if($services->count() > 0)
        @foreach($services as $key => $value)
            <section class="service-detail @if($key % 2 === 0) first @else second @endif sb">
                <div class="container">
                    <div class="service-desc">
                        <div class="title"><h2>{{ $value->title }}</h2></div>
                        <div class="text">{!! $value->description !!}</div>
                    </div>
                    <div class="clearfix">
                        <div class="@if($key % 2 === 0) left @else right @endif">
                            <div class="service-img">
                                <img src="{{ asset(isset($value->image) ? $value->image->path : 'img/home-about.jpg') }}"
                                     alt="">
                            </div>
                        </div>
                        <div class="@if($key % 2 === 0) right @else left @endif">
                           <div class="service-info">{!! $value->detail !!}</div>
                        </div>
                    </div>
                </div>
            </section>
        @endforeach
    @endif
@endsection