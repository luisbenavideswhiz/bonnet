@if($events->count() > 0)
    @foreach($events as $key => $value)
        @include('web.partials.events_item')
    @endforeach
@endif
<div class="load-more">
    <a id="load_more" data-id="{{ $value->id }}" href="javascript:void(0)" class="button">
        <span>Ver más</span>
    </a>
</div>
