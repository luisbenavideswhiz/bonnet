<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Próximamente</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>

<style>
    body {
        background: #f5f5f5;
    }

    .fullpage {
        display: table;
        width: 100%;
        height: 100vh;
    }

    .fullpage-cnt {
        padding-top: 20px;
        padding-bottom: 20px;
        display: table-cell;
        vertical-align: middle;
        text-align: center;
    }

    .logo {
        margin-bottom: 60px;
    }

    .logo img {
        display: inline-block;
        animation: pulse 1.2s infinite;
        -webkit-animation: pulse 1.2s infinite;
        -moz-animation: pulse 1.2s infinite;
        -ms-animation: pulse 1.2s infinite;
    }

    .timer {
        padding-left: 0;
        margin: 0;
        list-style: none;
        font-size: 20px;
        color: #999;
        letter-spacing: -1px;
    }

    .timer .timer-item {
        display: inline-block;
        vertical-align: middle;
        margin-left: 15px;
        margin-right: 15px;
        color: #002347;
        font-size: 18px;
    }

    .timer span {
        font-size: 48px;
        color: #002347;
        font-family: 'patron_bold', sans-serif;
    }

    .timer span:first-child {
        margin-left: 0;
    }

    @media screen and (max-width: 450px) {
        .fullpage {
            display: block;
            height: auto;
        }

        .fullpage-cnt {
            display: block;
        }

        .logo {
            margin-bottom: 30px;

        }

        .timer .timer-item {
            display: block;
            margin-bottom: 15px;
        }
    }

</style>

<div class="fullpage">
    <div class="fullpage-cnt">
        <div class="logo">
            <img src="{{ asset('img/logo-cs.svg') }}" width="83" alt="">
        </div>
        <ul class="timer">
            <li class="timer-item">
                <div><span id="days"></span></div>
                <div>Días</div>
            </li>
            <li class="timer-item">
                <div><span id="hours"></span></div>
                <div>Horas</div>
            </li>
            <li class="timer-item">
                <div><span id="minutes"></span></div>
                <div>Minutos</div>
            </li>
            <li class="timer-item">
                <div><span id="seconds"></span></div>
                <div>Segundos</div>
            </li>
        </ul>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    var timer;
    var compareDate = new Date("October 1, 2018 13:00:00");
    compareDate.setDate(compareDate.getDate()); //just for this demo today + 7 days

    timer = setInterval(function () {
        timeBetweenDates(compareDate);
    }, 1000);

    function timeBetweenDates(toDate) {
        var dateEntered = toDate;
        var now = new Date();
        var difference = dateEntered.getTime() - now.getTime();

        if (difference <= 0) {
            clearInterval(timer);
            setTimeout(function () {
                window.location.href = window.location.protocol + "//" + window.location.host + "/";
            }, 500);
        } else {
            var seconds = Math.floor(difference / 1000);
            var minutes = Math.floor(seconds / 60);
            var hours = Math.floor(minutes / 60);
            var days = Math.floor(hours / 24);

            hours %= 24;
            minutes %= 60;
            seconds %= 60;

            $("#days").text(days);
            $("#hours").text(hours);
            $("#minutes").text(minutes);
            $("#seconds").text(seconds);
        }
    }
</script>
</body>
</html>