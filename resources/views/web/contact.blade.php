@extends('web.layouts.app')

@section('content')
    <section class="contact" id="contact">
        <div class="container">
            <div class="title">
                <h2>Contáctanos</h2>
            </div>
            <div class="contact-form form">
                <form v-on:submit.prevent="onSubmit" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-control" v-bind:class="{ error: errors.name, active: name }">
                                    <div class="form-label">
                                        <label for="">Nombre</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text" v-model="name" id="name">
                                    </div>
                                    <span class="error-label" v-if="errors.name">
                                        @{{ errors.name[0] }}
                                    </span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control" v-bind:class="{ error: errors.last_name, active: last_name }">
                                    <div class="form-label">
                                        <label for="">Apellido</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text" v-model="last_name" id="last_name">
                                    </div>
                                    <span class="error-label" v-if="errors.last_name">
                                        @{{ errors.last_name[0] }}
                                    </span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control" v-bind:class="{ error: errors.document, active: document }">
                                    <div class="form-label">
                                        <label for="">DNI</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text" v-model="document" id="document">
                                    </div>
                                    <span class="error-label" v-if="errors.document">
                                        @{{ errors.document[0] }}
                                    </span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control"
                                     v-bind:class="{ error: errors.business_document, active: business_document }">
                                    <div class="form-label">
                                        <label for="">RUC</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text" v-model="business_document" id="business_document">
                                    </div>
                                    <span class="error-label" v-if="errors.business_document">
                                        @{{ errors.business_document[0] }}
                                    </span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control"
                                     v-bind:class="{ error: errors.business_name, active: business_name }">
                                    <div class="form-label">
                                        <label for="">Nombre de la empresa</label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text" v-model="business_name" id="business_name">
                                    </div>
                                    <span class="error-label" v-if="errors.business_name">
                                        @{{ errors.business_name[0] }}
                                    </span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-control"
                                     v-bind:class="{ error: errors.specialization_id, active: specialization_id }">
                                    <div class="form-label">
                                        <label for="">Nivel de especializaci&oacute;n</label>
                                    </div>
                                    <div class="form-select">
                                        <select v-model="specialization_id" id="specialization_id">
                                            <option value=""></option>
                                            <option value="0">Otros</option>
                                            @foreach($specializations as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->meta_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <span class="error-label" v-if="errors.specialization_id">
                                        @{{ errors.specialization_id[0] }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group other-group" style="display: none">
                            <div class="form-control" v-bind:class="{ error: errors.other, active: other }">
                                <div class="form-label">
                                    <label for="">Otra especializaci&oacute;n</label>
                                </div>
                                <div class="form-input">
                                    <input type="text" v-model="other" id="other" >
                                </div>
                                <span class="error-label" v-if="errors.other">
                                    @{{ errors.other[0] }}
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-control" v-bind:class="{ error: errors.email, active: email }">
                                        <div class="form-label">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text" v-model="email" id="email">
                                        </div>
                                        <span class="error-label" v-if="errors.email">
                                            @{{ errors.email[0] }}
                                        </span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-control" v-bind:class="{ error: errors.phone, active: phone }">
                                        <div class="form-label">
                                            <label for="">Celular</label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text" v-model="phone" id="phone">
                                        </div>
                                        <span class="error-label" v-if="errors.phone">
                                            @{{ errors.phone[0] }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-control"
                                         v-bind:class="{ error: errors.product_interest, active: product_interest }">
                                        <div class="form-label">
                                            <label for="">Producto de inter&eacute;s</label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text" v-model="product_interest" id="product_interest">
                                        </div>
                                        <span class="error-label" v-if="errors.product_interest">
                                            @{{ errors.product_interest[0] }}
                                        </span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-control"
                                         v-bind:class="{ error: errors.how_contact, active: how_contact }">
                                        <div class="form-label">
                                            <label for="">¿C&oacute;mo nos contactó?</label>
                                        </div>
                                        <div class="form-select">
                                            <select type="text" v-model="how_contact" id="how_contact">
                                                <option value=""></option>
                                                @foreach($howContacts as $key => $value)
                                                    <option value="{{ $value->id }}">{{ $value->meta_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="error-label" v-if="errors.how_contact">
                                            @{{ errors.how_contact[0] }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" v-bind:class="{ error: errors.message, active: message }">
                            <div class="form-label">
                                <label for="">Escribe tu mensaje</label>
                            </div>
                            <div class="form-textarea">
                                <textarea v-model="message" id="message" cols="30" rows="10"></textarea>
                            </div>
                            <span class="error-label" v-if="errors.message">
                                @{{ errors.message[0] }}
                            </span>
                        </div>
                        <div class="form-button">
                            <button type="submit" class="button"><span>Enviar</span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section class="find-us sb" id="find_map">
        <google-map></google-map>
    </section>
@endsection

@section('modal')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt3-aLL5MF1iaRTiFXgz8CeomI9xCeJcE"></script>
@endsection
