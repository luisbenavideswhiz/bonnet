@extends('web.layouts.app')

@section('content')
    <section class="certification">
        <div class="container">
            <div class="title">
                <h2>Nuestras Certificaciones</h2>
            </div>
            <div class="row">
                @foreach($certificates as $key => $value)
                    <div class="col-4 text-center">
                        <img src="{{ $value->image->path }}" width="270" height="380" alt="">
                    </div>
                @endforeach
            </div>
            <br>
            <div class="certification-button">
                <a href="{{ route('web.home') }}" class="button"><span>Volver al inicio</span></a>
            </div>
        </div>
    </section>
@endsection
