@extends('web.layouts.app')

@section('content')
    <section class="about-we summary">
        <div class="container clearfix">
            <div class="left">
                <div class="title"><h2>Nosotros</h2></div>
            </div>
            <div class="right">
                {!! isset($enterprise) ? $enterprise->description: '' !!}
            </div>
        </div>
    </section>
    @if(count($corporations) > 0)
        <section class="about-corporation sb">
            <div class="container">
                <div class="title"><h2>Nuestra Corporación</h2></div>
                <ul class="corporation-list">
                    @foreach($corporations as $key => $value)
                        <li class="corporation-item">
                            <img src="{{ asset($value->image->path) }}" alt="">
                            <p>{!! $value->description !!}</p>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif
    <section class="about-mv">
        <div class="left">
            <div class="about-vision first">
                <div class="about-bg"
                     style="background-image:url('{{ isset($vision)?$vision->image->path:'' }}')"></div>
                <div class="about-desc">
                    <h3>Visión</h3>
                    {!! isset($vision) ? $vision->description: '' !!}
                    <div class="about-button">
                        <a href="javascript:void(0)" class="button btn-mission"><span>Misión</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="right">
            <div class="about-mission">
                <div class="about-bg"
                     style="background-image:url('{{ isset($mission)?$mission->image->path:'' }}')"></div>
                <div class="about-desc">
                    <h3>Misión</h3>
                    {!! isset($mission) ? $mission->description: '' !!}
                    <div class="about-button">
                        <a href="javascript:void(0)" class="button btn-vision"><span>Visión</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-values">
        <div class="container">
            <div class="about-title title">
                <h2>Valores</h2>
            </div>
            <div class="clearfix">
                <div class="left">
                    <div class="about-img">
                        <img src="{{ asset(isset($holding) ? $holding->image->path : '') }}" alt="">
                    </div>
                </div>
                <div class="right">
                    {!! isset($holding) ? $holding->description : '' !!}
                </div>
            </div>
        </div>
    </section>
@endsection
