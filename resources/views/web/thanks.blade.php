@extends('web.layouts.app')

@section('content')
    <section class="thank">
        <div class="container">
            <div class="thank-img">
                <img src="{{ asset('img/send.svg') }}" width="270" alt="">
            </div>
            <div class="title">
                <h2>Su mensaje fue enviado con éxito.</h2>
            </div>
            <ul class="button-group">
                <li>
                    <a class="button" href="{{ route('web.home') }}"><span>Inicio</span></a>
                </li>
                <li>
                    <a class="button" href="{{ route('web.products') }}"><span>Productos</span></a>
                </li>
            </ul>
        </div>
    </section>
@endsection
