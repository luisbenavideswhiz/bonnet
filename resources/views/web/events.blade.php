@extends('web.layouts.app')

@section('content')
    <section class="event-info summary">
        <div class="container clearfix">
            <div class="left">
                <div class="title">
                    <h2>Eventos</h2>
                </div>
            </div>
            <div class="right">
                {!! $banner->meta_option !!}
            </div>
        </div>
    </section>
    <div class="load-data">
        @include('web.events_ajax')
    </div>
@endsection

@section('scripts')
    <script>
        $(function(){
            $(document).on('click','#load_more',function(){
                var id = $(this).data('id');
                $("#load_more").find('span').html("Cargando....");
                $.ajax({
                    url : '{{ url('eventos/loadmore') }}',
                    method : "POST",
                    data : {id:id, _token:"{{csrf_token()}}"},
                    success : function (response)
                    {
                        if(response.status)
                        {
                            $('.load-more').remove();
                            $('.load-data').append(response.data);
                        }
                        else
                        {
                            $('#load_more').hide();
                        }
                    }
                });
            });
        });
    </script>
@endsection
