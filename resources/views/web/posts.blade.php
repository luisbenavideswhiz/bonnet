@extends('web.layouts.app')

@section('content')
    <section class="post-info">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @if($posts->count() != 0)
                        <h2>Nuestras últimas noticias</h2>
                    @else
                        <h2>Hola, muy pronto tendremos noticias</h2>
                    @endif
                </div>
            </div>
            <div class="row">
                @forelse($posts as $key => $value)
                    @include('web.partials.blog-item')
                @empty @endforelse
            </div>
            @include('web.partials.paginate')
        </div>
    </section>
@endsection