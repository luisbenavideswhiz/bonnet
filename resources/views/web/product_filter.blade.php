@extends('web.layouts.app')

@section('content')
    <section class="product-filter" id="product_search">
        <div class="container">
           <product-filter></product-filter>
        </div>
    </section>
@endsection
