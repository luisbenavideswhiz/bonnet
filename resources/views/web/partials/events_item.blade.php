<section class="event-detail @if($key % 2 === 0) first @else second @endif sb">
    <div class="container clearfix">
        <div class="@if($key % 2 === 0) left @else right @endif">
            <div class="event-img">
                <img src="{{ asset(!is_null($value->gallery_id) ? $value->image->path : 'img/home-about.jpg') }}"
                     alt="">
            </div>
        </div>
        <div class="@if($key % 2 === 0) right @else left @endif">
            <div class="title"><h2>{{ $value->title }}</h2></div>
            <div class="event-text">{!! $value->text !!}</div>
            <div class="event-button">
                <a href="{{ route('web.events.show', ['slug'=>$value->slug]) }}" class="button"><span>Leer más</span></a>
            </div>
        </div>
    </div>
</section>