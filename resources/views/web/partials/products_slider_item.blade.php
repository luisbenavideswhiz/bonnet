@foreach($categories as $key => $value)
    <div class="product-category @if($key % 2 === 0) category-2 @endif @if($key !== 0) sb @endif">
        <div class="container">
            <div class="title"><h2>{{ $value->name }}</h2></div>
        </div>
        <div class="product-list product-carousel">
            @foreach($value->product as $product)
                <figure class="product-item">
                    <a href="{{ route('web.product_detail', ['slug'=> $product->slug]) }}">
                        <div class="product-photo">
                            <img src="{{ asset(isset($product->image) ? $product->image->path : '') }}" alt="">
                        </div>
                        <figcaption class="product-name">
                            <span>{{ strlen(substr($product->name, 0, 50)) == 50 ? substr($product->name, 0, 50).'...' : substr($product->name, 0, 50)}}</span>
                        </figcaption>
                    </a>
                </figure>
            @endforeach
        </div>
        <div class="product-category-btn">
            <a href="/producto/buscar?category={{ $value->slug }}" class="button"><span>Ver categoría</span></a>
        </div>
    </div>
@endforeach