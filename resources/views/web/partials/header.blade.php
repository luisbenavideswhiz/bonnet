<header class="header">
    <div class="header-primary">
        @if($banner_top->meta_value != 0)
            <a href="{{ $banner_top->meta_link }}" class="header-label">
                <div class="container">
                    <p>{{ $banner_top->meta_option }}<span class="icon icon-arrow-right"></span></p>
                </div>
            </a>
        @endif
        <div class="header-cnt container clearfix">
            <div class="left">
                <div class="logo">
                    <a href="{{ route('web.home') }}">Bonnett</a>
                </div>
            </div>
            <div class="right">
                <nav class="menu-primary">
                    <ul class="menu-list">
                        <li class="menu-item {{ Route::is('web.about') ? 'active' : '' }}">
                            <a class="menu-link" href="{{ route('web.about') }}">{{ $menu_us->meta_value }}</a>
                        </li>
                        <li class="menu-item menu-link-hover {{ Route::is('web.products') ? 'active' : '' }}">
                            <a class="menu-link" href="#">{{ $menu_product->meta_value }} <span class="icon-arrow-bottom"></span></a> 
                            <ul class="li-sub-menu">
                                @foreach($categories as $key => $value)
                                    <li>
                                        <a href="{{ url('producto/buscar?category='.$value->slug) }}"
                                           data-value="{{ $value->slug }}">{{ $value->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="sub-menu">
                                <div class="container">
                                    <div class="row">
                                        @foreach($categories as $key => $value)
                                        <div class="col-3 width">
                                            <a href="{{ url('producto/buscar?category='.$value->slug) }}" data-value="{{ $value->slug }}"><b>{{ $value->name }}</b></a>
                                            <hr>
                                            @foreach($types as $key => $value2)
                                                @if($value->product[0]->category_id == $value2->category_id)
                                                    <a href="{{ url('producto/buscar?category='.$value->slug.'&type[]='.$value2->meta_option) }}" data-value="{{ $value->slug }}">{{ $value2->meta_value }}</a> <br>
                                                @endif
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="menu-item {{ Route::is('web.services.*') ? 'active' : '' }}">
                            <a class="menu-link"
                               href="{{ route(('web.services.index')) }}">{{ $menu_service->meta_value }}</a>
                        </li>
                        <li class="menu-item {{ Route::is('web.contact') ? 'active' : '' }}">
                            <a class="menu-link" href="{{ route('web.contact') }}">{{ $menu_contact->meta_value }}</a>
                        </li>
                        <li class="menu-item {{ Route::is('web.events.*') ? 'active' : '' }}">
                            <a class="menu-link"
                               href="{{ route('web.events.index') }}">{{ $menu_event->meta_value }}</a>
                        {{-- </li>
                        <li class="menu-item {{ Route::is('web.posts', 'web.post') ? 'active' : '' }}">
                            <a class="menu-link"
                               href="{{ route('web.posts') }}">{{ $menu_blog->meta_value }}</a>
                        </li> --}}
                    </ul>
                </nav>
                <div class="header-filter">
                    <form action="{{ route('web.product_filter') }}" method="GET">
                        <div class="form-dropdown">
                            <a class="dropdown-button" href="javascript:void(0)"><span class="label-search"></span></a>
                            <ul class="dropdown-list">
                                @foreach($categories as $key => $value)
                                    <li class="dropdown-item">
                                        <a href="javascript:void(0)"
                                           data-value="{{ $value->slug }}">{{ $value->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="form-input">
                            <input type="text" id="search" name="q" placeholder="Estoy buscando..." autocomplete="off">
                        </div>
                        <div class="form-select">
                            <select name="category" id="category" disabled>
                                <option>Categorias</option>
                                @foreach($categories as $key => $value)
                                    <option value="{{ $value->slug }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-button">
                            <a class="btn-search" href="javascript:void(0)"><span class="icon-search"></span></a>
                        </div>
                    </form>
                    <label class="error"></label>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mobile">
        @if($banner_top->meta_value != 0)
            <a href="{{ $banner_top->meta_link }}" class="header-label">
                <div class="container">
                    <p>{{ $banner_top->meta_option }}<span class="icon icon-arrow-right"></span></p>
                </div>
            </a>
        @endif
        <div class="header-cnt container">
            <div class="clearfix">
                <div class="left">
                    <a class="button-menu" href="javascript:void(0)">
                        <span class="bar1"></span>
                        <span class="bar2"></span>
                        <span class="bar3"></span>
                    </a>
                    <div class="logo">
                        <a href="{{ route('web.home') }}">Bonnett</a>
                    </div>
                    <div class="title">
                        <span></span>
                    </div>
                </div>
                <div class="right">
                    <a href="javascript:void(0)" class="button-search"><span class="icon-search"></span></a>
                </div>
            </div>
            <div class="header-mobile-search">
                <form action="{{ route('web.product_filter') }}" method="GET">
                    <div class="form-input">
                        <input type="search" name="q" placeholder="Estoy buscando…">
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
