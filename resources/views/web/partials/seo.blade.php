{{--HTML Meta Tags--}}
<title>{{ !isset($seo->title)?'Grupo Bonnett':$seo->title }}</title>
<meta name="title" content="{{ !isset($seo->title)?'':$seo->title }}">
<meta name="description" content="{{ !isset($seo->description)?'':$seo->description }}">
<meta name="keywords" content="{{ !isset($seo->keywords)?'':$seo->keywords }}">
<meta name="author" content="WHIZ - Agencia Digital">


{{--Open graph data--}}
<meta property="og:title" content="{{ !isset($seo->title)?'Grupo Bonnett':$seo->title }}"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="{{ !isset($seo->url)?'':$seo->url }}"/>
<meta property="og:image" content="{{ !isset($seo->image)?'':$seo->image }}"/>
<meta property="og:description" content="{{ !isset($seo->description)?'':$seo->description }}"/>
<meta property="og:site_name" content="Grupo Bonnett"/>
