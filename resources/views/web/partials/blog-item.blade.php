<div class="col-4">
    <a href="{{ route('web.post', ['slug'=> $value->slug]) }}">
        <div class="image" style="background: url({{ $value->image }})"></div>
    </a>
    <div class="data">
        <div class="date">
            <span>{{ $value->date_publish->format('d.m.y') }}</span>
        </div>
        <div class="title">
            <span>{{ $value->title }}</span>
        </div>
        <div class="description">
            {!! $value->short_description !!}
        </div>
        <div class="more">
            <a href="{{ route('web.post', ['slug'=> $value->slug]) }}" class="button"><span>leer más</span></a>
        </div>
    </div>
</div>