@foreach($comments as $key => $value)
    <div class="media-body @if($key%2==0) text-left bg-dark @else bg-white text-right @endif">
        <h4 class="media-heading">
            <strong>{{ $value->name }}</strong>
            <br>
            {{ $value->created_at->diffForHumans() }}
        </h4>
        <p>{{ $value->comment }}</p>
    </div>
@endforeach