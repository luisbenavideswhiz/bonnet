<footer class="footer">
    <div class="footer-info">
        <div class="container">
            <a href="javascript:void(0)" class="back-top"></a>
            <div class="footer-about footer-item">
                <h3>Grupo Bonnett</h3>
                {!! isset($footer['description']) ? $footer['description'] : '' !!}
            </div>
            <div class="footer-links footer-item">
                <h3>Enlaces Rápidos</h3>
                <ul>
                    @forelse($footer['quickLink'] as $key => $value)
                        <li><a href="{{ $value->meta_option }}">{{ $value->meta_value }}</a></li>
                    @empty @endforelse
                </ul>
            </div>
            <div class="footer-links footer-products footer-item">
                <h3>Productos</h3>
                <ul>
                    @forelse($categories as $key => $value)
                        <li><a href="{{ $value->slug }}">{{ $value->name }}</a></li>
                    @empty @endforelse
                </ul>
            </div>
            <div class="footer-social footer-item">
                <h3>Redes Sociales</h3>
                <ul class="social-list">
                    @forelse($footer['socialNetwork'] as $key => $value)
                        <li class="social-item">
                            <a href="{{ $value->network->meta_link }}{{ $value->meta_option }}" target="_blank">{!! $value->network->meta_option !!}</a>
                        </li>
                    @empty @endforelse
                </ul>
            </div>

            <div class="footer-contact footer-item">
                <div class="footer-item-title"><h3>Contáctenos</h3></div>
                <div class="footer-item-collapse">
                    <ul class="contact-list">
                        <li class="icon-mobile">
                            @if(count($footer['phone']) != 0)
                                @foreach($footer['phone'] as $key => $value)
                                    <a href="tel:{{ $value }}">
                                        @if($key % 2 === 0){{ $value }} @else / {{ $value }} / @endif
                                    </a>
                                @endforeach
                            @endif
                        </li>
                        <li class="icon-mail"><a href="mailto:{{ $footer['email'] }}">{{ $footer['email'] }}</a></li>
                        <li class="icon-map">{{ $footer['address'] }}</li>
                    </ul>
                </div>
                <ul class="social-list">
                    @if(count($footer['socialNetwork']) != 0)
                        @forelse($footer['socialNetwork'] as $key => $value)
                            <li class="social-item">
                                <a href="{{ $value->meta_option }}"
                                   target="_blank">{!! $value->network->meta_option !!}</a>
                            </li>
                        @empty @endforelse
                    @endif
                </ul>
            </div>
            <div class="footer-links-2 footer-item">
                <div class="footer-item-title">
                    <h3>Enlaces Rapidos</h3>
                </div>
                <div class="footer-item-collapse">
                    <ul>
                        @if(count($footer['socialNetwork']) != 0)
                            @forelse($footer['quickLink'] as $key => $value)
                                <li><a href="{{ $value->meta_option }}">{{ $value->meta_value }}</a></li>
                            @empty @endforelse
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <ul>
                <li>Grupo Bonnett - Todos los derechos reservados {{ date('Y') }}</li>
                <li>Powered by <a href="https://whiz.pe" target="_blank">WHIZ</a></li>
            </ul>
        </div>
    </div>
</footer>