@extends('web.layouts.app')

@section('content')
    <section class="event-detail-banner"
             style="background: url({{ asset(isset($event->image) ? $event->image->path : 'img/event-detail.jpg') }})">
        <div class="container clearfix">
            <div class="title"><h2>{{ $event->title }}</h2></div>
        </div>
    </section>
    <section class="event-detail-desc">
        <div class="container">
            <div class="event-detail-text">{!! $event->description !!}</div>
        </div>
    </section>
    <section class="event-detail-gallery">
        <div class="container">
            <ul class="gallery-list gallery-carousel">
                <div class="slider image">
                    @forelse($event->gallery as $key => $value)
                        <div>
                            <li class="gallery-item">
                                <a data-fancybox="gallery" rel="group" href="{{ url($value->path) }}">
                                    <img src="{{ url($value->path) }}">
                                </a>
                            </li>
                        </div>
                    @empty
                        <div>
                            <li class="gallery-item">
                                <img src="{{ asset(isset($event->image) ? $event->image->path : 'img/gallery -01.jpg') }}"
                                     alt="">
                            </li>
                        </div>
                    @endforelse
                </div>
            </ul>
        </div>
    </section>
    @if($recent_events->count() > 0)
        <section class="event-last sb">
            <div class="container">
                <div class="title"><h2>Eventos Recientes</h2></div>
                <div class="event-list">
                    @foreach($recent_events as $key => $value)
                        <figure class="event-item">
                            <a href="{{ route('web.events.show', ['slug'=> $value->slug]) }}">
                                <div class="event-photo">
                                    <img src="{{ asset(isset($value->image) && $value->image->path != '' ? $value->image->path : 'img/event-01.jpg') }}"
                                         alt="">
                                </div>
                                <figcaption class="event-name">
                                    <span>{{ strlen(substr($value->title, 0, 35)) == 35 ? substr($value->title, 0, 35).'...' : substr($value->title, 0, 35)}}</span>
                                </figcaption>
                            </a>
                        </figure>
                    @endforeach
                </div>
                <div class="event-button">
                    <a href="{{ route('web.events.index') }}" class="button"><span>Ver más</span></a>
                </div>
            </div>
        </section>
    @endif
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('scripts')
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@endsection