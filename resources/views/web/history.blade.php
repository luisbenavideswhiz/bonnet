@extends('web.layouts.app')

@section('content')
    <section class="politic">
        <div class="container">
            <div class="title"><h2>Historia</h2></div>
            <div class="politic-text">{!! is_null($history) ? '' : $history->description !!}</div>
            <div class="politic-button">
                <a href="{{ route('web.home') }}" class="button"><span>Volver al inicio</span></a>
            </div>
        </div>
    </section>
@endsection
