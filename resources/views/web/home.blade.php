@extends('web.layouts.app')

@section('bodyId')id="home"@endsection

@section('content')
    @if($sliders->count() > 0)
        <section class="banner">
            <div class="banner-cnt">
                <div class="banner-side">
                    <div class="container">
                        <div class="banner-text">
                            <p>{{ is_null($general_text)? '': $general_text->meta_value }}</p>
                        </div>
                    </div>
                </div>
                <ul class="banner-slider">
                    @foreach($sliders as $key => $value)
                        <li class="banner-item"
                            style="background-image: url({{ asset(isset($value->image)?$value->image->path:'') }})">
                            <div class="banner-item-cnt">
                                <div class="banner-info">
                                    <div class="banner-title">
                                        <h3>{{ $value->title != '' ? $value->title : '' }}</h3>
                                    </div>
                                    <div class="banner-text">
                                        {!! $value->description != '' ? $value->description : '' !!}
                                    </div>
                                    @if($value->button and $value->url)
                                        <div class="banner-button">
                                            <a href="{{ $value->url }}" class="button"><span>{{ $value->button }}</span></a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif
    @if($alliances->count() > 0)
        <section class="home-alliance">
            <div class="container">
                {{-- <div class="title"><h2>Marcas que representamos</h2></div> --}}
                <ul class="alliance-list alliance-carousel">
                    @foreach($alliances as $key => $value)
                        <li class="alliance-item">
                            <a href="{{ !is_null($value->description)? $value->description: 'javascript:void(0)'}}"
                               target="_blank">
                                <img src="{{ asset('/'.$value->path) }}" alt="">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif
    <section class="home-service">
        <div class="container">
            <div class="title">
                <h2>{{ is_null($second_banner['center']) ? '' : $second_banner['center'] }}</h2>
            </div>
            <div class="home-service-btn">
                <a class="button" href="{{ route('web.contact') }}"><span>Inscribirse</span></a>
            </div>
        </div>
        <div class="home-service-links">
            <div class="left">
                @if(!is_null($second_banner['left_link']))
                    <a href="{{ $second_banner['left_link'] }}">{{ $second_banner['left_text'] }}
                        <span class="icon icon-arrow-right"></span></a>
                @endif
            </div>
            <div class="right">
                @if(!is_null($second_banner['right_link']))
                    <a href="{{ $second_banner['right_link'] }}">{{ $second_banner['right_text'] }}
                        <span class="icon icon-arrow-right"></span></a>
                @endif
            </div>
        </div>
    </section>
    <section class="home-category">
        <div class="title">
            <h2>Nuestras Líneas</h2>
        </div>
        <div class="category-text">
            {!! $descriptive !!}
        </div>
        <div class="category-list category-carousel">
            @foreach($categories as $key => $value)
                <figure class="category-item">
                    <!-- <a href="/producto/buscar?category={{ $value->slug }}"> -->
                    <a href="{{ url('producto/buscar?category='.$value->slug) }}">
                        <div class="category-photo">
                            <img src="{{ isset($value->image) ? $value->image->path : 'http://placehold.it/270x320'}}">
                        </div>
                        <figcaption class="category-name">
                            <span>{{ $value->description }}</span>
                        </figcaption>
                    </a>
                </figure>
            @endforeach
        </div>
        @if($show_link == 1)
        <div class="home-category-button">
            <a href="{!! $link !!}" target="_blank"
               class="button"><span>{!! $name_button !!}</span></a>
        </div>
        @endif
    </section>
@endsection

