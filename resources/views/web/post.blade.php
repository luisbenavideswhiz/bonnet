@extends('web.layouts.app')

@section('content')
    <section class="event-detail-banner"
             style="background-image: url({{ url($post->image) }}); background-size: cover">
        <div class="container clearfix">
            <div class="post-title">
                <h2>{{ $post->title }}</h2>
                <span>Fecha de publicación: {{  $post->date_publish->format('d.m.y') }}</span>
            </div>
        </div>
    </section>
    <section class="event-detail-desc">
        <div class="container">
            <div class="post-detail-text">
                {!! $post->description !!}
            </div>
        </div>
    </section>
    <section class="event-detail-gallery">
        <div class="container">
            <ul class="gallery-list gallery-carousel">
                @forelse($post->gallery as $key => $value)
                    <li class="gallery-item"><img src="{{ url($value->path) }}" alt=""></li>
                @empty @endforelse
            </ul>
            <div class="share">
                <a href="#" id="share" data-placement="left" data-content="
                      <a href='https://www.facebook.com/sharer/sharer.php?u={{ $url }}' target='_blank'
                         style='font-size:25px;padding:5px;'><span class='icon-facebook'></span></a>
                      <a href='https://twitter.com/intent/tweet?url={{ $url }}' target='_blank'
                          style='font-size:25px;padding:5px;'><span class='icon-twitter'></span></a>"
                ><img src="{{ asset('img/share.svg') }}" alt="" width="15px"> Compartir</a>
            </div>
        </div>
    </section>
    <section class="comment">
        <div class="container">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="row">
                        <h3>Todos los comentarios: </h3>
                        <div class="media" id="media">
                            @if($post->comments->count() > 0)
                                @component('web.partials.comment_item', ['comments'=> $post->comments->chunk(6)[0]])
                                @endcomponent
                            @endif
                        </div>
                        <div class="form-button text-center">
                            <button type="button" class="button" id="more-comments" data-slug="{{ $post->slug }}">
                                <span>Ver más comentarios</span></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form">
                            <h4>¿Qué opinas de esta publicación?</h4>
                            <form action="{{ route('web.comment.store', ['post'=> $post->id]) }}" method="POST"
                                  id="form-comment">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="col-12">
                                        <div class="form-control">
                                            <div class="form-label">
                                                <label for="">Comentario</label>
                                            </div>
                                            <div class="form-textarea">
                                                <textarea name="comment" id="comment" maxlength="200"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-control">
                                            <div class="form-label">
                                                <label for="">Nombre</label>
                                            </div>
                                            <div class="form-input">
                                                <input type="text" name="name" id="name">
                                            </div>
                                        </div>
                                        <div class="form-control">
                                            <div class="form-label">
                                                <label for="">Correo electronico</label>
                                            </div>
                                            <div class="form-input">
                                                <input type="text" name="email" id="email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-button">
                                    <button type="submit" class="button"><span>Publicar</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="post-info sb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Lo más Leído</h2>
                </div>
            </div>
            <div class="row">
                @forelse($posts as $key => $value)
                    @include('web.partials.blog-item')
                @empty @endforelse
            </div>
            <div class="post-button">
                <a href="{{ route('web.posts') }}" class="button"><span>Ver más</span></a>
            </div>
        </div>
    </section>
@endsection