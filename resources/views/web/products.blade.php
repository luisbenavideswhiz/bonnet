@extends('web.layouts.app')

@section('bodyId')id="products_category" @endsection

@section('content')
    <section class="product-categories">
        @foreach($categories as $key => $value)
            <div class="product-category @if($key % 2 === 0) category-2 @endif @if($key !== 0) sb @endif">
                <div class="container">
                    <div class="title"><h2>{{ $value->name }}</h2></div>
                </div>
                <div class="product-list product-carousel">
                    @foreach($value->product as $product)
                        <figure class="product-item">
                            <a href="{{ route('web.product_detail', ['slug'=> $product->slug]) }}">
                                <div class="product-photo">
                                    <img src="{{ asset(isset($product->image) ? $product->image->path : '') }}" alt="">
                                </div>
                                <figcaption class="product-name">
                                    <span>{{ strlen(substr($product->name, 0, 50)) == 50 ? substr($product->name, 0, 50).'...' : substr($product->name, 0, 50)}}</span>
                                </figcaption>
                            </a>
                        </figure>
                    @endforeach
                </div>
                <div class="product-category-btn">
                    <a href="/producto/buscar?category={{ $value->slug }}" class="button"><span>Ver categoría</span></a>
                </div>
            </div>
        @endforeach
    </section>

    {{-- <section class="thank">
        <div class="container">
            <div class="thank-img">
                <img src="{{ asset('img/ico-construction.svg') }}" width="270" alt="">
            </div>
            <div class="title">
                <h2>Estamos optimizando nuestra web. <br> Pronto podrás visualizar todos nuestros productos.</h2>
            </div>
            <ul class="button-group">
                <li>
                    <a class="button" href="{{ route('web.home') }}"><span>Inicio</span></a>
                </li>
                <li>
                    <a class="button" href="{{ route('web.contact') }}"><span>Contacto</span></a>
                </li>
            </ul>
        </div>
    </section> --}}
@endsection
