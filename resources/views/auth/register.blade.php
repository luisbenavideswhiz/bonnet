<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Panel de Administración</title>


    <!-- Styles -->

    <link rel="stylesheet" href="{{ asset('libs/assets/animate.css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/simple-line-icons/css/simple-line-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap/dist/css/bootstrap.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/admin.min.css') }}" type="text/css" />

</head>

<body>
<div class="app app-header-fixed ">

    <div class="container w-xxl w-auto-xs">

        <a href class="navbar-brand block m-t">Panel de administración</a>
        <div class="m-b-lg">
            <div class="wrapper text-center">
                <strong>Formulario de registro</strong>
            </div>
            <form name="form" class="form-validation" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="list-group list-group-sm">
                    <div class="list-group-item{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input placeholder="Nombre completo" class="form-control no-border"  name="name" value="{{ old('name') }}" required autofocus>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="list-group-item{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" placeholder="Correo electrónico" class="form-control no-border" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="list-group-item{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" placeholder="Contraseña" class="form-control no-border"  name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="list-group-item">
                        <input id="password-confirm" type="password" placeholder="Confirmar contraseña" class="form-control no-border"  name="password_confirmation" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg btn-primary btn-block">Registrar</button>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('libs/jquery/jquery/dist/jquery.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>

</body>
