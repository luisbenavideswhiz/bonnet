<?php

return [
    'category' => [
        'success' => [
            'list' => 'Se encontraron :total categoria(s)'
        ]
    ],

    'product' => [
        'success' => [
            'list' => 'Se encontraron :total producto(s)'
        ]
    ],
    'type' => [
        'success' => [
            'list' => 'Se encontraron :total tipos de producto(s)'
        ]
    ],
    'brand' => [
        'success' => [
            'list' => 'Se encontraron :total marca(s)'
        ]
    ],
    'headquarter' => [
        'success' => [
            'list' => 'Se encontraron :total sede(s)'
        ]
    ],
];