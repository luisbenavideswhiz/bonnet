<?php

return [
    'unsupported_grant_type'  => 'El tipo de grant no es soportado por el servidor de autorización.',
    'invalid_request' => 'A la solicitud le falta un parámetro requerido, tiene algun parámetro no válido, incluye un parámetro de más, o está con un formato erroneo.',
    'invalid_request_hint' => 'Verifica el parámetro %s',
    'invalid_client' => 'Autenticación de cliente falló',
    'invalid_scope' => 'El scope es invalido, desconocido, o de formato erroneo.',
    'invalid_scope_hint_default' => 'Especifica un scope en la peticion.',
    'invalid_scope_hint' => 'Revisa el scope %s',
    'invalid_credentials' => 'Las credenciales de usuario fueron incorrectas.',
    'server_error' => 'El servidor de autorización encontró un error en la peticion:',
    'invalid_refresh_token' => 'El refresh token es inválido.',
    'access_denied' => 'El dueño del recurso o servidor de autorización denego la petición.',
    'invalid_grant' => 'La concesión de autorización proporcionada (por ejemplo, código de autorización, credenciales del propietario del recurso) o token de actualización no es válido, caducado, revocado, no coincide con el URI de redirección utilizado en la solicitud de autorización o se emitió a otro cliente.'
];