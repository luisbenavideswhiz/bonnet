var GoogleMaps = function () {
    var map;
    var markers = [];
    var geocoder;

    var initMap = function (idMap, idSearchInput, idLocationInput, location) {
        geocoder = new google.maps.Geocoder();

        map = new google.maps.Map(document.getElementById(idMap), {
            zoom: 13,
            center: location,
            mapTypeId: 'terrain'
        });

        map.addListener('click', function(event) {
            if(markers.length>=1){
                deleteMarkers();
            }
            addMarker(event.latLng);
            addInfoInput(event.latLng, idLocationInput);
            reverseGeocodeLatLng(event.latLng, idSearchInput);
        });

        setMarker(idLocationInput);

        document.getElementById(idSearchInput).addEventListener("keypress", function(event){
            event = event || window.event;
            if(event.keyCode === 13) {
                event.preventDefault();
                geocodeLatLng(this.value.trim(), idLocationInput);
            }
        });
    };

    var addInfoInput = function (location, idLocationInput) {
        document.getElementById(idLocationInput).value = location.lat()+','+location.lng();
    };

    var addMarker = function (location, center) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        center = center || false;
        if(center){
            map.setCenter(location);
        }
        markers.push(marker);
    };

    var setMapOnAll = function (map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    };

    var clearMarkers = function() {
        setMapOnAll(null);
    };

    var deleteMarkers = function () {
        clearMarkers();
        markers = [];
    };

    var geocodeLatLng = function (address, idLocationInput) {
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status === 'OK') {
                map.setCenter(results[0].geometry.location);
                if(markers.length>=1){
                    deleteMarkers();
                }
                addMarker(results[0].geometry.location);
                addInfoInput(results[0].geometry.location, idLocationInput);
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    };

    var reverseGeocodeLatLng = function (location, idSearchInput) {
        geocoder.geocode({'location': location}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    if(markers.length>=1){
                        deleteMarkers();
                    }
                    addMarker(location);
                    document.getElementById(idSearchInput).value = results[0].formatted_address;
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    };

    var setMarker = function (idLocationInput) {
        var location = document.getElementById(idLocationInput).value;
        if(location!=''){
            addMarker({lat: parseFloat(location.split(',')[0]), lng: parseFloat(location.split(',')[1])}, true);
        }
    };

    return {
        initGeo: function (idMap, idSearchInput, idLocationInput, location) {
            initMap(idMap, idSearchInput, idLocationInput,  location);
        }
    };
}();