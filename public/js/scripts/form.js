var Form = function () {

    var cropper, appends;

    var appendDataForm = function (items) {
        appends = items;
    };

    var removeDataForm = function () {
        appends = null;
    };

    var ajaxMultipart = function (form) {
        var data = new FormData(form[0]);
        if (typeof appends !== 'undefined' && appends !== null) {
            $.each(appends, function (i, val) {
                data.append(val.name, val.value);
            });
        }
        return {
            url: form.prop('action'),
            type: form.prop('method'),
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (!response.status) {
                    $.each(response.data, function (i, val) {
                        toastr.error(val, 'Error');
                    });
                } else {
                    if (typeof response.data.redirect !== 'undefined') {
                        window.location.href = response.data.redirect;
                    }
                    if (typeof response.data.modal !== 'undefined') {
                        $('#' + response.data.modal).modal('hide');
                    }
                    toastr.success(response.message, 'Correcto');
                    window.table.ajax.reload(null, false);
                }
            }
        };
    };

    var ajaxSimple = function (form) {
        return {
            url: form.prop('action'),
            type: form.prop('method'),
            data: form.serialize(),
            success: function (response) {
                if (!response.status) {
                    $.each(response.data, function (i, val) {
                        toastr.error(val, 'Error');
                    });

                } else {
                    if (typeof response.data.redirect !== 'undefined') {
                        window.location.href = response.data.redirect;
                    }
                    if (typeof response.data.modal !== 'undefined') {
                        $('#' + response.data.modal).modal('hide');
                    }
                    toastr.success(response.message, 'Correcto');
                    window.table.ajax.reload(null, false);
                }
            }
        };
    };

    var submitForm = function (id, multipart) {
        $(id).submit(function (event) {
            event.preventDefault();
            CKEditorUpdate();
            var form = $(this);
            var callback;
            multipart = multipart || false;
            if (multipart) {
                callback = ajaxMultipart(form);
            } else {
                callback = ajaxSimple(form);
            }
            $.ajax(callback);
        });
    };


    var CKEditorUpdate = function () {
        if (typeof CKEDITOR !== 'undefined') {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    };

    var getData = function (id, callback) {
        $.get($(id).data('action'), callback);
    };

    var addNetwork = function (id, content, clone) {
        $(id).click(function () {
            if ($(content + '>' + clone).length < 5) {
                $(content).append($(content + '>' + clone + ':last').clone());
                Form.selectInputGroup('.input-group-btn');
            }
        });
    };

    var deleteNetwork = function (className, dropdown) {
        $(document).on('click', className, function () {
            if ($(className).length !== 1) {
                $(this).parent().parent().remove();
            } else {
                $(this).parent().parent().find('input').val('');
                $(this).parent().parent().find(dropdown).html('Seleccionar <span class="caret"></span>');
            }
        });
    };

    var selectInputGroup = function (id) {
        $(id).on("click.bs.dropdown", function (event) {
            event.preventDefault();
            var content = ' <span class="caret"></span>';
            if ($(event.target).prop('tagName') !== 'BUTTON' && $(event.target).prop('tagName') !== 'SPAN') {
                if ($(event.target).text().trim() !== '') {
                    content = $(event.target).html() + content;
                    $(this).find('.dropdown-toggle').html(content);
                    $(this).find('input').val($(event.target).data('id'));
                } else if ($(event.target).prop('tagName') === 'I' && $(event.target).parent().prop('tagName') !== 'BUTTON') {
                    content = $(event.target).parent().html() + content;
                    $(this).find('.dropdown-toggle').html(content);
                    $(this).find('input').val($(event.target).data('id'));
                }
            }
        });
    };

    function imageCropper(image, _width, _height) {
        var cropper = new Cropper($(image)[0], {
            aspectRatio: _width / _height,
            minContainerWidth: 600,
            minContainerHeight: 480
        });
        return cropper;
    }

    function imageEditor(modal, wrapper, image, btn, input, _width, _height) {
        $(modal).on('show.bs.modal', function () {
            $(image).prop('src', $(wrapper).find('img:visible').prop('src'));
            cropper = imageCropper(image, _width, _height);
        });

        $(btn).click(function () {
            var canvas = cropper.getCroppedCanvas({width: _width, height: _height});
            $(wrapper).find('img:visible').prop('src', canvas.toDataURL());
            $(modal).modal('hide');
            canvas.toBlob(function (blob) {
                appends = [{name: 'image', value: blob}];
            });
        });
        $(modal).on('hide.bs.modal', function () {
            cropper.destroy();
        });
    }

    return {
        get: getData,
        form: submitForm,
        appendDataForm: appendDataForm,
        removeDataForm: removeDataForm,
        addNetwork: addNetwork,
        deleteNetwork: deleteNetwork,
        selectInputGroup: selectInputGroup,
        imageEditor: imageEditor,
    };
}();