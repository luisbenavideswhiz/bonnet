let mix = require('laravel-mix');
//var nib = require('nib');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/admin/admin.js', 'public/js')
    .stylus('resources/assets/stylus/admin/admin.styl', 'public/css');

mix.js('resources/assets/js/web/app.js', 'public/js')
    .stylus('resources/assets/stylus/web/app.styl', 'public/css')
    .sass('resources/assets/sass/web/popover.sass', 'public/css');

mix.styles(['public/css/app.css', 'public/css/popover.css'],
    'public/css/app.css');


mix.options({
    processCssUrls: false
})
    .sourceMaps()
    .version();
